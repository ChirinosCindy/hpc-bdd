@ServicioObtenerDatosParalelo
Feature: ServicioObtenerDatosParalelo

  @ServicioObtenerDatosParalelo
  Scenario Outline: Servicio Obtener Datos Paralelo
    Given Se quiere realizar una operacion de "Servicio Obtener Datos Paralelo"
    When La operacion se realiza por administrativo
    Then Ingresar a la url del servicio Obtener Datos Paralelo
    And Ingresar numero de credito principal "<nroCredito>"
    And Invocar servicio Obtener Datos Paralelo

    Examples:
      |nroCredito |
      |39680|
      |39019|
      |38288|
      |36301|
      |36299|
      |30277|
      |20957|
      |18490|
      |18489|
      |5594|
      |5574|
