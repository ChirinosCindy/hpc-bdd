@HPC
Feature: HPC

  @DesembolsoHipotecario
  Scenario Outline: Desembolso Hipotecario administrativo
    Given Se quiere realizar una operacion de "Desembolso Hipotecario"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "Hipotecario"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Seleccionar el tipo de evaluacion
    And Seleccionar la moneda del precio de venta "<moneda>"
    And Ingresar el monto del precio de venta "<montoDesembolso>"
    And Seleccionar la moneda del valor comercial "<moneda>"
    And Ingresar el monto del valor comercial "<montoDesembolso>"
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And ingresar Seguro del Bien "<montoDesembolso>"
    And Seleccionar la moneda del gravamen "<moneda>"
    And Ingresar el monto de Gravamen "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario Desembolso
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
     |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
     |0101|DOLARES AMERICANOS|60072850|100000|240|12|01/08/2020|
     |0101|DOLARES AMERICANOS|60072850|100000|240|12|30/07/2020|

  @DesembolsoBonoBMS
  Scenario Outline: Desembolso Bono BMS
    Given Se quiere realizar una operacion de "Desembolso Bono BMS"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "Hipotecario"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Seleccionar el tipo de evaluacion
    And Seleccionar la moneda del precio de venta "<moneda>"
    And Ingresar el monto del precio de venta "<montoDesembolso>"
    And Seleccionar la moneda del valor comercial "<moneda>"
    And Ingresar el monto del valor comercial "<montoDesembolso>"
    And Ingresar el numero Fondo MV "<nroFMVCofide>"
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And ingresar Seguro del Bien "<montoDesembolso>"
    And ingresar BonoBMS "<montoBonoBMS>"
    And Seleccionar la moneda del gravamen "<moneda>"
    And Ingresar el monto de Gravamen "<montoDesembolso>"
    And Guardar Operacion
    And Verificar registro Tarifa Dia Ayer
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario Desembolso
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|nroFMVCofide|plazo|tasa|fechaValor|montoBonoBMS|
      |1701|SOLES|60072850|80000|12234|120|9|07/05/2021|3000|
      |1801|SOLES|60072850|80000|12342|120|9|07/05/2021|3000|



  @DesembolsoHipotecarioUpgrade
  Scenario Outline: Desembolso Hipotecario administrativo Upgrade
    Given Se quiere realizar una operacion de "Desembolso Hipotecario"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo Upgrade "Hipotecario"
    And Seleccionar la Opcion Operaciones Upgrade
    And Seleccionar la Opcion Creditos Upgrade
    And Seleccionar la Opcion Ingreso Upgrade
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Seleccionar el tipo de evaluacion
    And Seleccionar la moneda del precio de venta "<moneda>"
    And Ingresar el monto del precio de venta "<montoDesembolso>"
    And Seleccionar la moneda del valor comercial "<moneda>"
    And Ingresar el monto del valor comercial "<montoDesembolso>"
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And ingresar Seguro del Bien "<montoDesembolso>"
    And Seleccionar la moneda del gravamen "<moneda>"
    And Ingresar el monto de Gravamen "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |0101|DOLARES AMERICANOS|60072850|100000|240|12|01/08/2020|
      |0101|DOLARES AMERICANOS|60072850|100000|240|12|30/07/2020|


  @DesembolsoPPGH
  Scenario Outline: Desembolso PPGH administrativo
    Given Se quiere realizar una operacion de "Desembolso PPGH"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "PPGH"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Ingresar el importe asegurado "<montoDesembolso>"
    And Seleccionar la moneda del gravamen "<moneda>"
    And Ingresar el monto de Gravamen "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |0601|DOLARES AMERICANOS|60072850|110000|240|12|01/08/2020|
      |0601|SOLES|60072850|120000|240|12|30/07/2021|
      #|0601|SOLES|60074947|200000|240|11|05/02/2020|
      #|0601|DOLARES AMERICANOS|60072850|100000|240|12|05/04/2020|
      #|0601|SOLES|60074947|200000|240|11|05/02/2020|
      #|0601|DOLARES AMERICANOS|60027011|150000|240|15|05/04/2020|
      #|0601|SOLES|60074947|120000|240|13|05/02/2020|
      #|0601|DOLARES AMERICANOS|60072850|100000|240|12|05/12/2020|
      #|0601|SOLES|60074947|200000|240|11|05/11/2020|
      #|0601|DOLARES AMERICANOS|60027011|150000|240|15|05/02/2021|
      #|0601|SOLES|60074947|120000|240|13|05/03/2021|
      #|0601|SOLES|60072850|100000|240|12|05/04/2020|
      #|0101|SOLES|60074947|200000|240|11|05/02/2020|


  @DesembolsoPPGHUpgrade
  Scenario Outline: Desembolso PPGH administrativo Upgrade
    Given Se quiere realizar una operacion de "Desembolso PPGH"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo Upgrade "PPGH"
    And Seleccionar la Opcion Operaciones Upgrade
    And Seleccionar la Opcion Creditos Upgrade
    And Seleccionar la Opcion Ingreso Upgrade
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Ingresar el importe asegurado "<montoDesembolso>"
    And Seleccionar la moneda del gravamen "<moneda>"
    And Ingresar el monto de Gravamen "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |0601|DOLARES AMERICANOS|60072850|110000|240|12|01/08/2020|
      |0601|SOLES|60072850|120000|240|12|30/07/2021|
      #|0601|SOLES|60074947|200000|240|11|05/02/2020|
      #|0601|DOLARES AMERICANOS|60072850|100000|240|12|05/04/2020|
      #|0601|SOLES|60074947|200000|240|11|05/02/2020|
      #|0601|DOLARES AMERICANOS|60027011|150000|240|15|05/04/2020|
      #|0601|SOLES|60074947|120000|240|13|05/02/2020|
      #|0601|DOLARES AMERICANOS|60072850|100000|240|12|05/12/2020|
      #|0601|SOLES|60074947|200000|240|11|05/11/2020|
      #|0601|DOLARES AMERICANOS|60027011|150000|240|15|05/02/2021|
      #|0601|SOLES|60074947|120000|240|13|05/03/2021|
      #|0601|SOLES|60072850|100000|240|12|05/04/2020|
      #|0101|SOLES|60074947|200000|240|11|05/02/2020|




  @DesembolsoPersonal
  Scenario Outline: Desembolso Personal administrativo
    Given Se quiere realizar una operacion de "Desembolso Personal"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "Personal"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Seleccionar envio de informe de pago
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |2101|SOLES|60072850|10000|24|30|05/02/2021|
      #|2101|SOLES|60072850|10000|24|30|05/02/2020|
      #|2101|SOLES|60072850|10000|24|30|05/04/2020|
      #|2101|SOLES|60072850|10000|24|30|05/02/2020|
      #|2101|DOLARES AMERICANOS|60027011|15000|24|15|05/02/2021|
      #|2101|SOLES|60074947|12000|24|13|05/03/2021|
      #|2101|SOLES|60072850|10000|24|12|05/04/2020|
      #|2101|SOLES|60074947|12000|24|13|05/03/2021|
      #|2101|SOLES|60072850|10000|24|12|05/04/2020|

  @DesembolsoPersonalUpgrade
  Scenario Outline: Desembolso Personal administrativo Upgrade
    Given Se quiere realizar una operacion de "Desembolso Personal"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo Upgrade "Personal"
    And Seleccionar la Opcion Operaciones Upgrade
    And Seleccionar la Opcion Creditos Upgrade
    And Seleccionar la Opcion Ingreso Upgrade
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Seleccionar envio de informe de pago
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |2101|SOLES|60072850|10000|24|30|05/02/2021|
      #|2101|SOLES|60072850|10000|24|30|05/02/2020|
      #|2101|SOLES|60072850|10000|24|30|05/04/2020|
      #|2101|SOLES|60072850|10000|24|30|05/02/2020|
      #|2101|DOLARES AMERICANOS|60027011|15000|24|15|05/02/2021|
      #|2101|SOLES|60074947|12000|24|13|05/03/2021|
      #|2101|SOLES|60072850|10000|24|12|05/04/2020|
      #|2101|SOLES|60074947|12000|24|13|05/03/2021|
      #|2101|SOLES|60072850|10000|24|12|05/04/2020|



  @DesembolsoVehicular
  Scenario Outline: Desembolso Vehicular administrativo
    Given Se quiere realizar una operacion de "Desembolso Vehicular"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "Vehicular"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Ingresar codigo de establecimiento
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Seleccionar envio de informe de pago
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Ingresar la tasa del seguro del bien vehicular
    And Ingresar monto asegurado "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |4601|DOLARES AMERICANOS|60072850|60000|60|12.990|05/02/2021|
      #|4601|SOLES|60072850|60000|60|12.990|05/02/2020|
      #|4601|SOLES|60074947|45000|60|12.990|05/03/2021|
      #|4601|SOLES|60072850|60000|60|12.990|05/04/2020|
      #|4601|SOLES|60074947|42000|60|12.990|05/03/2021|
      #|4601|SOLES|60072850|34000|60|12.990|05/04/2020|



  @DesembolsoVehicularUpgrade
  Scenario Outline: Desembolso Vehicular administrativo Upgrade
    Given Se quiere realizar una operacion de "Desembolso Vehicular"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo Upgrade "Vehicular"
    And Seleccionar la Opcion Operaciones Upgrade
    And Seleccionar la Opcion Creditos Upgrade
    And Seleccionar la Opcion Ingreso Upgrade
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Ingresar codigo de establecimiento
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Seleccionar envio de informe de pago
    And Ingresar el tipo de abono
    And Ingresar el monto solicitado "<montoDesembolso>"
    And Ingresar la tasa del seguro del bien vehicular
    And Ingresar monto asegurado "<montoDesembolso>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|montoDesembolso|plazo|tasa|fechaValor|
      |4601|DOLARES AMERICANOS|60072850|60000|60|12.990|05/02/2021|
      #|4601|SOLES|60072850|60000|60|12.990|05/02/2020|
      #|4601|SOLES|60074947|45000|60|12.990|05/03/2021|
      #|4601|SOLES|60072850|60000|60|12.990|05/04/2020|
      #|4601|SOLES|60074947|42000|60|12.990|05/03/2021|
      #|4601|SOLES|60072850|34000|60|12.990|05/04/2020|



  @DesembolsoRefinanciado
  Scenario Outline: Desembolso Refinanciado administrativo
    Given Se quiere realizar una operacion de "Desembolso Refinanciado"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "Refinanciado"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ingreso
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Ingresar datos de producto a refinanciar
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|plazo|tasa|fechaValor|
      |8001|SOLES|60072850|60|25|15/03/2020|


  @DesembolsoRefinanciadoUpgrade
  Scenario Outline: Desembolso Refinanciado administrativo Upgrade
    Given Se quiere realizar una operacion de "Desembolso Refinanciado"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo Upgrade "Refinanciado"
    And Seleccionar la Opcion Operaciones Upgrade
    And Seleccionar la Opcion Creditos Upgrade
    And Seleccionar la Opcion Ingreso Upgrade
    And Seleccionar Boton Nuevo
    And Ingresar el codigo del producto "<codigoProducto>"
    And Seleccionar la moneda "<moneda>"
    And Ingresar CU del cliente "<CU>"
    And Ingresar codigo del promotor
    And Ingresar canal de venta
    And Ingresar codigo de tienda
    And Ingresar datos de producto a refinanciar
    And ingresar fecha valor "<fechaValor>" de desembolso
    And Ingresar el destino del credito
    And Ingresar el plazo "<plazo>"
    And Ingresar la tasa "<tasa>"
    And Ingresar el tipo de abono
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |codigoProducto|moneda|CU|plazo|tasa|fechaValor|
      |8001|SOLES|60072850|60|25|15/03/2020|


  @ModificacionHipotecario
  Scenario Outline: Modificacion administrativo
    Given Se quiere realizar una operacion de "Modificacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Modificacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|TasaInteres|
      |Hipotecario|31405|19|
      |Personal|36062|15|
      |Hipotecario|31405|19|



  @AjusteHipotecarioTasa
  Scenario Outline: Ajuste administrativo
    Given Se quiere realizar una operacion de "Ajuste"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ajuste
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar Ajuste Desde la Cuota "<AjusteCuota>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|AjusteCuota|TasaInteres|
      |Hipotecario|42204|3|19|
      |Personal|42098|3|19|



  @DescargoHipotecario
  Scenario Outline: Descargo Hipotecario
    Given Se quiere realizar una operacion de "Descargo"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Descargo
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Seleccionar Datos del Descargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|
      |Hipotecario|42119|
      |Personal|216|

  @ReingresoHipotecario
  Scenario Outline: Reingreso Hipotecario
    Given Se quiere realizar una operacion de "Reingreso"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Reingreso
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Capturar numero de credito generado
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|TasaInteres|
      |Hipotecario|1504|14|
      |Personal|1504|14|

  @SimuladorEspecifico
  Scenario Outline: Simulador Especifico
    Given Se quiere realizar una operacion de "Simulador Especifico"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Especifico

    Examples:
      |grupo|
      |Hipotecario|

  @ConsultaCredito
  Scenario Outline: Evidencia Detalle del Credito
    Given Se quiere realizar una operacion de "Evidencia Detalle Credito"
    When antes de operacion
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And click en Consultas
    And click en Consulta del Credito
    And Ingresar credito "<nroCredito>"
    And tomar evidencia Detalle del Credito

    Examples:
      |grupo|nroCredito|
      |Hipotecario|18488|
      |Personal|1504|

  @ValidacionSaldoDetalleYSaldoCronograma
  Scenario Outline: Validacion del saldo que se muestra en el Detalle con el saldo del Cronograma
    Given Se quiere realizar una operacion de "Validacion Saldos"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And click en Consultas
    And click en Consulta del Credito
    And Ingresar credito "<nroCredito>"
    And ingresar a pestana cronograma
    And validar que el valor del saldo en el cronograma sea correcto

    Examples:
      |grupo|nroCredito|
      |Hipotecario|18488|
      |hipotecario|34208|

  @EvidenciaCronogramas
  Scenario Outline: Evidencia de cronogramas
    Given Se quiere realizar una operacion de "Evidencia Cronogramas"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And click en Consultas
    And click en Consulta del Credito
    And Ingresar credito "<nroCredito>"
    #And tomar evidencia Detalle del Credito
    And ingresar a pestana cronograma
    And tomar evidencias Cronogramas

    Examples:
      |grupo|nroCredito|
      #|Hipotecario|38873|
      #|Hipotecario|41515|
      #|Hipotecario|27578|
      |Personal|36982|
      |Personal|37122|
      |PPGH|40891|
      |PPGH|42138|
      |PPGH|42777|
      |Vehicular|42338|
      |Vehicular|43025|
      |Refinanciado|42092|
      |Refinanciado|40589|


  @CambioDeVencimiento
  Scenario Outline: Cambio de Vencimiento
    Given Se quiere realizar una operacion de "Cambio de Vencimiento"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Cambio de Vencimiento
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Datos del Cambio de Vencimiento
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|
      |Hipotecario|18470|
      |Personal|1504|


  @AjusteDeCuota
  Scenario Outline: Ajuste de Cuota
    Given Se quiere realizar una operacion de "Ajuste de Cuota"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ajuste de Cuota
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Seleccionar del Cronograma de Cuotas la opcion Modificar "<cantCuotas>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|cantCuotas|
      |Hipotecario|42802|2        |
      |Personal|27580|2        |



  @PagoDeCuotas
  Scenario Outline: Pago de Cuota
    Given Se quiere realizar una operacion de "Pago de Cuotas"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos
    And Seleccionar la Opcion de Pago de Cuotas
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And pagar "<cantCuotas>" cuotas
    And Ingresar Datos del Pago de Cuotas
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito | cantCuotas|
      |Hipotecario|19047| 1|
      |Personal|27580|1    |

  @PagoParcial
  Scenario Outline: Pago de cuota parcial
    Given Se quiere realizar una operacion de "Pago Parcial"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos
    And Seleccionar la Opcion de Pago de Cuotas
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And seleccionar una cuota
    And ingresar monto parcial a pagar "<montoParcial>"
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito |montoParcial|
      |Hipotecario|41533|26.44|


  @Cancelacion
  Scenario Outline: Cancelacion
    Given Se quiere realizar una operacion de "Cancelacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos
    And Seleccionar la Opcion Cancelacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito |
      |Hipotecario|19047|
      |Personal|27580|

  @CancelacionCFV
  Scenario Outline: Cancelacion CFV
    Given Se quiere realizar una operacion de "Cancelacion CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos CFV
    And Seleccionar la Opcion Cancelacion CFV
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>"
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito |fechaValor|
      |Hipotecario|19047|10/03/2021 |
      |Personal|27580|10/03/2021    |

  @SimuladorCancelacion
  Scenario Outline: Simulador Cancelacion
    Given Se quiere realizar una operacion de "Simulacion Cancelacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Cancelacion
    And Ingresar credito "<nroCredito>"
    And Simular Operacion

    Examples:
      | grupo | nroCredito |
      |Hipotecario|19047|
      |Personal|27580|

  @SimuladorCancelacionCFV
  Scenario Outline: Simulador Cancelacion CFV
    Given Se quiere realizar una operacion de "Simulacion Cancelacion CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos CFV
    And Seleccionar la Opcion Simulador Cancelacion CFV
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>" para simulacion
    And Simular Operacion

    Examples:
      | grupo | nroCredito |fechaValor|
      |Hipotecario|19047|10/03/2021 |

  @SimuladorPrepago
  Scenario Outline: Simulador Prepago
    Given Se quiere realizar una operacion de "Simulacion Prepago"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Prepago
    And Ingresar credito "<nroCredito>"
    And ingresar monto a simular en el prepago "<montoSimulacionPrepago>"
    And ingresar modalidad de prepago "<modalidadPrepago>"
    And Simular Operacion
    #And Descargar Excel

    Examples:
      | grupo | nroCredito |montoSimulacionPrepago|modalidadPrepago|
      |Hipotecario|  18389 |12800        |reduccion de plazo|
      |Hipotecario|  18389 |1280        |reduccion de plazo|
  #19047
  @SimuladorPrepagoCFV
  Scenario Outline: Simulador Prepago CFV
    Given Se quiere realizar una operacion de "Simulacion Prepago CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos CFV
    And Seleccionar la Opcion Simulador Prepago CFV
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>" para simulacion Prepago
    And ingresar monto a simular en el prepago "<montoSimulacionPrepago>"
    And ingresar modalidad de prepago "<modalidadPrepago>"
    And Simular Operacion

    Examples:
      | grupo | nroCredito |montoSimulacionPrepago|fechaValor|modalidadPrepago|
      |Hipotecario|19047   |12000        |10/03/2021 |reduccion de plazo|

  @Prepago
  Scenario Outline: Prepago
    Given Se quiere realizar una operacion de "Prepago"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos
    And Seleccionar la Opcion Prepago
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar Datos del Prepago "<montoNeto>" "<modalidad>"
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito |montoNeto|modalidad|
      |Hipotecario|18701|1000|REDUCCION DE PLAZOS|

  @MantenedorIndiceVAC
  Scenario Outline: Mantenedor Indice VAC
    Given Se quiere realizar una operacion de "Mantenimiento Indice VAC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA con usuario admin
    And Seleccionar el Grupo "<grupo>" admin
    And Seleccionar la Opcion Mantenimiento
    And Seleccionar la Opcion Indice VAC
    And Seleccionar Boton Nuevo
    And Ingresar fecha del Indice "<fecha>"
    And Ingresar valor del Indice "<valorIndice>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Salir del Formulario
    And Buscar el ultimo registro de indice VAC ingresado "<fecha>"
    And Seleccionar el registro del indice
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |fecha |valorIndice|
      |Hipotecario|01/04/2021|9.5|

  @MantenedorFactorBono
  Scenario Outline: Mantenedor Factor Bono
    Given Se quiere realizar una operacion de "Mantenimiento Factor Bono"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA con usuario admin
    And Seleccionar el Grupo "<grupo>" admin
    And Seleccionar la Opcion Mantenimiento
    And Seleccionar la Opcion Factor Bono
    And Seleccionar Boton Nuevo
    And Ingresar fecha del factor "<fecha>"
    And Ingresar valor del factor en soles "<factorSoles>"
    And Ingresar valor del factor en dolares "<factorDolares>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Buscar el ultimo registro de factores ingresado "<fecha>"
    And Seleccionar el registro de los Factores
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |fecha |factorSoles|factorDolares|
      |Hipotecario|01/04/2021|7.7|7.8|

  @PagoCuotasCFV
  Scenario Outline: Pago de Cuotas CFV
    Given Se quiere realizar una operacion de "Pago de Cuotas CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos CFV
    And Seleccion la Opcion Pago de Cuotas CFV
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And pagar "<cantCuotas>" cuotas CFV
    And Ingresar Datos del Pago de Cuotas
    And Seleccionar medio de cargo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito |cantCuotas|
      |PPGH|43142|20|



  @Bloqueo
  Scenario Outline: Bloqueo
    Given Se quiere realizar una operacion de "Bloqueo"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Bloqueo
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar comentario
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito

    Examples:
      |grupo |nroCredito |
      |Hipotecario|16271|


  @DesBloqueo
  Scenario Outline: DesBloqueo
    Given Se quiere realizar una operacion de "DesBloqueo"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de DesBloqueo
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar comentario
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito

    Examples:
      |grupo |nroCredito |
      |Hipotecario|16271|


  @CambioProductoHipotecario
  Scenario Outline: Cambio Producto Hipotecario
    Given Se quiere realizar una operacion de "Cambio de Producto Hipotecario"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Cambio Producto Hipotecario
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito |
      |Hipotecario|39379|


  @PrepagoCFV
  Scenario Outline: Prepago CFV
    Given Se quiere realizar una operacion de "Prepago CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Pagos CFV
    And Seleccionar la Operacion Prepago CFV
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>"
    And ingresar Datos del Prepago "<montoNeto>" "<modalidad>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito |montoNeto|fechaValor|modalidad|
      |Hipotecario|00004377   |1000        |10/03/2021 |REDUCCION DE PLAZOS|



  @ReengancheHipotecario
  Scenario Outline: Reenganche Hipotecario
    Given Se quiere realizar una operacion de "Reenganche"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Reenganche
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And ingresar Monto Solicitado
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Salir del Formulario
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|TasaInteres|
      |Hipotecario|19047|18        |


  @InclusionCuotaFlexible
  Scenario Outline: Inclusion Cuota Flexible
    Given Se quiere realizar una operacion de "Inclusion Cuota Flexible"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Inclusion Cuota Flexible
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Datos de Inclusion de Cuota flexible CF "<Anio>" "<Mes>"
    And Seleccionar Tipo
    And Ingresar Observacion
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|Anio|Mes|
      |Hipotecario|5566|2021|04|




  @InclusionCuotaFlexibleCFV
  Scenario Outline: Inclusion Cuota Flexible CFV
    Given Se quiere realizar una operacion de "Inclusion Cuota Flexible CFV"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Inclusion Cuota Flexible CFV
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Fecha Fecha Valor "<fechaValor>"
    And Ingresar Datos de Inclusion de Cuota flexible CF "<Anio>" "<Mes>"
    And Seleccionar Tipo para Inclusion CuotaFlexible CFV "<tipo>"
    And Ingresar Observacion
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|fechaValor|tipo|Anio|Mes|
      |Hipotecario|5566|16/03/2021|CONSERVAR EL PLAZO  |2021|04|
     #|Hipotecario|5566|16/03/2021|CONSERVAR EL PLAZO |2021|04|
     #|Hipotecario|5566|16/03/2021|CONSERVAR LA CUOTA|2021|04|


  @InclusionCuotaReprogramada
  Scenario Outline: Inclusion Cuota Reprogramada
    Given Se quiere realizar una operacion de "Inclusion Cuota Reprogramada"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Inclusion Cuota Reprogramada
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Cuota Cero "<nroCuotaCero>"
    And Ingresar Nro de Cuotas "<nroCuotas>"
    And Ingresar Observacion
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|nroCuotaCero|nroCuotas|
      |Hipotecario|00005548|45|2|


  @AnulacionPagoDeCuotas
  Scenario Outline: Anulacion Pago de Cuotas
    Given Se quiere realizar una operacion de "Anulacion de Pago de Cuotas"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Anulacion
    And Seleccionar la Opcion Anulacion Pago de Cuotas
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And pagar "<cantCuotas>" cuotas
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Registros de la Grilla "<cantCuotas>"

    Examples:
      |grupo |nroCredito|cantCuotas|
      |Hipotecario|00018470|4|



  @AnulacionPrepago
  Scenario Outline: Anulacion Prepago
    Given Se quiere realizar una operacion de "Anulacion Prepago"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Anulacion
    And Seleccionar la Opcion Anulacion Prepago
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|
      |Hipotecario|00041503|



  @AnulacionCancelacion
  Scenario Outline: Anulacion Cancelacion
    Given Se quiere realizar una operacion de "Anulacion Cancelacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Anulacion
    And Seleccionar la Opcion Anulacion Cancelacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|
      |Hipotecario|41534|


  @SimuladorPagoDeCuotas
  Scenario Outline: Simulador Pago de Cuotas
    Given Se quiere realizar una operacion de "Simulacion Pago de Cuotas"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Pago de Cuotas
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>" para simulacion
    And Simular Operacion

    Examples:
      | grupo | nroCredito |fechaValor|
      |Hipotecario|00041825|17/03/2021|



  @SimuladorCuotaReprogramada
  Scenario Outline: Simulador Cuota Reprogramada
    Given Se quiere realizar una operacion de "Simulador Cuota Reprogramada"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Cuota Reprogramada
    And Ingresar credito "<nroCredito>"
    And Ingresar Cuota Cero "<nroCuotaCero>"
    And Ingresar Nro de Cuotas "<nroCuotas>"
    And Simular Operacion
    #And Descargar Archivo Excel


    Examples:
      |grupo |nroCredito|nroCuotaCero|nroCuotas|
      |Hipotecario|5548|45|2|



  @SimuladorCuotaFlexible
  Scenario Outline: Simulador Cuota Flexible
    Given Se quiere realizar una operacion de "Simulador Cuota Flexible"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Cuota Flexible
    And Ingresar credito "<nroCredito>"
    And Ingresar Datos de Inclusion de Cuota flexible CF "<Anio>" "<Mes>"
    And Seleccionar Tipo
    And Simular Operacion
    #And Descargar Archivo Excel


    Examples:
      |grupo |nroCredito|Anio|Mes|
      |Hipotecario|5548|2021|04|


  @SimuladorModificacion
  Scenario Outline: Simulador Modificacion
    Given Se quiere realizar una operacion de "Simulador Modificacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulador Modificacion
    And Ingresar credito "<nroCredito>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Simular Operacion
    Examples:
      |grupo|nroCredito|TasaInteres|
      |Hipotecario|31405|19|
      |Personal|36062|15|
      |Hipotecario|31405|19|



  @SimuladorAjuste
  Scenario Outline: Simulador Ajuste
    Given Se quiere realizar una operacion de "Simulador Ajuste"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ajuste
    And Ingresar credito "<nroCredito>"
    And ingresar Ajuste Desde la Cuota "<AjusteCuota>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Simular Operacion

    Examples:
      |grupo|nroCredito|AjusteCuota|TasaInteres|
      |Hipotecario|42204|123|19|
      |Personal|42098|3|19|


  @SimuladorCambioDeVencimiento
  Scenario Outline: Simulador Cambio de Vencimiento
    Given Se quiere realizar una operacion de "Simulador Cambio de Vencimiento"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Simuladores
    And Seleccionar la Opcion Simulacion Cambio de Vencimiento
    And Ingresar credito "<nroCredito>"
    And Ingresar Dia de Vencimiento de la Simulacion
    And Simular Operacion


    Examples:
      |grupo|nroCredito|
      |Hipotecario|18470|
      |Personal|36062|


  @AnulacionModificacion
  Scenario Outline: Anulacion Modificacion
    Given Se quiere realizar una operacion de "Anulacion Modificacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Anulacion
    And Seleccionar la Opcion Anulacion Modificacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Motivo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      | grupo | nroCredito |
      |Hipotecario|20502|
      |Personal|36062|


  @CargaCronogramaMasivo
  Scenario Outline: Carga Cronograma Masivo
    Given Se quiere realizar una operacion de "Carga Cronograma Masivo"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Carga Masiva
    And Seleccionar la Opcion Cronograma Masivo
    And Seleccionar Examinar


    Examples:
      | grupo |
      |Hipotecario|
      |Personal|


    @LiquidacionAdeudado
    Scenario Outline: Liquidacion Adeudado
      Given Se quiere realizar una operacion de "Liquidacion Adeudado"
      When La operacion se realiza por administrativo
      Then Ingresar al SDA
      And Seleccionar el Grupo "<grupo>"
      And Seleccionar la Opcion Operaciones
      And Seleccionar la Opcion Adeudado
      And Seleccionar la Opcion Liquidacion Adeudado
      And Seleccionar Boton Nuevo
      And Ingresar credito "<nroCredito>"
      And ingresar fecha valor "<fechaValor>"
      And Guardar Operacion
      And Verificar que la operacion se haya realizado con exito
      And Seleccionar Registro de la Grilla
      And Eliminar Operacion

      Examples:
        | grupo | nroCredito | fechaValor |
        |Hipotecario|00001381|06/04/2021 |

  @LiquidacionCoberturado
  Scenario Outline: Liquidacion Coberturado
    Given Se quiere realizar una operacion de "Liquidacion Coberturado"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Adeudado
    And Seleccionar la Opcion Liquidacion Coberturado
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar fecha valor "<fechaValor>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion

    Examples:
      | grupo | nroCredito | fechaValor |
      |Hipotecario|32047|06/04/2021 |


  @AnulacionLiquidacionCoberturado
  Scenario Outline: Anulacion Liquidacion Coberturado
    Given Se quiere realizar una operacion de "Anulacion Liquidacion Coberturado"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Adeudado
    And Seleccionar la Opcion Anulacion Liquidacion Coberturado
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Motivo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion

    Examples:
      | grupo | nroCredito |
      |Hipotecario|34122|

  @AnulacionCoberturado
  Scenario Outline: Anulacion Coberturado
    Given Se quiere realizar una operacion de "Anulacion Coberturado"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Adeudado
    And Seleccionar la Opcion Anulacion Coberturado
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Motivo
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion

    Examples:
      | grupo | nroCredito |
      |Hipotecario|31545|



  @ReporteDesembolsos
  Scenario Outline: Reporte Desembolsos
    Given Se quiere realizar una operacion de "Reporte Desembolsos"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Reportes
    And Seleccionar la Opcion Reporte Desembolsos
    And Ingresar Fecha Ingreso del "<fechaIngresoDel>"
    And Ingresar Ffecha Ingreso al  "<fechaIngresoAl>"
    And Seleccionar Boton Buscar
    Examples:
      | grupo | fechaIngresoDel | fechaIngresoAl |
      #|Hipotecario|13/03/2021   |13/04/2021      |
      |Personal|13/03/2021   |13/04/2021      |



  @CreditosRelacionados
  Scenario Outline: Creditos Relacionados
    Given Se quiere realizar una operacion de "Creditos Relacionados"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos Relacionados
    And Seleccionar Boton Nuevo
    And Ingresar los Datos de la Relacion "<grupoPrincipal>" "<grupoAsociado>" "<nroCreditoPrincipal>" "<nroCreditoAsociado>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro Relacionado "<nroCreditoPrincipal>"
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado
    Examples:
      | grupo | grupoPrincipal | grupoAsociado | nroCreditoPrincipal | nroCreditoAsociado |
      |Hipotecario|CREDITOS HIPOTECARIOS|CREDITOS HIPOTECARIOS|39405|39407|



    @ModificacionHipotecarioSOX
  Scenario Outline: Modificacion Hipotecario SOX
    Given Se quiere realizar una operacion de "Modificacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Modificacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar el monto del precio de venta "<montoDesembolso>"
    And Ingresar el monto del valor comercial "<montoDesembolso>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Ingresar el plazo "<plazo>"
    And ingresar Seguro del Bien "<montoDesembolso>"
    And ingresar Motivo del Cambio
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|TasaInteres|plazo|montoDesembolso|
      |Hipotecario|31405|16|180         |78875.00|


  @ModificacionPersonalSOX
  Scenario Outline: Modificacion Personal SOX
    Given Se quiere realizar una operacion de "Modificacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion de Modificacion
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Ingresar el plazo "<plazo>"
    And ingresar Motivo del Cambio
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    #And Seleccionar Registro de la Grilla
    #And Eliminar Operacion
    #And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|TasaInteres|plazo|
      |Personal|41331|16|60         |



  @AjusteHipotecarioSOX
  Scenario Outline: Ajuste Hipotecario SOX
    Given Se quiere realizar una operacion de "Ajuste"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Ajuste
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And ingresar Ajuste Desde la Cuota "<AjusteCuota>"
    And Cambiar el valor de la tasa "<TasaInteres>"
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo|nroCredito|AjusteCuota|TasaInteres|
      |Hipotecario|42204|123|19|
      |Personal|42098|3|19|



  @InclusionCuotaMiTaxiSOX
  Scenario Outline: Inclusion Cuota Mi Taxi SOX
    Given Se quiere realizar una operacion de "Inclusion Cuota Mi Taxi"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Creditos
    And Seleccionar la Opcion Inclusion Cuota Mi Taxi
    And Seleccionar Boton Nuevo
    And Ingresar credito "<nroCredito>"
    And Ingresar Cuota Cero "<nroCuotaCero>"
    And Ingresar Nro de Cuotas "<nroCuotas>"
    And Ingresar Observacion
    And Guardar Operacion
    And Verificar que la operacion se haya realizado con exito
    And Seleccionar Registro de la Grilla
    And Eliminar Operacion
    And Verificar que la operacion se haya eliminado

    Examples:
      |grupo |nroCredito|nroCuotaCero|nroCuotas|
      |Personal|41331|7|1|



  @ConfirmacionCofideSOX
  Scenario Outline: Confirmacion Cofide SOX
    Given Se quiere realizar una operacion de "Confirmacion Cofide"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion Adeudado
    And Seleccionar la Opcion Confirmacion Cofide
    And Ingresar credito "<nroCredito>"
    And Ingresar Fecha del Proceso Del "<Fecha>"
    And Seleccionar Boton Buscar
    And Seleccionar registro Grilla Confirmacion Cofide
    And Seleccionar Boton Abrir
    And Ingresar Fecha Liquidacion "<FechaLiquidacion>"
    And Ingresar Observacion Confirmacion Cofide
    And Guardar Operacion


    Examples:
      | grupo | nroCredito | Fecha |FechaLiquidacion|
      | Hipotecario | 38456 | 13/05/2021 | 15/05/2021|


  @AsignacionDeBonosSOX
  Scenario Outline: Asignacion de Bonos SOX
    Given Se quiere realizar una operacion de "Asignacion de Bonos"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion de Asignacion de Bonos
    And Seleccionar credito de la grilla
    And Seleccionar Boton Abrir
    And Ingresar credito "<nroCredito>"
    And Seleccionar Check del registro
    And Guardar Operacion
    Examples:
      | grupo | nroCredito |
      | Hipotecario | 00038796 |


  @TitulizacionDeCreditosSOX
  Scenario Outline: Titulizacion de Creditos SOX
    Given Se quiere realizar una operacion de "Titulizacion de Creditos"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Operaciones
    And Seleccionar la Opcion de Titulizacion de Creditos
    And Seleccionar Entidad "<entidad>"
    And Seleccionar Patrimonio
    And Ingresar NroPortafolio "<nroPortafolio>"
    And Seleccionar Boton Buscar
    And Seleccionar credito de la grilla
    And Seleccionar Boton Abrir
    And Ingresar credito "<nroCredito>"
    And Seleccionar Tipo Desasignacion "<tipoDesasignacion>"
    And Guardar Operacion
    Examples:
      | grupo | entidad | nroPortafolio | nroCredito | tipoDesasignacion |
      | Hipotecario | TITULIZADORA PERUANA |1| 40481 | RECOMPRA |
     # | Hipotecario | TITULIZADORA PERUANA | PATRIMONIO 1 | 1 | nroCredito | SUSTITUCION |



