@ServicioObtenerDatosPrepago
Feature: ServicioObtenerDatosPrepago

  @ServicioObtenerDatosPrepago
  Scenario Outline: Servicio Obtener Datos Prepago
    Given Se quiere realizar una operacion de "Servicio Obtener Datos Prepago"
    When La operacion se realiza por administrativo
    Then Ingresar a la url del servicio Obtener Datos Prepago
    And Ingresar numero de credito "<nroCredito>"
    And Invocar servicio Obtener Datos Prepago

    Examples:
      |nroCredito |
      |39680|
      |39019|
      |38288|
      |36301|
      |36299|
      |30277|
      |20957|
      |18490|
      |18489|
      |5594|
      |5574|


