@JCC
Feature: JCC

  @JCCNuevaOperacion
  Scenario Outline: JCC Nueva Operacion
    Given Se quiere realizar una operacion de "Nueva Operacion JCC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Ingresar Operacion
    And Seleccionar Nuevo registro
    And Ingresar codigo del producto "<codProducto>"
    And Ingresar codigo unico del cliente "<codUnicoCliente>"
    And Seleccionar el tipo de moneda "<tipoMoneda>"
    And Ingresar capital origen "<capitalOrigen>"
    And Ingresar interes compensatorio origen "<intCompOrigen>"
    And Ingresar interes compensatorio vencido origen "<intCompVencOrigen>"
    And Ingresar interes moratorio origen "<intMoratorio>"
    And Ingresar interes diferido origen "<intDifOrigen>"
    And Ingresar tasa interes compensatorio origen "<tasaIntCompOrigen>"
    And Ingresar tasa interes compensatorio vencido origen "<tasaIntCompVencOrigen>"
    And Ingresar tasa interes compensatorio vencido "<tasaIntCompVenc>"
    And Ingresar tasa interes moratorio "<tasaIntMoratorio>"
    And Ingresar gastos origen "<gastosOrigen>"
    And Ingresar seguros origen "<segurosOrigen>"
    And Ingresar comision origen "<comisionOrigen>"
    And Ingresar fecha inicio calculo "<fechaInicioCalculo>"
    And Ingresar fecha desembolso "<fechaDesembolso>"
    And Ingresar fecha vencimiento "<fechaVencimiento>"
    And Ingresar codigo estudio de abogados "<codEstudioAbog>"
    And Seleccionar aplicativo origen "<aplicativoOrigen>"
    And Ingresar numero de Operacion Anterior "<nroOperacionAnterior>"
    And Ingresar tienda de colocacion "<tdaColocacion>"
    And Guardar nuevo registro
    #And Seleccionar operacion en la grilla
    #And Eliminar registro

    Examples:
      |grupo |codProducto|codUnicoCliente|tipoMoneda|capitalOrigen|intCompOrigen|intCompVencOrigen|intMoratorio|intDifOrigen|tasaIntCompOrigen|tasaIntCompVencOrigen|tasaIntCompVenc|tasaIntMoratorio|gastosOrigen|segurosOrigen|comisionOrigen|fechaInicioCalculo|fechaDesembolso|fechaVencimiento|codEstudioAbog|aplicativoOrigen|nroOperacionAnterior|tdaColocacion|
      #|BC    |214        |60072850       |PEN       |3000         |100          |120              |50          |20          |8                |10                   |12             |15              |110         |300          |150           |24/03/2021        |05/05/2020     |05/12/2020      |1             |HPC             |12342                |100          |
      |BC    |502        |60089285       |PEN       |37417.68     |9515.74      |100              |1713.5      |12          |0                |44.92                |44.92          |19.56           |100         |120          |10            |24/03/2021        |07/01/2020     |02/03/2021      |1             |LPC             |23144                |100          |
      |BC    |502        |60089285       |PEN       |3000         |100          |120              |50          |20          |8                |10                   |12             |15              |110         |300          |150           |24/03/2021        |05/02/2020     |05/12/2020      |1             |HPC             |12323                |100          |
      |BC    |502        |50000127       |USD       |37417.68     |9515.74      |100              |1713.5      |11          |0                |44.92                |44.92          |19.56           |80          |250          |120           |24/03/2021        |07/01/2020     |02/03/2021      |1             |FCD             |23112                |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |HPC             |23111                |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |HPC             |231112               |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |LPC             |2311123              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |HPC             |2311124              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |FCD             |2311111              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |HPC             |2311156              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |HPC             |2311178              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |FCD             |2311122              |100          |
      |MP    |806        |60089285       |PEN       |37417.68     |4515.74      |25               |1713.5      |5           |0                |44.92                |44.92          |19.56           |50          |120          |100           |24/03/2021        |07/03/2020     |02/03/2021      |1             |LPC             |2311189              |100          |


  @JCCPagoEstandar
  Scenario Outline: JCC Pago Estandar
    Given Se quiere realizar una operacion de "Pago Estandar JCC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Pago Estandar
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Ingresar el monto a pagar "<montoPagar>"
    And Seleccionar el motivo del pago estandar "<motivoPago>"
    And Ingresar texto en observacion
    And Guardar registro
    And Confirmar operacion
    And Seleccionar operacion en la grilla
    #And Eliminar registro
    #And Confirmar operacion

    Examples:
      |grupo |nroOperacion|montoPagar|motivoPago|
      #|BP|46153961|30000|DISMINUCION POR PAGO REESTRUC.|
      #|BP|46024999|45000|DISMINUCION PAGO A CUENTA|
      #|BC|46113913|18000|DISMINUCION PAGO A CUENTA|
      #|BP|46000175|9000 |DISMINUCION POR PAGO REESTRUC.|
      #|BP|46000081|6000 |DISMINUCION POR PAGO REESTRUC.|
      #|BP|46025979|8000 |DISMINUCION POR PAGO REESTRUC.|
      #|BP|46024864|13000|DISMINUCION POR PAGO REESTRUC.|
      #|BP|46071286|35000|DISMINUCION POR PAGO REESTRUC.|
      #|BP|46071786|35000.23|DISMINUCION POR PAGO REESTRUC.|
      #|BP|46105000|2000|DISMINUCION POR PAGO REESTRUC.|
      #|BP|46071545|35000.34|DISMINUCION POR PAGO REESTRUC.|
      |BP|46072294|45000|DISMINUCION POR PAGO REESTRUC.|
      |BP|46071926|40000.34|DISMINUCION POR PAGO REESTRUC.|
      |BP|46072053|55000|DISMINUCION POR PAGO REESTRUC.|
      |BP|46025574|22000.12|DISMINUCION POR PAGO REESTRUC.|

  @JCCPagoEstandarCancelacion
  Scenario Outline: JCC Pago Estandar Cancelacion
    Given Se quiere realizar una operacion de "Pago Estandar JCC Cancelacion"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Pago Estandar
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Ingresar el monto a pagar
    And Seleccionar el motivo del pago estandar "<motivoPago>"
    And Ingresar texto en observacion
    And Guardar registro
    And Confirmar operacion
    And Seleccionar operacion en la grilla
    #And Eliminar registro
    #And Confirmar operacion

    Examples:
      |grupo |nroOperacion|motivoPago|
      #|BP|46024823|CANCELC PAGO SEG/POR SINIESTRO|
      #|BC|46167543|CANCELC PAGO SEG/POR SINIESTRO|
      #|BC|46167559|CANCELC PAGO SEG/POR SINIESTRO|
      #|MP|46024873|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46020529|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46025961|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46000274|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46103869|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071470|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46073272|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072160|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071233|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46076383|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072230|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072110|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071804|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071001|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071997|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071316|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071320|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071261|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072319|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46025475|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46094968|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071459|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071461|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46095374|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46113076|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46102839|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46072831|CANCELC PAGO SEG/POR SINIESTRO|


  @JCCPagoEspecial
  Scenario Outline: JCC Pago Especial
    Given Se quiere realizar una operacion de "Pago EspecialJCC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Pago Especial
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Seleccionar el motivo del pago especial "<motivoPago>"
    And Ingresar texto en observacion del Pago Especial
    And Ingresar Capital "<Capital>"
    And Ingresar Monto Interes Compensatorio "<InteresCompensatorio>"
    And Ingresar Monto Interes Moratorio "<InteresMoratorio>"
    And Ingresar Monto Gastos "<Gastos>"
    And Ingresar Monto Gastos Protesto "<GastosProtesto>"
    And Ingresar Monto Seguros "<Seguros>"
    And Ingresar Monto Honorarios "<Honorarios>"
    And Ingresar Monto Comisiones "<Comisiones>"
    And Guardar registro
    And Confirmar operacion
    And Seleccionar registro en la grilla
    #And Eliminar registro
    #And Confirmar operacion

    Examples:
        |grupo |nroOperacion|motivoPago|Capital|InteresCompensatorio|InteresMoratorio|Gastos|GastosProtesto|Seguros|Honorarios|Comisiones|
        #|BP|46095876|DISMINUCION PAGO A CUENTA	|10000|	5000|	1000|	500|	0|	100|	0|	2000|
        #|BP|46000138|DISMINUCION POR PAGO REESTRUC.	|400|	3000|	400|	10|	0|	10|	0	|100|
        #|BC|46024806|DISMINUCION PAGO A CUENTA	|0|	300|	200|	200|	0|	0|	100|	0|
        #|MP|46024817|DISMINUCION PAGO A CUENTA	|15000|	35000|	35000|	1000|	0|	50|	0|	1400|
        #|BP|46000158|DISMINUCION PAGO A CUENTA	|300|	2000|	300|	20|	0|	10|	0|	100|
        #|BP|46154406|DISMINUCION PAGO A CUENTA	|15000|	5000|	0|	0|	0|	100|	0|	200|
        #|BP|46025912|DISMINUCION PAGO A CUENTA	|10|1|	0|	0|	0|	0|	0|	0|
        #|BP|46026218|DISMINUCION PAGO A CUENTA	|20|	1|	0|	1|	0|	0|	0|	0|
        #|BP|46072897|DISMINUCION PAGO A CUENTA	|5000|	15000|	0|	300|	0|	300|	0|	300|
        #|BP|46072281|DISMINUCION PAGO A CUENTA	|6000|	22000|	0|	600|	0|	300|	0|	200|
        #|BP|46074703|DISMINUCION PAGO A CUENTA	|7000|	12000|	0|	300|	0|	200|	0|	200|
        #|BP|46074954|DISMINUCION PAGO A CUENTA	|12000|	19000|	0|	350|	0|	200|	0|	200|
        #|BP|46075145|DISMINUCION PAGO A CUENTA	|12000|	19000|	0|	500|	0|	200|	0|	200|
        #|BP|46072731|DISMINUCION PAGO A CUENTA	|15000|	19000|	0|	800|	0|	300|	0|	300|
        #|BP|46103767|DISMINUCION PAGO A CUENTA	|100|	30|	0|	100|	0|	0|	0|	0|
        #|BP|46073792|DISMINUCION PAGO A CUENTA	|5000|	14000|	0|	4000|	0|	300|	0|	300|
        #|BP|46071193|DISMINUCION PAGO A CUENTA	|4000|	19000|	0|	900|	0|	400|	0|	200|
        #|BP|46071569|DISMINUCION PAGO A CUENTA	|9000|	12000|	0|	500|	0|	200|	0|	200|
        #|BP|46070841|DISMINUCION PAGO A CUENTA	|3000|	1000|	300|	300|	0|	100|	0|	200|
        #|BP|46075854|DISMINUCION PAGO A CUENTA	|6000|	6000|	300|	600|	0|	300|	0|	300|
        #|BP|46075982|DISMINUCION PAGO A CUENTA	|7000|	16000|	300|	300|	0|	300|	0|	300|
        #|BP|46095659|DISMINUCION PAGO A CUENTA	|2000|	1300|	300|	400|	0|	200|	0|	300|
        #|BP|46073632|DISMINUCION PAGO A CUENTA	|9000|	13000|	300|	200|	0|	400|	0|	450|
        |BP|46096238|DISMINUCION PAGO A CUENTA	|1000|	300|	200|	100|	0|	100|	0|	200|
        |BP|46071402|DISMINUCION PAGO A CUENTA	|4000|	13000|	0|	200|	0|	200|	0|	200|
        |BP|46073080|DISMINUCION PAGO A CUENTA	|5000|	15000|	0|	200|	0|	200|	0|	200|



  @JCCPagoEspecialCancelacion
  Scenario Outline: JCC Pago Especial Cancelacion
    Given Se quiere realizar una operacion de "Pago Especial Cancelacion JCC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Pago Especial
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Seleccionar el motivo del pago especial "<motivoPago>"
    And Ingresar texto en observacion del Pago Especial
    And Ingresar Monto Capital
    And Ingresar Interes Compensatorio
    And Ingresar Interes Moratorio
    And Ingresar Gastos
    And Ingresar Gastos Protesto
    And Ingresar Seguros
    And Ingresar Honorarios
    And Ingresar Comisiones
    And Guardar registro
    And Confirmar operacion
    And Seleccionar registro en la grilla
    #And Eliminar registro
    #And Confirmar operacion

    Examples:
      |grupo |nroOperacion|motivoPago|
      #|BP|46095945|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46000107|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46020394|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46026010|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46103647|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072837|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46074701|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46074208|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46073902|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46025376|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071617|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46073666|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46073298|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46072807|CANCELC PAGO SEG/POR SINIESTRO|
      #|BP|46071384|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46071636|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46070907|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46074186|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46095752|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46095643|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46095123|CANCELC PAGO SEG/POR SINIESTRO|
      |BP|46072830|CANCELC PAGO SEG/POR SINIESTRO|


  @JCCIncremento
  Scenario Outline: JCC Incremento
    Given Se quiere realizar una operacion de "Incremento"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Incrementos
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Seleccionar el motivo del pago especial "<motivoPago>"
    And Ingresar texto en observacion del Pago Especial
    And Ingresar Capital "<Capital>"
    And Ingresar interes compensatorio origen "<intCompOrigen>"
    And Ingresar Monto interes compensatorio vencido origen "<intCompVencOrigen>"
    And Ingresar Monto Interes Compensatorio "<InteresCompensatorio>"
    And Ingresar Monto interes moratorio origen "<intMoratorio>"
    And Ingresar Monto Interes Moratorio "<InteresMoratorio>"
    And Ingresar Monto Interes Diferido "<InteresDiferido>"
    And Ingresar Monto gastos origen "<gastosOrigen>"
    And Ingresar Monto seguros origen "<segurosOrigen>"
    And Ingresar Monto Comisiones "<Comisiones>"
    And Guardar registro
    And Confirmar operacion
    #And Seleccionar registro en la grilla
    #And Eliminar registro
    #And Confirmar operacion

    Examples:
      |grupo |nroOperacion|motivoPago|Capital|intCompOrigen|intCompVencOrigen|InteresCompensatorio|intMoratorio|InteresMoratorio|InteresDiferido|gastosOrigen|segurosOrigen|Comisiones|
      #|BC|46113947|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113948|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113949|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113954|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113956|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113957|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113963|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113965|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113966|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113971|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113972|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113973|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113974|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113979|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113980|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113981|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113982|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113988|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113989|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113990|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113991|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113996|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113997|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113998|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113999|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114005|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114006|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114007|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114013|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114014|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114015|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114016|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114021|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114022|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114023|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114024|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113994|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113995|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114000|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114001|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114002|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114003|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114008|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114009|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114010|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114011|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114012|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114017|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114018|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114019|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114020|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114025|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114026|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46114027|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113917|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113918|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113919|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113920|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113925|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113927|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113928|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113933|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113934|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113935|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113936|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113942|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113943|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113944|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113945|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113950|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113951|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113952|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113953|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113958|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113959|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113961|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113962|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113967|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113968|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113969|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113970|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113975|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113976|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113977|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113984|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113985|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113986|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113987|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113993|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46024806|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113909|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113910|MOTIVO INCREMENTO 1|1009|100|100.99|100|300|300|300|200|200|200|
      |BC|46113911|MOTIVO INCREMENTO 2|1009|100|100.99|100|300|300|300|200|200|200|



  @JCCModificaciones
  Scenario Outline: JCC Modificaciones
    Given Se quiere realizar una operacion de "Modificaciones"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opción Modificaciones
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Ingresar valor de Honorarios "<honorarios>"
    And Guardar registro
    And Confirmar operacion

    Examples:
      |grupo |nroOperacion|honorarios|
      #|BP|46025177|20|
      #|BP|46095876|30|
      #|BP|46153961|30|

  @JCCAnulacionPago
  Scenario Outline: JCC Anulacion Pago
    Given Se quiere realizar una operacion de "Anulacion Pago JCC"
    When La operacion se realiza por administrativo
    Then Ingresar al SDA
    And Seleccionar el Grupo "<grupo>"
    And Seleccionar la Opcion Transacciones
    And Seleccionar la Opcion Anulacion de Pago
    And Seleccionar Nuevo registro
    And Ingresar numero de Operacion "<nroOperacion>"
    And Seleccionar el motivo del pago especial "<motivoPago>"
    And Ingresar texto en observacion del Pago Especial
    And Seleccionar Pagos a Anular
    Examples:
      |grupo |nroOperacion|motivoPago|
      |BP|46070960|MOTIVO ANULACION PAGO 1|