@ServicioPrepagoDigital
Feature: ServicioPrepagoDigital

  @ServicioSimulacionPrepagoDigital
  Scenario Outline: Servicio Simulacion Prepago Digital
    Given Se quiere realizar una operacion de "Servicio Simulacion Prepago Digital"
    When La operacion se realiza por administrativo
    Then Ingresar a la url del servicio Simulacion Prepago Digital
    And Ingresar numero de credito a simular "<nroCredito>"
    And Ingresar periodo de gracia "<periodoGracia>"
    And Ingresar monto de prepago "<montoPrepago>"
    And Invocar servicio

    Examples:
      |nroCredito |periodoGracia|montoPrepago|
      |39680|0|1550.675|
      |39019|0|2621.5|
      |38288|0|3154|
      |36301|0|5993.65|
      |36299|0|5160.975|
      |30277|0|5112.675|
      |20957|0|4697.15|
      |18490|0|627.55|
      |18489|0|556.35|
      |5594|0|1295.475|
      |5574|0|1096.475|
