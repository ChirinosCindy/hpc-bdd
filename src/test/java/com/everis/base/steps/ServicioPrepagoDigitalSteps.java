package com.everis.base.steps;

import com.everis.base.PageObject.ServicioPrepagoDigitalPage;
import com.everis.base.Utils.Util;
import com.everis.base.commons.Constant;
import com.sun.tools.jxc.ap.Const;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class ServicioPrepagoDigitalSteps {

    private static final Logger logger = LoggerFactory.getLogger(ServicioPrepagoDigitalSteps.class);
    private ServicioPrepagoDigitalPage servicioPrepagoDigitalPage;
    private Util util;

    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ServicioPrepagoDigitalSteps() {
        this.util = new Util();
    }

    @Step

    public void ingresarALaUrlDelServicioSimulacionPrepagoDigital() {
        servicioPrepagoDigitalPage.open();
        logger.info("Se ingreso al servicio Simulacion Prepago Digital");
    }

    public void ingresarNumeroDeCreditoASimular(String nroCredito) {
        servicioPrepagoDigitalPage.nroCredito.sendKeys(nroCredito);
        Constant.numCredito = nroCredito;
    }

    public void ingresarPeriodoDeGracia(String periodoGracia) {
        servicioPrepagoDigitalPage.periodoGracia.sendKeys(periodoGracia);
    }

    public void ingresarMontoDePrepago(String montoPrepago) {
        servicioPrepagoDigitalPage.montoPrepago.sendKeys(montoPrepago);
    }

    public void invocarServicio() throws IOException {
        servicioPrepagoDigitalPage.txtNombreServicio.click();
        util.tomarEvidenciaSimulacionPrepagoDigital("Request");

        servicioPrepagoDigitalPage.btnInvoke.click();
        pause(1);
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(1));
        pause(1);
        getDriver().manage().window().maximize();

        String codigoError = servicioPrepagoDigitalPage.txtCodError.getTextValue();
        util.tomarEvidenciaSimulacionPrepagoDigital("Response");

        if(codigoError.equals("0")){
            util.renombrarCarpeta();
        }
        else{
            String mensajeError = servicioPrepagoDigitalPage.txtMensajeError.getTextValue();
            logger.info("Codigo error: " + codigoError);
            logger.info("Mensaje error: " + mensajeError);
            util.crearLog(codigoError + " " + mensajeError);
        }

        getDriver().close();
        getDriver().switchTo().window(tabs2.get(0));
    }

}