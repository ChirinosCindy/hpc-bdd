package com.everis.base.steps;

import com.everis.base.PageObject.GeneralPage;
import com.everis.base.Utils.Util;
import com.everis.base.commons.Constant;
import net.thucydides.core.annotations.Step;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class GeneralSteps {

    private static final Logger logger = LoggerFactory.getLogger(GeneralSteps.class);
    private GeneralPage generalPage;
    private Util util;
    public int codGrupo = 0;

    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public GeneralSteps() {
        this.util = new Util();
    }

    @Step

    //LOGIN
    public void openSDA() {
        generalPage.open();
        logger.info("Se ingreso al SDA");
    }

    public void RegistroLogin() {
        generalPage.txtUserLogin.sendKeys(Constant.User);
        logger.info("Se ingreso el registro");
    }

    public void ContraLogin() {
        generalPage.txtPassLogin.sendKeys(Constant.Password);
        logger.info("Se ingreso la contraseña");
    }

    public void seleccionarAmbienteDePrueba() {
        generalPage.cbAmbienteLogin.selectByVisibleText(Constant.Ambiente);
        logger.info("Se selecciono el ambiente de pruebas");
    }

    public void AceptarLogin() {
        generalPage.btnAceptarLogin.click();
        logger.info("Se dio click en el boton Aceptar");
    }

    public void ingresarAlSDAconUsuarioAdmin() {
        generalPage.open();
        generalPage.txtUserLogin.sendKeys(Constant.User_ADMIN);
        generalPage.txtPassLogin.sendKeys(Constant.Password_ADMIN);
        generalPage.cbAmbienteLogin.selectByVisibleText(Constant.Ambiente);
        generalPage.btnAceptarLogin.click();
        logger.info("Se realizo Login con usuario Admin");
    }

    //INGRESAR A GRUPO

    public void clickGrupo(String Grupo) {
        switch (Grupo.toUpperCase()) {
            case "HIPOTECARIO":
                generalPage.txtHipotecario.click();
                Constant.codGrupo = "1";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Hipotecario");
                break;
            case "PPGH":
                generalPage.txtPPGH.click();
                Constant.codGrupo = "2";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo PPGH");
                break;
            case "PERSONAL":
                generalPage.txtPersonal.click();
                Constant.codGrupo = "3";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Personal");
                break;
            case "VEHICULAR":
                generalPage.txtVehicular.click();
                Constant.codGrupo = "4";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Vehicular");
                break;
            case "REFINANCIADO":
                generalPage.txtRefinanciado.click();
                Constant.codGrupo = "5";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Refinanciado");
                break;
            case "BP":
                generalPage.JCC_BP_OPE.click();
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo JCC BP OPE");
                break;
            case "BC":
                generalPage.JCC_BC_OPE.click();
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo JCC BC OPE");
                break;
            case "MP":
                generalPage.JCC_MP_OPE.click();
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo JCC MP OPE");
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + Grupo.toUpperCase());
        }
        pause(2);

    }


    /*Upgrade*/


    public void clickGrupoUpgrade(String Grupo) {
        switch (Grupo.toUpperCase()) {
            case "HIPOTECARIO":
                generalPage.txtHipotecarioUpgrade.click();
                Constant.codGrupo = "1";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Hipotecario");
                break;
            case "PPGH":
                generalPage.txtPPGHUpgrade.click();
                Constant.codGrupo = "2";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo PPGH");
                break;
            case "PERSONAL":
                generalPage.txtPersonalUpgrade.click();
                Constant.codGrupo = "3";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Personal");
                break;
            case "VEHICULAR":
                generalPage.txtVehicularUpgrade.click();
                Constant.codGrupo = "4";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Vehicular");
                break;
            case "REFINANCIADO":
                generalPage.txtRefinanciadoUpgrade.click();
                Constant.codGrupo = "5";
                Constant.nombreGrupo = Grupo;
                logger.info("Se ingreso al grupo Refinanciado");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + Grupo.toUpperCase());
        }
        pause(2);

    }



    public void seleccionarElGrupoAdmin(String grupo) {
        generalPage.txtHipotecarioADM.click();
        logger.info("click en grupo Hipotecario perfil ADM");
    }

    public void seQuiereRealizarUnaOperacionDe(String nombreCarpetaEvidencias) {
        util.crearCarpetaEvidencias(nombreCarpetaEvidencias);
    }

}