package com.everis.base.steps;

import com.everis.base.PageObject.JCCPage;
import com.everis.base.Utils.Util;
import com.everis.base.commons.Constant;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class JCCSteps {

    private static final Logger logger = LoggerFactory.getLogger(JCCSteps.class);
    private JCCPage jccPage;
    public static WebDriver driver;
    public int codGrupo = 0;
    private Util util;


    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public JCCSteps(){
        this.util = new Util();
    }

    @Step

    public void seleccionarLaOpcionTransacciones() {
        jccPage.opcTransacciones.click();
        logger.info("Se selecciono la opcion Transacciones");
    }

    public void seleccionarLaOpcionPagoEstandar() {
        jccPage.opcPagoEstandar.click();
        cambioFrameJCC();
        logger.info("Se selecciono la opcion Pago Estandar");
    }

    public void cambioFrameJCC(){
        jccPage.iFrame.waitUntilEnabled();
        getDriver().switchTo().frame("mainFrame");

        Actions actions = new Actions(getDriver());
        actions.moveToElement(jccPage.btnCerrarRegistro).perform();//Hover
    }

    public void seleccionarNuevoRegistro() throws IOException {
        jccPage.btnNuevoRegistro.click();
        util.tomarEvidencia("Nuevo registro");
        logger.info("Click boton nuevo registro JCC");
    }

    public void ingresarNumeroDeOperacion(String nroOperacion) throws IOException {
        Constant.numCredito = nroOperacion;
        jccPage.txtNroOperacion.click();
        jccPage.txtNroOperacion.sendKeys(nroOperacion);
        jccPage.txtNroOperacion.sendKeys(Key.ENTER);
        pause(3);
        util.tomarEvidencia("Se ingreso nro Operacion");
        logger.info("Se ingreso el número de Operacion "  + nroOperacion);
    }

    public void ingresarElMontoAPagar(String montoPagar) throws IOException {
        jccPage.txtMontoPagar.click();
        jccPage.txtMontoPagar.clear();
        jccPage.txtMontoPagar.sendKeys(montoPagar);

        util.tomarEvidencia("Monto a pagar");
        logger.info("Se ingreso el monto a pagar " + montoPagar);
    }

    public void ingresarElMontoAPagar() throws IOException {
        jccPage.txtImporteCancelacion.click();
        String importeCancelacion = jccPage.txtImporteCancelacion.getValue().replace(",","");
        logger.info("Importe de cancelacion : " + importeCancelacion);

        jccPage.txtMontoPagar.click();
        jccPage.txtMontoPagar.clear();
        jccPage.txtMontoPagar.sendKeys(importeCancelacion);

        util.tomarEvidencia("Monto a pagar");
        logger.info("Se ingreso el monto a pagar " + importeCancelacion);
    }

    public void seleccionarElMotivoDelPagoEstandar(String motivoPago) throws IOException {
        jccPage.cmbMotivoPago.click();
        jccPage.cmbMotivoPago.selectByVisibleText(motivoPago);
        util.tomarEvidencia("Motivo de pago");
        logger.info("Se selecciono el motivo de pago " + motivoPago);
        pause(5);
    }

    public void ingresarTextoEnObservacion() throws IOException {
        jccPage.txtObservaciones.click();
        jccPage.txtObservaciones.sendKeys("prueba");
        util.tomarEvidencia("Texto observacion");
        logger.info("Se ingreso observacion");
    }

    public void guardarRegistro() {
        jccPage.btnGuardarRegistro.click();
        logger.info("Se selecciono el boton Guardar");
    }

    public void guardarNuevoRegistro() throws IOException {
        jccPage.btnGuardarRegistro.click();
        logger.info("Se selecciono el boton Guardar");

        util.tomarEvidencia("Mensaje de confirmacion");
        jccPage.contenedorMensajeConfirm.click();

        jccPage.alertSi2.click();
        pause(2);

        util.tomarEvidencia("Numero Operacion generado");

        jccPage.alertAceptar2.click();
        util.tomarEvidencia("Operacion confirmada");

        logger.info("Se confirmo operacion");
    }

    public void ingresarObservacion() throws IOException {
        jccPage.txtObservacion.click();
        jccPage.txtObservacion.sendKeys(Constant.InclusionCuotaFleixble_Comentario);

        util.tomarEvidencia("Se ingresa Comentario");

    }

    public void seleccionarRegistroEnLaGrilla() throws IOException {
        jccPage.registroGrillaPagoEspecial.click();
        util.tomarEvidencia("Seleccion registro");
        logger.info("Se selecciono el registro en la grilla");
    }

    public void seleccionarOperacionEnLaGrilla() throws IOException {
        jccPage.registroGrillaJCC.click();
        util.tomarEvidencia("Seleccion registro");
        logger.info("Se selecciono el registro en la grilla");
    }

    public void eliminarRegistro() {
        jccPage.btnEliminarRegistro.click();
        logger.info("Se selecciono el boton eliminar");
    }

    public void confirmarOperacion() throws IOException {
        util.tomarEvidencia("Mensaje de confirmacion");
        jccPage.contenedorMensajeConfirm.click();

        jccPage.alertSi.click();
        pause(2);
        jccPage.alertAceptar.click();
        util.tomarEvidencia("Operacion confirmada");
        util.renombrarCarpeta();
        logger.info("Se confirmo operacion");
    }

    public void seleccionarLaOpcionIngresarOperacion() {
        jccPage.opcIngresarOperacion.click();
        cambioFrameJCC();
    }

    public void ingresarCodigoDelProducto(String codProducto) {
        jccPage.txtCodigoProducto.click();
        jccPage.txtCodigoProducto.clear();
        jccPage.txtCodigoProducto.sendKeys(codProducto);
        logger.info("Se ingreso codigo del producto");
    }

    public void ingresarCodigoUnicoDelCliente(String codUnicoCliente) {
        jccPage.txtCUCliente.click();
        jccPage.txtCUCliente.sendKeys(codUnicoCliente);
        logger.info("Se ingreso CU del cliente");
    }

    public void seleccionarElTipoDeMoneda(String tipoMoneda) {
        jccPage.cmbTipoMoneda.waitUntilEnabled().click();
        jccPage.cmbTipoMoneda.selectByVisibleText(tipoMoneda);
        logger.info("Se selecciono el tipo de moneda");
    }

    public void ingresarCapitalOrigen(String capitalOrigen) {
        jccPage.txtCapitalOrigen.click();
        jccPage.txtCapitalOrigen.sendKeys(capitalOrigen);
        logger.info("Se ingreso capital origen");
    }

    public void ingresarInteresCompensatorioOrigen(String intCompOrigen) {
        jccPage.txtInteresCompensatorioOrigen.click();
        jccPage.txtInteresCompensatorioOrigen.sendKeys(intCompOrigen);
        logger.info("Se ingreso interes compensatorio origen");
    }

    public void ingresarInteresCompensatorioVencidoOrigen(String intCompVencOrigen) {
        jccPage.txtInteresCompensatorioVencidoOrigen.click();
        jccPage.txtInteresCompensatorioVencidoOrigen.sendKeys(intCompVencOrigen);
        logger.info("Se ingreso interes compensatorio vencido origen");
    }

    public void ingresarInteresMoratorioOrigen(String intMoratorio) {
        jccPage.txtInteresMoratorioOrigen.click();
        jccPage.txtInteresMoratorioOrigen.sendKeys(intMoratorio);
        logger.info("Se ingreso interes moratorio origen");
    }

    public void ingresarInteresDiferidoOrigen(String intDiferidoOrigen) {
        jccPage.txtInteresDiferidoOrigen.click();
        jccPage.txtInteresDiferidoOrigen.sendKeys(intDiferidoOrigen);
        logger.info("Se ingreso interes diferido origen");
    }

    public void ingresarTasaInteresCompensatorioOrigen(String tasaIntCompOrigen) {
        jccPage.txtTasaInteresCompensatorioOrigen.click();
        jccPage.txtTasaInteresCompensatorioOrigen.sendKeys(tasaIntCompOrigen);
        logger.info("Se ingreso tasa interes compensatorio origen");
    }

    public void ingresarTasaInteresCompesnsatorioVencidoOrigen(String tasaIntCompVencOrigen) {
        jccPage.txtTasaInteresCompensatorioVencidoOrigen.click();
        jccPage.txtTasaInteresCompensatorioVencidoOrigen.sendKeys(tasaIntCompVencOrigen);
        logger.info("Se ingreso tasa interes compensatorio vencido origen");
    }

    public void ingresarTasaInteresCompensatorioVencido(String tasaIntCompVenc) {
        jccPage.txtTasaInteresCompensatorioVencido.click();
        jccPage.txtTasaInteresCompensatorioVencido.clear();
        jccPage.txtTasaInteresCompensatorioVencido.sendKeys(tasaIntCompVenc);
        logger.info("Se ingreso tasa interes compensatorio vencido");
    }

    public void ingresarTasaInteresMoratorio(String tasaIntMoratorio) {
        jccPage.txtTasaInteresMoratorio.click();
        jccPage.txtTasaInteresMoratorio.clear();
        jccPage.txtTasaInteresMoratorio.sendKeys(tasaIntMoratorio);
        logger.info("Se ingreso tasa interes moratorio");
    }

    public void ingresarGastosOrigen(String gastosOrigen) {
        jccPage.txtGastosOrigen.click();
        jccPage.txtGastosOrigen.sendKeys(gastosOrigen);
        logger.info("Se ingreso gastos origen");
    }

    public void ingresarSegurosOrigen(String segurosOrigen) {
        jccPage.txtSegurosOrigen.click();
        jccPage.txtSegurosOrigen.sendKeys(segurosOrigen);
        logger.info("Se ingreso seguros origen");
    }

    public void ingresarComisionOrigen(String comisionOrigen) {
        jccPage.txtComisionOrigen.click();
        jccPage.txtComisionOrigen.sendKeys(comisionOrigen);
        logger.info("Se ingreso comision origen");
    }

    public void ingresarFechaInicioCalculo(String fechaInicioCalculo) {

        jccPage.txtFechaInicioCalculo.click();
        jccPage.txtFechaInicioCalculo.clear();

        String fecha = fechaInicioCalculo.replace("/","");
        String[] numfecha = new String[fecha.length()];

        for (int i = 0; i < fecha.length(); i++){
            numfecha[i] = fecha.substring(i,i+1);
            jccPage.txtFechaInicioCalculo.sendKeys(numfecha[i]);
        }
        logger.info("Se ingreso fecha inicio calculo");
    }

    public void ingresarFechaDesembolso(String fechaDesembolso) {
        jccPage.txtFechaDesembolso.click();

        String fecha = fechaDesembolso.replace("/","");
        String[] numfecha = new String[fecha.length()];

        for (int i = 0; i < fecha.length(); i++){
            numfecha[i] = fecha.substring(i,i+1);
            jccPage.txtFechaDesembolso.sendKeys(numfecha[i]);
        }
        logger.info("Se ingreso fecha desembolso");
    }

    public void ingresarFechaVencimiento(String fechaVencimiento) {
        jccPage.txtFechaVencimiento.click();

        String fecha = fechaVencimiento.replace("/","");
        String[] numfecha = new String[fecha.length()];

        for (int i = 0; i < fecha.length(); i++){
            numfecha[i] = fecha.substring(i,i+1);
            jccPage.txtFechaVencimiento.sendKeys(numfecha[i]);
        }
        logger.info("Se ingreso fecha vencimiento");
    }

    public void ingresarCodigoEstudioDeAbogados(String codEstudioAbog) {
        jccPage.txtCodEstudioAbogados.click();
        jccPage.txtCodEstudioAbogados.clear();
        jccPage.txtCodEstudioAbogados.sendKeys(codEstudioAbog);
        logger.info("Se ingreso codigo estudio abogados");
    }

    public void seleccionarAplicativoOrigen(String aplicativoOrigen) {
        jccPage.cmbAplicativoOrigen.waitUntilEnabled().click();
        jccPage.cmbAplicativoOrigen.selectByVisibleText(aplicativoOrigen);
        logger.info("Se selecciono aplicativo origen");
    }

    public void ingresarNumeroDeOperacionAnterior(String nroOperacionAnterior) {
        jccPage.txtNroOperacionAnterior.click();
        //desembolsoPage.txtNroOperacionAnterior.clear();
        jccPage.txtNroOperacionAnterior.sendKeys(nroOperacionAnterior);
        logger.info("Se ingreso número de operacion anterior");
    }

    public void ingresarTiendaDeColocacion(String tdaColocacion) throws IOException {
        jccPage.txtTiendaColocacion.click();
        //desembolsoPage.txtTiendaColocacion.clear();
        jccPage.txtTiendaColocacion.sendKeys(tdaColocacion);
        util.tomarEvidencia("Datos operacion");
        logger.info("Se ingreso tienda de colocacion");
    }

    public void seleccionarLaOpcionPagoEspecial() {
        jccPage.opcPagoEspecial.click();
        cambioFrameJCC();
        logger.info("Se selecciono la opcion Pago Especial");
    }

    public void ingresarMontoCapital() {
        jccPage.saldoActual_capital.click();
        String saldoActualCapital = jccPage.saldoActual_capital.getText().replace(",","");
        logger.info("SaldoActualCapital " + saldoActualCapital);
        jccPage.montoCapital.click();
        jccPage.montoCapital.clear();
        jccPage.montoCapital.sendKeys(saldoActualCapital);
    }

    public void seleccionarElMotivoDelPagoEspecial(String motivoPago) throws IOException {
        jccPage.cmbMotivoPago.click();
        jccPage.cmbMotivoPago.selectByVisibleText(motivoPago);
        jccPage.cmbMotivoPago.click();
        util.tomarEvidencia("Motivo de pago");
        logger.info("Se selecciono el motivo de pago " + motivoPago);

    }

    public void ingresarTextoEnObservacionDelPagoEspecial() throws IOException {
        jccPage.txtObservacion.click();
        jccPage.txtObservacion.sendKeys("prueba");
        util.tomarEvidencia("Se ingreso Observacion");
    }

    public void ingresarInteresCompensatorio() throws IOException {
        jccPage.saldoActual_interesCompensatorio.click();
        String saldoActualInteresCompensatorio= jccPage.saldoActual_interesCompensatorio.getText().replace(",","");
        logger.info("saldoActualInteresCompensatorio " + saldoActualInteresCompensatorio);
        jccPage.montoInteresCompensatorio.click();
        jccPage.montoInteresCompensatorio.clear();
        jccPage.montoInteresCompensatorio.sendKeys(saldoActualInteresCompensatorio);
        util.tomarEvidencia("Se ingreso Interes Compensatorio");
        logger.info("Se ingreso el Interes Compensatorio" + saldoActualInteresCompensatorio);

    }

    public void ingresarInteresMoratorio() throws IOException {
        jccPage.saldoActual_interesMoratorio.click();
        String saldoActualInteresMoratorio= jccPage.saldoActual_interesMoratorio.getText().replace(",","");
        jccPage.montoInteresMoratorio.click();
        jccPage.montoInteresMoratorio.clear();
        jccPage.montoInteresMoratorio.sendKeys(saldoActualInteresMoratorio);
        util.tomarEvidencia("Se ingreso Interes Moratorio");
        logger.info("Se ingreso el Interes Moratorio" + saldoActualInteresMoratorio);
    }

    public void ingresarGastos() throws IOException {
        jccPage.saldoActual_gastos.click();
        String saldoActualGastos = jccPage.saldoActual_gastos.getText().replace(",","");
        jccPage.montoGastos.click();
        jccPage.montoGastos.clear();
        jccPage.montoGastos.sendKeys(saldoActualGastos);
        util.tomarEvidencia("Se ingreso Gastos");
        logger.info("Se ingreso el Monto de Gasto" + saldoActualGastos);

    }

    public void ingresarGastosProtesto() throws IOException {
        jccPage.saldoActual_gastosProtesto.click();
        String saldoActualGastosProtesto = jccPage.saldoActual_gastosProtesto.getText().replace(",","");
        jccPage.montoGastosProtesto.click();
        jccPage.montoGastosProtesto.clear();
        jccPage.montoGastosProtesto.sendKeys(saldoActualGastosProtesto);
        util.tomarEvidencia("Se ingreso Gastos Protesto");
        logger.info("Se ingreso el Monto de Gastos Protesto" + saldoActualGastosProtesto);

    }

    public void ingresarSeguros() throws IOException {
        jccPage.saldoActual_Seguros.click();
        String saldoActualSeguros = jccPage.saldoActual_Seguros.getText().replace(",","");
        jccPage.montoSeguros.click();
        jccPage.montoSeguros.clear();
        jccPage.montoSeguros.sendKeys(saldoActualSeguros);
        util.tomarEvidencia("Se ingreso Seguros");
        logger.info("Se ingreso el Monto del Seguro" +saldoActualSeguros);

    }


    public void ingresarHonorarios() throws IOException {
        jccPage.saldoActual_Honorarios.click();
        String saldoActualHonorarios = jccPage.saldoActual_Honorarios.getText().replace(",","");
        jccPage.montoHonorarios.click();
        jccPage.montoHonorarios.clear();
        jccPage.montoHonorarios.sendKeys(saldoActualHonorarios);
        util.tomarEvidencia("Se ingreso Honorarios");
        logger.info("Se ingresa Monto de Honorairos" + saldoActualHonorarios);
    }

    public void ingresarComisiones() throws IOException {
        jccPage.saldoActual_Comisiones.click();
        String saldoActualComisiones = jccPage.saldoActual_Comisiones.getText().replace(",","");
        jccPage.montoComisiones.click();
        jccPage.montoComisiones.clear();
        jccPage.montoComisiones.sendKeys(saldoActualComisiones);
        util.tomarEvidencia("Se ingreso Comision");
        logger.info("Se ingresa el monto de Comisiones" + saldoActualComisiones);

    }

    public void ingresarCapital(String Capital) throws IOException {
        jccPage.montoCapital.click();
        jccPage.montoCapital.clear();
        jccPage.montoCapital.sendKeys(Capital);
        util.tomarEvidencia("Se ingreso el Capital");
        logger.info("Se ingreso el Monto del Capital" + Capital);
    }


    public void ingresarMontoInteresCompensatorio(String InteresCompensatorio) throws IOException {
        jccPage.montoInteresCompensatorio.click();
        jccPage.montoInteresCompensatorio.clear();
        jccPage.montoInteresCompensatorio.sendKeys(InteresCompensatorio);
        util.tomarEvidencia("Se ingreso el Interes Compensatorio");
        logger.info("Se ingreso el Monto Interes Compensatorio" + InteresCompensatorio);
    }

    public void ingresarMontoInteresMoratorio(String InteresMoratorio) throws IOException {
        jccPage.montoInteresMoratorio.click();
        jccPage.montoInteresMoratorio.clear();
        jccPage.montoInteresMoratorio.sendKeys(InteresMoratorio);
        util.tomarEvidencia("Se ingreso el Interes Moratorio");
        logger.info("Se ingreso el Monto del Interes Moratorio" + InteresMoratorio);
    }

    public void ingresarMontoGastos(String Gastos) throws IOException {
        jccPage.montoGastos.click();
        jccPage.montoGastos.clear();
        jccPage.montoGastos.sendKeys(Gastos);
        util.tomarEvidencia("Se ingreso el Gasto");
        logger.info("Se ingreso el Monto de Gastos" + Gastos);
    }

    public void ingresarMontoGastosProtesto(String GastosProtesto) throws IOException {
        jccPage.montoGastosProtesto.click();
        jccPage.montoGastosProtesto.clear();
        jccPage.montoGastosProtesto.sendKeys(GastosProtesto);
        util.tomarEvidencia("Se ingreso el Gasto Protesto");
        logger.info("Se ingreso el Monto Gasto Protesto" + GastosProtesto);
    }

    public void ingresarMontoSeguros(String Seguros) throws IOException {
        jccPage.montoSeguros.click();
        jccPage.montoSeguros.clear();
        jccPage.montoSeguros.sendKeys(Seguros);
        util.tomarEvidencia("Se ingreso Seguros");
        logger.info("Se ingreso el Monto de Seguros" + Seguros);
    }

    public void ingresarMontoHonorarios(String Honorarios) throws IOException {
        jccPage.montoHonorarios.click();
        jccPage.montoHonorarios.clear();
        jccPage.montoHonorarios.sendKeys(Honorarios);
        util.tomarEvidencia("Se ingreso Honorarios");
        logger.info("Se ingreso el Monto de Honorarios" + Honorarios);
    }
    
    public void ingresarMontoComisiones(String Comisiones) throws IOException {
        jccPage.montoComisiones.click();
        jccPage.montoComisiones.clear();
        jccPage.montoComisiones.sendKeys(Comisiones);
        util.tomarEvidencia("Se ingreso Comisiones");
        logger.info("Se ingreso el Monto de Comisiones" + Comisiones);

    }


    public void seleccionarLaOpcionIncrementos() throws IOException {
        jccPage.opcIncrementos.click();
        cambioFrameJCC();
        util.tomarEvidencia("Se ingreso a la opción de Incrementos");

    }

    public void ingresarMontoInteresDiferido(String interesDiferido) throws IOException {
        jccPage.txtInteresDiferidoOrigen.click();
        jccPage.txtInteresDiferidoOrigen.clear();
        jccPage.txtInteresDiferidoOrigen.sendKeys(interesDiferido);
        util.tomarEvidencia("Se ingreso Interes Diferido");
        logger.info("Se ingreso el Monto de Interes Diferido" + interesDiferido);
    }

    public void ingresarMontoInteresCompensatorioVencidoOrigen(String intCompVencOrigen) throws IOException {
        jccPage.InteresCompensatorioVencidoOrigen.click();
        jccPage.InteresCompensatorioVencidoOrigen.clear();
        jccPage.InteresCompensatorioVencidoOrigen.sendKeys(intCompVencOrigen);
        util.tomarEvidencia("Se ingreso Interes Compensatorio Vencido Origen");
        logger.info("Se ingreso el Monto de Interes Diferido" + intCompVencOrigen);
    }

    public void ingresarMontoInteresMoratorioOrigen(String intMoratorio) throws IOException {
        jccPage.InteresMoratorioOrigen.click();
        jccPage.InteresMoratorioOrigen.clear();
        jccPage.InteresMoratorioOrigen.sendKeys(intMoratorio);
        util.tomarEvidencia("Se ingreso Interes Moratorio Origen");
        logger.info("Se ingreso el Monto de Interes Diferido" + intMoratorio);

    }

    public void ingresarMontoGastosOrigen(String gastosOrigen) throws IOException {
        jccPage.montoGastos.click();
        jccPage.montoGastos.clear();
        jccPage.montoGastos.sendKeys(gastosOrigen);
        util.tomarEvidencia("Se ingreso Monto Gasto Origen");
        logger.info("Se ingreso el Monto Gasto Origen" + gastosOrigen);
    }

    public void ingresarMontoSegurosOrigen(String segurosOrigen) throws IOException {
        jccPage.seguroOrigen.click();
        jccPage.seguroOrigen.clear();
        jccPage.seguroOrigen.sendKeys(segurosOrigen);
        util.tomarEvidencia("Se ingreso Monto Seguro Origen");
        logger.info("Se ingreso el Monto Seguro Origen" + segurosOrigen);
    }

    public void seleccionarLaOpciónModificaciones() throws IOException {
        jccPage.opcModificacion.click();
        cambioFrameJCC();
        util.tomarEvidencia("Se seleccionó la Opcion de Modificaciones");

    }

    public void ingresarValorDeHonorarios(String honorarios) throws IOException {
        jccPage.txtHonorarios.click();
        jccPage.txtHonorarios.clear();
        jccPage.txtHonorarios.sendKeys(honorarios);
        util.tomarEvidencia("Se ingreso valor para Honorarios");
        logger.info("Se ingreso el Monto para Honorarios" + honorarios);

    }

    public void seleccionarLaOpcionAnulacionDePago() throws IOException {
        jccPage.opcAnulacionPagosJCC.click();
        cambioFrameJCC();
        util.tomarEvidencia("Se seleccionó la Opcion de Anulacion de Pagos");

    }

    public void seleccionarPagosAAnular() throws IOException{
        //String ventanaActual = getDriver().getWindowHandle();
        jccPage.lnkPagosAnular.click();
/*
        for (String ventana :getDriver().getWindowHandles()) {
            if(!ventana.equals(ventanaActual)) {
                getDriver().switchTo().window(ventana);
                break;
            }
        }
        pause(1);
        util.tomarEvidencia("Producto refinanciado ingresado");
        jccPage.lnkPagosAnular.click();

        getDriver().switchTo().window(ventanaActual);
        getDriver().switchTo().defaultContent();

        pause(2);
        getDriver().switchTo().frame(getDriver().findElement(By.xpath("//table[@id='gvwPago']/tbody/tr[2]/td[1]")));
  */
   }

}
