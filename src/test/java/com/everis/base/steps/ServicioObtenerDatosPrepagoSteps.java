package com.everis.base.steps;

import com.everis.base.PageObject.ServicioObtenerDatosPrepagoPage;
import com.everis.base.Utils.Util;
import com.everis.base.commons.Constant;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class ServicioObtenerDatosPrepagoSteps {

    private static final Logger logger = LoggerFactory.getLogger(ServicioObtenerDatosPrepagoSteps.class);
    private ServicioObtenerDatosPrepagoPage servicioObtenerDatosPrepagoPage;
    private Util util;

    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ServicioObtenerDatosPrepagoSteps() {
        this.util = new Util();
    }

    @Step

    public void ingresarALaUrlDelServicioObtenerDatosPrepago() {
        servicioObtenerDatosPrepagoPage.open();
        logger.info("Se ingreso al servicio Simulacion Prepago Digital");
    }

    public void ingresarNumeroDeCredito(String nroCredito) {
        servicioObtenerDatosPrepagoPage.nroCredito.sendKeys(nroCredito);
        Constant.numCredito = nroCredito;
    }

    public void invocarServicioObtenerDatosPrepago() throws IOException {
        servicioObtenerDatosPrepagoPage.txtNombreServicio.click();
        util.tomarEvidenciaObtenerDatosPrepago("Request");

        servicioObtenerDatosPrepagoPage.btnInvoke.click();
        pause(1);
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(1));
        pause(1);
        getDriver().manage().window().maximize();

        if(servicioObtenerDatosPrepagoPage.txtCodError.isPresent()){
            util.tomarEvidenciaObtenerDatosPrepago("Response");
            String codigoError = servicioObtenerDatosPrepagoPage.txtCodError.getTextValue();
            String mensajeError = servicioObtenerDatosPrepagoPage.txtMensajeError.getTextValue();
            logger.info("Codigo error: " + codigoError);
            logger.info("Mensaje error: " + mensajeError);
            util.crearLog(codigoError + " " + mensajeError);
        }
        else {
            util.tomarEvidenciaObtenerDatosPrepago("Response");
            util.renombrarCarpeta();
        }

        getDriver().close();
        getDriver().switchTo().window(tabs2.get(0));
    }
}