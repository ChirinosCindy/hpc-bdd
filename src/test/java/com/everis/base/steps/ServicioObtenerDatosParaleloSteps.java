package com.everis.base.steps;

import com.everis.base.PageObject.ServicioObtenerDatosParaleloPage;
import com.everis.base.Utils.Util;
import com.everis.base.commons.Constant;
import net.thucydides.core.annotations.Step;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class ServicioObtenerDatosParaleloSteps {

    private static final Logger logger = LoggerFactory.getLogger(ServicioObtenerDatosParaleloSteps.class);
    private ServicioObtenerDatosParaleloPage servicioObtenerDatosParaleloPage;
    private Util util;

    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ServicioObtenerDatosParaleloSteps() {
        this.util = new Util();
    }

    @Step

    public void ingresarALaUrlDelServicioObtenerDatosParalelo() {
        servicioObtenerDatosParaleloPage.open();
        logger.info("Se ingreso al servicio Simulacion Prepago Digital");
    }

    public void ingresarNumeroDeCreditoPrincipal(String nroCredito) {
        servicioObtenerDatosParaleloPage.nroCredito.sendKeys(nroCredito);
        Constant.numCredito = nroCredito;
    }

    public void invocarServicioObtenerDatosParalelo() throws IOException {
        servicioObtenerDatosParaleloPage.txtNombreServicio.click();
        util.tomarEvidenciaObtenerDatosParalelo("Request");

        servicioObtenerDatosParaleloPage.btnInvoke.click();
        pause(1);
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(1));
        pause(1);
        getDriver().manage().window().maximize();

        String codigoError = servicioObtenerDatosParaleloPage.txtCodError.getTextValue();
        util.tomarEvidenciaObtenerDatosParalelo("Response");

        if(codigoError.equals("0")){
            util.renombrarCarpeta();
        }
        else{
            String mensajeError = servicioObtenerDatosParaleloPage.txtMensajeError.getTextValue();
            logger.info("Codigo error: " + codigoError);
            logger.info("Mensaje error: " + mensajeError);
            util.crearLog(codigoError + " " + mensajeError);
        }

        getDriver().close();
        getDriver().switchTo().window(tabs2.get(0));
    }


}