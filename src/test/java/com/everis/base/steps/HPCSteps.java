package com.everis.base.steps;

import com.everis.base.PageObject.HPCPage;
import com.everis.base.commons.Constant;
import com.everis.base.Utils.Util;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;



import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class HPCSteps {

    private static final Logger logger = LoggerFactory.getLogger(HPCSteps.class);
    private HPCPage hpcPage;
    private Util util;
    private String mensajeAValidar = "";

    public HPCSteps(){
        this.util = new Util();
    }

    public void pause(Integer seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Step

    public void clickPestanaOperaciones() throws IOException {
        hpcPage.opcOperaciones.click();
        logger.info("Se selecciono la opcion Operaciones");
        util.tomarEvidencia("Seleccionar la Opcion de Operaciones");
    }

    public void clickPestanaOperacionesUpgrade() throws IOException {
        hpcPage.opcOperacionesUpgrade.click();
        logger.info("Se selecciono la opcion Operaciones");
        util.tomarEvidencia("Seleccionar la Opcion de Operaciones");
    }

    public void clickOpcionCreditos() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")){
            logger.info(Constant.codGrupo);
            hpcPage.opcCreditos.click();
        }
        else {
            logger.info(Constant.codGrupo);
            hpcPage.opcCreditosPersonal.click();
        }

        logger.info("Se selecciono la opcion Creditos");
        util.tomarEvidencia("Seleccionar Opcion Creditos");
    }

    public void clickOpcionCreditosUpgrade() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")){
            logger.info(Constant.codGrupo);
            hpcPage.opcCreditosUpgrade.click();
        }
        else {
            logger.info(Constant.codGrupo);
            hpcPage.opcCreditosPersonalUpgrade.click();
        }

        logger.info("Se selecciono la opcion Creditos");
        util.tomarEvidencia("Seleccionar Opcion Creditos");
    }

    public void clickSubOpcionIngreso() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcIngreso.click();
        } else { hpcPage.opcIngresoPersonal.click();}

        logger.info("Se selecciono la opcion Ingreso");
        cambioFrame();
        util.tomarEvidencia("Seleccionar Opcion Ingreso");
    }

    public void clickSubOpcionIngresoUpgrade() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcIngresoUpgrade.click();
        } else { hpcPage.opcIngresoPersonalUpgrade.click();}

        logger.info("Se selecciono la opcion Ingreso");
        cambioFrame();
        util.tomarEvidencia("Seleccionar Opcion Ingreso");
    }

    public void clickSubOpcionModificacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcModificacion.click();
        } else { hpcPage.opcModificacionPersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Modificacion");
        logger.info("click en Modificacion");
        cambioFrame();
    }


    public void clickSubOpcionAjuste() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAjuste.click();
        } else { hpcPage.opcAjustePersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Modificacion");
        logger.info("click en Ajuste");
        cambioFrame();
        util.tomarEvidencia("Seleccionar la opcion Ajuste");
    }

    public void cambioFrame() {
        hpcPage.ifrContent.waitUntilEnabled();
        getDriver().switchTo().frame("frmcontent");

        Actions actions = new Actions(getDriver());
        actions.moveToElement(hpcPage.btnCerrar).perform();//Hover
    }

    public void SeleccionarBotonNuevo() throws IOException {
        util.tomarEvidencia("Nuevo registro");
        hpcPage.btnNuevo.click();
        logger.info("Se selecciono el boton Nuevo");
    }

    public void SeleccionarBotonNuevoUpgrade() throws IOException {
        util.tomarEvidencia("Nuevo registro");
        hpcPage.btnNuevo.click();
        logger.info("Se selecciono el boton Nuevo");
    }

    public void clickOpcConsultas() {
        logger.info("Click opcion Consultas");
        hpcPage.opcConsultas.click();
    }

    public void clickConsultaDelCredito() {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcConsultaCredito.click();
        } else { hpcPage.opcConsultaCreditoPersonal.click();}

        logger.info("Click opcion Consulta del Credito");

        cambioFrame();
    }

    public void ingresarElCodigoDelProducto(String nroProducto) throws IOException {
        hpcPage.txtNroProducto.click();
        hpcPage.txtNroProducto.sendKeys(nroProducto);
        hpcPage.txtNroProducto.sendKeys(Key.ENTER);
        pause(7);
        util.tomarEvidencia("Se ingreso codigo del producto");
        logger.info("Se ingreso codigo de producto");
    }

    public void seleccionarLaMoneda(String cmbMoneda) throws IOException {
        hpcPage.cmbMoneda.waitUntilEnabled().click();
        hpcPage.cmbMoneda.selectByVisibleText(cmbMoneda);
        util.tomarEvidencia("Se ingreso moneda");
        logger.info("Se ingreso moneda");
    }

    public void ingresarCUDelCliente(String nrCU) {
        hpcPage.txtCodUnico.click();
        hpcPage.txtCodUnico.sendKeys(nrCU);
        hpcPage.txtCodUnico.sendKeys(Key.ENTER);
        pause(6);
        logger.info("Se ingreso CU");
    }

    public void ingresarCodigoDelPromotor() {
/*
        boolean eleSelected = hpcPage.txtCodPromotor.isEnabled();
        logger.info("elemento presente: " + eleSelected);
        while (eleSelected) {
            logger.info("esperando elemento");
            eleSelected = hpcPage.txtCodPromotor.isEnabled();
            logger.info("elemento presente: " + eleSelected);
        }
*/
        hpcPage.txtCodPromotor.waitUntilVisible().click();
        hpcPage.txtCodPromotor.sendKeys(Constant.Desembolso_Promotor);
        hpcPage.txtCodPromotor.sendKeys(Key.ENTER);
        pause(3);
        hpcPage.txtCodPromotor.sendKeys(Key.TAB);
        hpcPage.txtCodPromotor.click();
        hpcPage.txtCodPromotor.sendKeys(Key.ENTER);
        pause(3);
        logger.info("Se ingreso codigo del promotor");
    }

    public void ingresarCanalDeVenta() {
        hpcPage.txtCodCanalVenta.click();
        hpcPage.txtCodCanalVenta.clear();
        hpcPage.txtCodCanalVenta.sendKeys(Constant.Desembolso_Canal);
        hpcPage.txtCodCanalVenta.sendKeys(Key.ENTER);
        pause(3);
        hpcPage.txtCodCanalVenta.sendKeys(Key.TAB);
        hpcPage.txtCodCanalVenta.click();
        hpcPage.txtCodCanalVenta.sendKeys(Key.ENTER);
        pause(3);
        logger.info("Se ingreso canal de venta");
    }

    public void ingresarCodigoDeTienda() {
        hpcPage.txtCodBusquedaTienda.click();
        hpcPage.txtCodBusquedaTienda.sendKeys(Constant.Desembolso_Tienda);
        hpcPage.txtCodBusquedaTienda.sendKeys(Key.ENTER);
        pause(5);
        logger.info("Se ingreso codigo de tienda");
    }

    public void ingresarCodigoDeEstablecimiento() {
        hpcPage.txtCodigoEstablecimiento.click();
        hpcPage.txtCodigoEstablecimiento.sendKeys(Constant.Desembolso_Establecimiento);
        hpcPage.txtCodigoEstablecimiento.sendKeys(Key.ENTER);
        pause(5);
        logger.info("Se ingreso codigo de establecimiento");
    }

    public void ingresarDatosDeProductoARefinanciar() throws IOException {

        String ventanaActual = getDriver().getWindowHandle();

        //PopUp
        hpcPage.btnIngresoProductosRefinanciar.click();

        for (String ventana :getDriver().getWindowHandles()) {
            if(!ventana.equals(ventanaActual)) {
                getDriver().switchTo().window(ventana);
                break;
            }
        }

        pause(1);
        hpcPage.cmbProductoRefinanciado.selectByVisibleText(Constant.Desembolso_ProductoRefinanciado);

        hpcPage.txtNProductoRefinanciado.click();
        hpcPage.txtNProductoRefinanciado.sendKeys(Constant.Desembolso_NumeroProdRefinanciar);

        hpcPage.txtMontoSolicitadoProductoRefinanciado.click();
        hpcPage.txtMontoSolicitadoProductoRefinanciado.sendKeys(Constant.Desembolso_MontoRefinanciar);

        hpcPage.txtInteresDiferidoRefinanciado.click();
        hpcPage.txtInteresDiferidoRefinanciado.sendKeys(Constant.Desembolso_InteresRefinanciar);

        hpcPage.txtComisionDiferidoRefinanciado.click();
        hpcPage.txtComisionDiferidoRefinanciado.sendKeys(Constant.Desembolso_ComisionRefinanciar);

        hpcPage.txtCodigoUnicoRefinanciado.click();
        hpcPage.txtCodigoUnicoRefinanciado.sendKeys(Constant.Desembolso_ClienteRefinanciar);

        hpcPage.btnGuardarProductoRefinanciado.click();
        pause(1);
        /*
        //Si_Error
        if(this.metodo.isPopUpPresent(8)) {
            this.metodo.tomarEvidencia("Error al guardar producto refinanciado, datos inválidos");
            msjFinal = "Error al guardar producto refinanciado, datos inválidos.";
            this.globalData.setData("vOutResultado", msjFinal);
            return;
        }
        */
        util.tomarEvidencia("Producto refinanciado ingresado");
        hpcPage.btnAceptarProductoRefinanciado.click();

        getDriver().switchTo().window(ventanaActual);
        getDriver().switchTo().defaultContent();

        pause(2);
        getDriver().switchTo().frame(getDriver().findElement(By.id("frmcontent")));
    }

    public void seleccionarElTipoDeEvaluacion() {
        hpcPage.cmbTipoEvaluacion.click();
        hpcPage.cmbTipoEvaluacion.selectByVisibleText(Constant.Desembolso_TipoEvaluacion);
        hpcPage.cmbTipoEvaluacion.click();
        logger.info("Se ingreso tipo de evaluacion");
    }

    public void seleccionarLaMonedaDelPrecioDeVenta(String Moneda) {
        hpcPage.cmbPrecioVenta.click();
        hpcPage.cmbPrecioVenta.selectByVisibleText(Moneda);
        hpcPage.cmbPrecioVenta.click();
        logger.info("Se selecciono moneda de precio de venta");
    }

    public void ingresarElMontoDelPrecioDeVenta(String precioVenta) {
        hpcPage.txtPrecioVenta.click();
        hpcPage.txtPrecioVenta.clear();
        hpcPage.txtPrecioVenta.sendKeys(precioVenta);
        hpcPage.txtPrecioVenta.click();
        logger.info("Se ingreso monto de precio de venta");
    }

    public void seleccionarLaMonedaDelValorComercial(String cmbMoneda) {
        hpcPage.cmbValorComercial.click();
        hpcPage.cmbValorComercial.selectByVisibleText(cmbMoneda);
        hpcPage.cmbValorComercial.click();
        logger.info("Se selecciono moneda de valor comercial");
    }

    public void ingresarElMontoDelValorComercial(String valorComercial) throws IOException {
        hpcPage.txtValorComercial.click();
        hpcPage.txtValorComercial.clear();
        hpcPage.txtValorComercial.sendKeys(valorComercial);
        pause(5);
        logger.info("Se ingreso monto de valor comercial");
        util.tomarEvidencia("Datos administrativos");
    }

    public void ingresarElDestinoDelCredito() {
        hpcPage.txtTipoDestino.click();
        hpcPage.txtTipoDestino.clear();
        hpcPage.txtTipoDestino.sendKeys(Constant.Desembolso_DestinoCredito);
        hpcPage.txtTipoDestino.sendKeys(Key.ENTER);
        pause(5);
        logger.info("Se selecciono el destino del credito");
    }

    public void ingresarElPlazo(String plazo) {
        hpcPage.txtPlazo.click();
        hpcPage.txtPlazo.clear();
        hpcPage.txtPlazo.sendKeys(plazo);
        logger.info("Se ingreso Plazo");
    }

    public void ingresarLaTasa(String tasaInteres) throws IOException {
        hpcPage.txtTasaInteres.click();
        hpcPage.txtTasaInteres.sendKeys(tasaInteres);
        logger.info("Se ingreso Tasa de Interes");
        util.tomarEvidencia("Datos del credito");
    }

    public void seleccionarEnvioDeInformeDePago() throws IOException {
        hpcPage.cmbEnvioInformePago.click();
        hpcPage.cmbEnvioInformePago.selectByVisibleText(Constant.Desembolso_TipoEnvioInformePago);
        pause(2);
        logger.info("Se selecciono el tipo de envio de informe de pago");
        util.tomarEvidencia("Se selecciono el tipo de envio de informe de pago");
    }

    public void ingresarElTipoDeAbono() {
        hpcPage.txtTipoAbono.click();
        hpcPage.txtTipoAbono.sendKeys(Constant.Desembolso_TipoAbono);
        hpcPage.txtTipoAbono.sendKeys(Key.ENTER);
        pause(5);
        logger.info("Se selecciono el tipo de abono");
    }

    public void ingresarElMontoSolicitado(String montoSolicitado) throws IOException {
        hpcPage.txtMontoSolicitado.click();
        hpcPage.txtMontoSolicitado.clear();
        hpcPage.txtMontoSolicitado.sendKeys(montoSolicitado);
        logger.info("Se ingreso el monto desembolsado");
        util.tomarEvidencia("Datos desembolso");
    }

    public void ingresarSeguroDelBien(String montoSeguroBien) throws IOException {
        hpcPage.txtImporteAsegurado.click();
        hpcPage.txtImporteAsegurado.clear();
        hpcPage.txtImporteAsegurado.sendKeys(montoSeguroBien);
        logger.info("Se ingreso el monto del seguro del bien");
        util.tomarEvidencia("Seguro del bien");
    }

    public void ingresarLaTasaDelSeguroDelBienVehicular() throws IOException {
        hpcPage.txtTasaSeguroBienVehicular.click();
        hpcPage.txtTasaSeguroBienVehicular.clear();
        hpcPage.txtTasaSeguroBienVehicular.sendKeys(Constant.Desembolso_TasaSegBienVehicular);
        logger.info("Se ingreso el monto del seguro del bien vehicular");
        util.tomarEvidencia("Seguro del bien vehicular");
    }

    public void ingresarMontoAsegurado(String montoAsegurado) throws IOException {
        hpcPage.txtImporteAseguradoVehicular.click();
        hpcPage.txtImporteAseguradoVehicular.sendKeys(montoAsegurado);
        logger.info("Se ingreso el monto asegurado vehicular");
        util.tomarEvidencia("Monto asegurado vehicular");
    }

    public void seleccionarLaMonedaDelGravamen(String monedaGravamen){
        hpcPage.txtMonedaGravamen.click();
        hpcPage.txtMonedaGravamen.sendKeys(monedaGravamen);
        pause(5);
        logger.info("Se selecciono la moneda del monto de gravamen");
    }
    public void ingresarElMontoDeGravamen(String montoGravamen) throws IOException {
        hpcPage.txtMontoGravamen.click();
        hpcPage.txtMontoGravamen.sendKeys(montoGravamen);
        logger.info("Se ingreso el monto de gravamen");
        util.tomarEvidencia("Monto gravamen");
    }

    public void clickBtnGrabar() throws IOException {
        hpcPage.btnGrabar.click();
        logger.info("Se selecciono boton grabar");

        pause(5);
        Alert alert = getDriver().switchTo().alert();
        String mensajeAlerta = alert.getText();
        alert.accept();
        mensajeAValidar = mensajeAlerta;
   /*
        WebDriverWait wait = new WebDriverWait(getDriver(), 300);
        while (wait.until(ExpectedConditions.alertIsPresent()) != null){
            logger.info("alert was present");
            Alert alert = getDriver().switchTo().alert();
            String mensajeAlerta = alert.getText();
            alert.accept();
            mensajeAValidar = mensajeAlerta;
            //mensajeAValidar = mensajeAValidar.substring(0,31);
            logger.info("Mensaje: "+ mensajeAValidar);

            if (mensajeAValidar.contains(Constant.Mensaje_REGISTRO_DATOS_FUERA_PLANTILLA) ||
                    mensajeAValidar.contains(Constant.Mensaje_REGISTRO_TARIFA_DIA_AYER) ||
                    mensajeAValidar.contains(Constant.Mensaje_PROCESANDO)){
                logger.info("Alerta: " + mensajeAValidar);
            }
            else {
                logger.info("Alerta: " + mensajeAValidar);
                break;
            }
        }
   */
    }

    public void clickBtnEliminar() throws IOException {
        hpcPage.btnEliminar.click();
        logger.info("Se selecciono boton eliminar");

        Alert alert = getDriver().switchTo().alert();
        alert.accept();
        pause(5);
        String mensajeAlerta = alert.getText();
        alert.accept();
        pause(5);
        mensajeAValidar = mensajeAlerta;
    }

    public void verificarQueLaOperacionSeHayaRealizadoConExito() throws IOException {
        Assert.assertEquals(mensajeAValidar, Constant.Mensaje_REGISTRO_EXITOSO);
        logger.info(Constant.Mensaje_REGISTRO_EXITOSO.toUpperCase());
        util.tomarEvidencia(Constant.Mensaje_REGISTRO_EXITOSO.toUpperCase());
        util.renombrarCarpeta();
        logger.info("Se selecciono boton aceptar");

    }

    public void verificarRegistroTarifaDiaAyer() throws IOException {
        Assert.assertEquals(mensajeAValidar, Constant.Mensaje_REGISTRO_TARIFA_DIA_AYER);
        logger.info(Constant.Mensaje_REGISTRO_TARIFA_DIA_AYER.toUpperCase());
        util.tomarEvidencia(Constant.Mensaje_REGISTRO_TARIFA_DIA_AYER.toUpperCase());
        logger.info("Se selecciono boton aceptar");

    }


    public void verificarQueLaOperacionSeHayaEliminado() throws IOException {
        Assert.assertEquals(mensajeAValidar, Constant.Mensaje_REGISTRO_ELIMINADO);
        logger.info(Constant.Mensaje_REGISTRO_ELIMINADO.toUpperCase());
        util.tomarEvidencia(Constant.Mensaje_REGISTRO_ELIMINADO.toUpperCase());
    }

    public void ingresarNroCredito(String nroCredito) throws IOException {
        Constant.numCredito = nroCredito;
        hpcPage.txtNroCredito.click();
        hpcPage.txtNroCredito.sendKeys(nroCredito);
        hpcPage.txtNroCredito.sendKeys(Key.ENTER);
        pause(5);
        logger.info("Se ingreso el número del credito: " + Constant.numCredito);
        util.tomarEvidencia("Ingreso credito");
    }

    public void cambiarValorTasa(String TasaInteres) throws IOException {
        hpcPage.txtTasaInteres.waitUntilEnabled().click();
        hpcPage.txtTasaInteres.clear();
        hpcPage.txtTasaInteres.sendKeys(TasaInteres);
        pause(5);
        util.tomarEvidencia("Seleccionar la Tasa");
    }

    public void seleccionarRegistrodelaGrilla() throws IOException {
        hpcPage.clickGrilla.click();
        util.tomarEvidencia("seleccionar registro");
    }

    public void ingresarAjusteDesdelaCuota(String AjusteCuota) throws IOException {
        hpcPage.cuotaAjuste.click();
        hpcPage.cuotaAjuste.sendKeys(AjusteCuota);
        util.tomarEvidencia("Ingresar Ajuste desde la Cuota");
    }

    public void clicSubopcionDescargo() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcDescargo.click();
        } else { hpcPage.opcDescargoPersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Modificacion");
        cambioFrame();
        logger.info("Click opcion Descargo");
        util.tomarEvidencia("Seleccionar Opcion Descargo");
    }

    public void seleccionarDatosDelDescargo() throws IOException {
        hpcPage.tipoDescargo.click();
        hpcPage.tipoDescargo.selectByVisibleText(Constant.Descargo_TipoDescargo);
        pause(5);
        hpcPage.subTipoDescargo.click();
        hpcPage.subTipoDescargo.selectByVisibleText(Constant.Descargo_SubTipoDescargo);
        pause(5);
        hpcPage.comentario.click();
        hpcPage.comentario.sendKeys(Constant.Descargo_Comentario);

        util.tomarEvidencia("Datos descargo");
    }

    public void clickSubopcionReingreso() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcReingreso.click();
        } else { hpcPage.opcReingresoPersonal.click();}

        logger.info("Click opcion Reingreso");

        cambioFrame();
        util.tomarEvidencia("Seleccionar Reingreso");

    }

    public void clickBtnCerrar() throws IOException {
        hpcPage.btnCerrar.click();
        logger.info("se dio click al boton salir");
        util.tomarEvidencia("Click en salir");
    }

    public void clickBtnCerrarDesembolso() throws IOException {
        cambioFrame();
        hpcPage.btnCerrarDesembolso.click();
        logger.info("se dio click al boton salir");
        util.tomarEvidencia("Click en salir");
    }

    public void clickSubopcionCambioVencimiento() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcCambioVencimiento.click();
        } else { hpcPage.opcCambioVencimientoPersonal.click();}

       logger.info("Click opcion Cambio de Vencimiento");

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion Cambio de Vencimiento");
    }


    public void ingresarDatosDelCambioDeVencimiento() throws IOException {
        hpcPage.diaVencimiento.click();
        hpcPage.diaVencimiento.sendKeys(Constant.CambioVencimiento_DiaVencimiento);
        hpcPage.comentario.click();
        hpcPage.comentario.sendKeys(Constant.CambioVencimiento_Comentario);

        util.tomarEvidencia("Datos cambio de vencimiento");
    }

    public void seleccionarLaOpcionAjusteDeCuota() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAjusteDeCuota.click();
        } else { hpcPage.opcAjusteDeCuotaPersonal.click();}

        logger.info("Click opcion Ajuste de Cuota");
        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion Ajuste de Cuota");

    }

    public void seleccionarDelCronogramaDeCuotasLaOpcionModificar(String cantCuotas) throws IOException {
        logger.info("Click en el icono Lapiz (Operacion Modificar)");

        int nroCuotas = Integer.parseInt(cantCuotas);
        //nroCuotas = nroCuotas + 1;

        for (int i = 2; i <= nroCuotas; i++) {
            WebElement myElement;
            if (i <= 9) {
                myElement = getDriver().findElement(By.xpath("//a[@id='dgCronograma_ctl0"+i+"_hypUpd']/img"));
                myElement.click();
                ingresarValorDelInteresCompensatorio();
                ingresarValorDelInteresMoratorio();
                clickBtnGrabarCronograma();
            } else {
                myElement = getDriver().findElement(By.xpath("//a[@id='dgCronograma_ctl0"+i+"_hypUpd']/img"));
                myElement.click();
                ingresarValorDelInteresCompensatorio();
                ingresarValorDelInteresMoratorio();
                clickBtnGrabarCronograma();
            }

        }

        util.tomarEvidencia("Cuotas seleccionadas");
        util.tomarEvidencia("Seleccionar del Cronograma de Cuotas la Opcion Modificar");
    }

    public void ingresarValorDelInteresCompensatorio() throws IOException {
        hpcPage.interesCompensatorioMod.clear();
        hpcPage.interesCompensatorioMod.click();
        hpcPage.interesCompensatorioMod.sendKeys(Constant.AjusteCuota_interesCompensatorio);

        util.tomarEvidencia("Ingreso interes compensatorio");
    }


    public void clickBtnGrabarCronograma() {
        hpcPage.btnGrabar.click();
        logger.info("click en Grabar");
        pause(5);
    }

    public void SeleccionarLaOpcionPagos() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPagos.click();
        } else { hpcPage.opcPagosPersonal.click();}

        logger.info("click en la opcion de Pagos");
        util.tomarEvidencia("Seleccionar la Opcion de Pagos");
    }

    public void SeleccionarLaOpcionPagosCFV() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPagosCFV.click();
        } else { hpcPage.opcPagosCFVPersonal.click();}


        logger.info("click en la opcion de Pagos CFV");
        util.tomarEvidencia("Seleccionar la Opcion Pagos CFV");
    }


    public void seleccionarLaOpcionDePagoDeCuotas() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPagoCuotas.click();
        } else { hpcPage.opcPagoCuotasPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion de Pago de Cuotas");
    }

    public void pagarCuotas(String cantCuotas) throws IOException {

        int nroCuotas = Integer.parseInt(cantCuotas);
        nroCuotas = nroCuotas + 1;

        for (int i = 2; i <= nroCuotas; i++) {
            WebElement myElement;
            if (i <= 9) {
                myElement = getDriver().findElement(By.id("dgLista_ctl0" + i + "_chkSeleccion"));
            } else {
                myElement = getDriver().findElement(By.id("dgLista_ctl" + i + "_chkSeleccion"));
            }
            myElement.click();
        }

        util.tomarEvidencia("Cuotas seleccionadas");


    }


    public void seleccionarOpcionSimuladorCancelacionCFV() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorCancelacionCFV.click();
        } else { hpcPage.opcSimuladorCancelacionCFVPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Se selecciona la Opción de Simulación Cancelación CFV");
    }

    public void ingresarFechaValorParaSimulacion(String fechaValor) throws IOException {
        hpcPage.txtFechaSimulacion.waitUntilEnabled().click();
        hpcPage.txtFechaSimulacion.clear();
        hpcPage.txtFechaSimulacion.sendKeys(fechaValor);
        util.tomarEvidencia("Se ingresa Fecha Valor para la Simulación");
    }

    public void simularOperacion() throws IOException {
        hpcPage.btnSimular.click();
        logger.info("Se selecciono boton Simular");
        util.tomarEvidencia("Simulacion");

        if(hpcPage.txtMensajeErrorSimulacionPrepago.isVisible()){
            String mensajeError = hpcPage.txtMensajeErrorSimulacionPrepago.getText();
            util.crearLog(mensajeError);
        }
        else{
            hpcPage.txtNuevasCuotasRestantes.click();
            util.tomarEvidencia("Simulacion");
            util.renombrarCarpeta();
        }


/*        Alert alert = getDriver().switchTo().alert();
        alert.accept();
        pause(5);
        alert.accept();
        pause(5);
        logger.info("Se selecciono boton eliminar");

        //validacionMensajeSimulacion(desembolsoPage.mensajeValidacionFechaSimulacion);
*/    }

    public void validacionMensajeSimulacion(WebElementFacade mensaje) {
        if (mensaje.isDisplayed()) {
            logger.info("Mensaje validacion: " + mensaje.getText());
        }

    }

    public void seleccionarLaOpcionPrepago () throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPrepago.click();
        } else { hpcPage.opcPrepagoPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion Prepago");
    }

    public void ingresarDatosDelPrepago (String montoNeto, String modalidad) throws IOException {
        hpcPage.montoNeto.click();
        hpcPage.montoNeto.clear();
        hpcPage.montoNeto.sendKeys(montoNeto);

        hpcPage.modalidadPrepago.click();
        hpcPage.modalidadPrepago.selectByVisibleText(modalidad);

        hpcPage.lstMedioCargo.click();

        hpcPage.lstMedioCargo.selectByVisibleText(Constant.Pago_MedioCargo);

        util.tomarEvidencia("Ingresar Datos del Prepago");




    }

    public void seleccionarLaOpcionCancelacionCFV() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcCancelacionCFV.click();
        } else { hpcPage.opcCancelacionCFVPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opción Cancelacion CFV");
    }

    public void selccionarMedioDeCargo() throws IOException {
        hpcPage.lstMedioCargo.click();
        hpcPage.lstMedioCargo.selectByVisibleText(Constant.Pago_MedioCargo);
        util.tomarEvidencia("Seleccionar Medio de Cargo");
    }

    public void ingresarFechaValor(String fechaValor) throws IOException {
        hpcPage.txtFechaValor.click();
        hpcPage.txtFechaValor.clear();
        hpcPage.txtFechaValor.sendKeys(fechaValor);
        hpcPage.txtFechaValor.sendKeys(Key.TAB);
        logger.info("Se ingreso fecha valor");
        util.tomarEvidencia("Ingresar Fecha Valor");
    }


    public void seleccionLaOpcionPagoDeCuotasCFV() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPagoCuotasCFV.click();
        } else { hpcPage.opcPagoCuotasCFVPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Se Selecciona la Opción Pago de Cuotas CFV");
    }

    public void pagarCuotasCFV(String cantCuotas) {

        String num = "";
        int nroCuotas = Integer.parseInt(cantCuotas);
        nroCuotas = nroCuotas + 1;

        for (int i = 2; i <= nroCuotas; i++) {

            if (i <= 9) {
                num = "0" + i;
                logger.info("numero concatenado: " + num);
            }

            logger.info("dgLista_ctl"+num+"_ucFechaValor_txtFecha");

            WebElement fechaVenc = getDriver().findElement(By.xpath("//table[@id='dgLista']/tbody/tr["+num+"]/td[3]"));
            WebElement txtFecha = getDriver().findElement(By.id("dgLista_ctl"+num+"_ucFechaValor_txtFecha"));

            String fechaVencimiento = fechaVenc.getText();

            txtFecha.click();
            Actions action = new Actions(getDriver());
            action.doubleClick().perform();
            action.doubleClick().perform();

            txtFecha.sendKeys(fechaVencimiento);

            txtFecha.sendKeys(Key.TAB);
            pause(4);
            WebElement check = getDriver().findElement(By.id("dgLista_ctl" + num + "_chkSeleccion"));
            check.click();
        }

    }


    public void seleccionarLaOpcionCancelacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcCancelacion.click();
        } else { hpcPage.opcCancelacionPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion Cancelación");
    }

    public void seleccionarLaOpcionSimuladores() throws IOException {
        hpcPage.opcSimuladores.click();
        logger.info("Se selecciono opcion Simuladores");
        util.tomarEvidencia("Seleccionar Opcion Simuladores");
    }

    public void seleccionarLaOpcionSimuladorCancelacion() {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorCancelacion.click();
        } else { hpcPage.opcSimuladorCancelacionPersonal.click();}

        cambioFrame();
        logger.info("Se selecciono opcion Simulador Cancelacion");
    }

    public void seleccionarLaOpcionSimuladorPrepagoCFV() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorPrepagoCFV.click();
        } else { hpcPage.opcSimuladorPrepagoCFVPersonal.click();}

        cambioFrame();
        logger.info("Se selecciono opcion Simulador Prepago CFV");
        util.tomarEvidencia("Se Selecciono la Opción Simulador Prepago CFV");
    }

    public void ingresarMontoASimularEnElPrepago(String montoSimulacionPrepago) {
        hpcPage.txtMontoAPrepagar.click();
        hpcPage.txtMontoAPrepagar.clear();
        hpcPage.txtMontoAPrepagar.sendKeys(montoSimulacionPrepago);
        logger.info("Se ingreso monto a simular");
    }

    public void ingresarFechaValorParaSimulacionPrepago(String fechaValor) {
        hpcPage.txtFechaValorPrepago.click();
        hpcPage.txtFechaValorPrepago.clear();
        hpcPage.txtFechaValorPrepago.sendKeys(fechaValor);
        logger.info("Se ingreso fecha valor");
    }

    public void ingresarModalidadDePrepago(String modalidadPrepago) {

        String modalidad = "";

        switch (modalidadPrepago.toUpperCase()){
            case "REDUCCION DE PLAZO":
            case "REDUCCION DE PLAZOS":
            case "REDUCCIoN DE PLAZO":
            case "REDUCCIoN DE PLAZOS":
                modalidad = "REDUCCION DE PLAZOS";
                break;
            case "REDUCCION DE CUOTA":
            case "REDUCCION DE CUOTAS":
            case "REDUCCIoN DE CUOTA":
            case "REDUCCIoN DE CUOTAS":
                modalidad = "REDUCCION DE IMPORTE DE CUOTAS";
                break;
        }

        hpcPage.modalidadPrepagoSimulacion.waitUntilEnabled().click();
        hpcPage.modalidadPrepagoSimulacion.selectByVisibleText(modalidad);
        logger.info("Se selecciono la modalidad del prepago: " + modalidad);
    }

    public void seleccionarLaOpcionSimuladorPrepago() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorPrepago.click();
        } else { hpcPage.opcSimuladorPrepagoPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar Opcion Simulador Prepago ");

    }

    public void seleccionarLaOpcionMantenimiento() {
        hpcPage.opcMantenimiento.click();
        logger.info("Se selecciono la opcion Mantenimiento");
    }

    public void seleccionarLaOpcionIndiceVAC() {
        hpcPage.opcIndiceVAC.click();
        cambioFrame();
        logger.info("Se selecciono la opcion Indice VAC");
    }

    public void ingresarFechaDelIndice(String fecha) {
        hpcPage.txtFechaIndiceVAC.click();
        hpcPage.txtFechaIndiceVAC.sendKeys(fecha);
        logger.info("Se ingreso la fecha del Indice VAC");
    }

    public void ingresarValorDelIndice(String valorIndice) {
        hpcPage.txtValorIndiceVAC.click();
        hpcPage.txtValorIndiceVAC.sendKeys(valorIndice);
        logger.info("Se ingreso valor del Indice VAC");
    }

    public void buscarElUltimoRegistroDeIndiceVACIngresado(String fecha) {
        hpcPage.txtFechaFin.click();
        hpcPage.txtFechaFin.clear();
        hpcPage.txtFechaFin.sendKeys(fecha);
        hpcPage.btnBuscar.click();
        logger.info("Se valido el ingreso del índice");
    }

    public void seleccionarElRegistroDelIndice() {
        hpcPage.clickGrillaMantenimiento.click();
        logger.info("Se selecciono el último registro de la grilla");
    }

    public void seleccionarLaOpcionFactorBono() {
        hpcPage.opcFactorBono.click();
        cambioFrame();
        logger.info("Se selecciono la opcion Factor Bono");
    }

    public void ingresarFechaDelFactor(String fechaFactor) {
        hpcPage.txtFechaFactor.click();
        hpcPage.txtFechaFactor.sendKeys(fechaFactor);
        logger.info("Se ingreso la fecha del factor");
    }

    public void ingresarValorDelFactorEnSoles(String factorSoles) {
        hpcPage.txtFactorSoles.click();
        hpcPage.txtFactorSoles.sendKeys(factorSoles);
        logger.info("Se ingreso el valor del Factor para moneda Soles");
    }

    public void ingresarValorDelFactorEnDolares(String factorDolares) {
        hpcPage.txtFactorDolares.click();
        hpcPage.txtFactorDolares.sendKeys(factorDolares);
        logger.info("Se ingreso el valor del Factor para moneda Dolares");
    }

    public void buscarElUltimoRegistroDeFactoresIngresado(String fecha) {
        hpcPage.txtFechaFactorFin.click();
        hpcPage.txtFechaFactorFin.clear();
        hpcPage.txtFechaFactorFin.sendKeys(fecha);
        hpcPage.btnBuscar.click();
        logger.info("Se busco el último registro de factores");
    }

    public void seleccionarElRegistroDeLosFactores() {
        hpcPage.registroFactor.click();
    }

    public void ingresarAPestanaCronograma() {
        hpcPage.pestanaCronograma.click();
        pause(2);
    }

    public void validarQueElValorDelSaldoEnElCronogramaSeaCorrecto() {
        boolean SaldosIguales = false;
        hpcPage.txtSaldo_SeccionDetalle.click();
        String saldo_SeccionDetalle = hpcPage.txtSaldo_SeccionDetalle.getValue();

        for (int i = 2 ; i <= 9 ; i++) {
            WebElement cuota = getDriver().findElement(By.xpath("//table[@id='dgrCronograma']/tbody/tr["+i+"]/td[1]"));
            WebElement situacion = getDriver().findElement(By.xpath("//table[@id='dgrCronograma']/tbody/tr["+i+"]/td[3]"));
            WebElement saldo = getDriver().findElement(By.xpath("//table[@id='dgrCronograma']/tbody/tr["+i+"]/td[7]"));

            if (!cuota.getText().equals("-") && !situacion.getText().equals("CANCELADO")
                    && saldo_SeccionDetalle.equals(saldo.getText())){
                SaldosIguales = true;
            }
        }

        Assert.assertTrue(SaldosIguales);
        logger.info("SALDOS SON IGUALES");
        util.renombrarCarpeta();

    }

    public void scrollHaciaLaDerecha() {
        hpcPage.ultimoElemento.click();
        hpcPage.penultimoElemento.click();
    }

    public void avanzarALaSiguientePagina() {
        hpcPage.avanzarSiguientePagina.click();
    }

    public void avanzarHastaLaUltimaPagina() {
        hpcPage.avanzarHastaUltimaPagina.click();
    }

    public void tomarEvidenciasCronogramas() throws IOException {

        if (!hpcPage.cmbTipoCronograma.isVisible()){
            metodoEvidencias("Cronograma");
        }
        else {
            metodoEvidencias("TNC CLIENTE");

            hpcPage.cmbTipoCronograma.selectByVisibleText("TC CLIENTE");
            hpcPage.btnBuscar.click();
            hpcPage.cantidadRegistros.click();
            if(!hpcPage.cantidadRegistros.getText().equals("0")) {
                metodoEvidencias("TC CLIENTE");
            }else { util.tomarEvidenciaHPC("TC CLIENTE"); }

            hpcPage.cmbTipoCronograma.selectByVisibleText("TNC ADEUDADO");
            hpcPage.btnBuscar.click();
            hpcPage.cantidadRegistros.click();
            if(!hpcPage.cantidadRegistros.getText().equals("0")) {
                metodoEvidencias("TNC ADEUDADO");
            }else { util.tomarEvidenciaHPC("TNC ADEUDADO"); }

            hpcPage.cmbTipoCronograma.selectByVisibleText("TC ADEUDADO");
            hpcPage.btnBuscar.click();
            hpcPage.cantidadRegistros.click();
            if(!hpcPage.cantidadRegistros.getText().equals("0")) {
                metodoEvidencias("TC ADEUDADO");
            }else { util.tomarEvidenciaHPC("TC ADEUDADO"); }

            hpcPage.cmbTipoCronograma.selectByVisibleText("COBERTURADO");
            hpcPage.btnBuscar.click();
            hpcPage.cantidadRegistros.click();
            if(!hpcPage.cantidadRegistros.getText().equals("0")) {
                metodoEvidencias("COBERTURADO");
            }else { util.tomarEvidenciaHPC("COBERTURADO"); }

            hpcPage.cmbTipoCronograma.selectByVisibleText("NO COBERTURADO");
            hpcPage.btnBuscar.click();
            hpcPage.cantidadRegistros.click();
            if(!hpcPage.cantidadRegistros.getText().equals("0")) {
                metodoEvidencias("NO COBERTURADO");
            }else { util.tomarEvidenciaHPC("NO COBERTURADO"); }
        }
        util.renombrarCarpeta();
    }

    private void metodoEvidencias(String nombreCronograma) throws IOException {

        int cantRegistrosPendientes = 0;

        WebElement situacionPrimerRegistro = getDriver().findElement(By.xpath("//table[@id='dgrCronograma']/tbody/tr[2]/td[3]"));

        for (int i = 2 ; i <= 9 ; i++){
            WebElement situacion = getDriver().findElement(By.xpath("//table[@id='dgrCronograma']/tbody/tr["+i+"]/td[3]"));
            if (situacion.getText().equals("PENDIENTE")){
                cantRegistrosPendientes++;
            }
        }

        if (situacionPrimerRegistro.getText().equals("PENDIENTE") &&
                hpcPage.nroPagina.getValue().equals("1")){
            capturaEvidencias(nombreCronograma);
        }
        else if(situacionPrimerRegistro.getText().equals("PENDIENTE") &&
                Integer.parseInt(hpcPage.nroPagina.getValue()) > 1){

            retrocederALaPaginaAnterior();
            capturaEvidencias(nombreCronograma);

            avanzarALaSiguientePagina();
            capturaEvidencias(nombreCronograma);
        }
        else if (cantRegistrosPendientes <= 3){

            capturaEvidencias(nombreCronograma);
            avanzarALaSiguientePagina();
            capturaEvidencias(nombreCronograma);
        }
        else {
            capturaEvidencias(nombreCronograma);
        }
    }

    public void capturaEvidencias(String nombreCronograma) throws IOException {
        util.tomarEvidenciaHPC(nombreCronograma + " - Pag" + hpcPage.nroPagina.getValue());
        scrollHaciaLaDerecha();
        util.tomarEvidenciaHPC(nombreCronograma + " derecha" + " - Pag" + hpcPage.nroPagina.getValue());
    }

    public void retrocederALaPaginaAnterior() {
        hpcPage.retrocederPaginaAnterior.click();
    }

    public void retrocederALaPrimeraPagina() {
        hpcPage.retrocederHastaPrimeraPagina.click();
    }

    public void tomarEvidenciaDetalleDelCredito() throws IOException {
        util.tomarEvidencia("Detalle credito - parte 1");
        hpcPage.txtFechaIngreso.click();
        util.tomarEvidencia("Detalle credito - parte 2");
        hpcPage.txtTipoSeguroDesgravamen.click();
        hpcPage.seccionSeguroDesgravamen.click();
        util.tomarEvidencia("Detalle credito - parte 3");
    }

    public void seleccionarLaOpcionDeBloqueo() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcBloqueo.click();
        } else { hpcPage.opcBloqueoPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar Opcion de Bloqueo");

    }

    public void ingresarComentario() throws IOException {
        hpcPage.comentario.click();
        hpcPage.comentario.sendKeys(Constant.Bloqueo_Comentario);
        util.tomarEvidencia("Ingresar Comentario");
    }

    public void seleccionarLaOpcionDeDesBloqueo() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcDesbloqueo.click();
        } else { hpcPage.opcDesbloqueoPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion de Desbloqueo");
    }



    public void seleccionarLaOpcionDeCambioProductoHipotecario() throws IOException {
        hpcPage.opcCambioProductoHipotecario.click();
        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion de Cambio de Producto Hipotecario");

    }

    public void seleccionarLaOperacionPrepagoCFV() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcPrepagoCFV.click();
        } else { hpcPage.opcPrepagoCFVPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Opcion de Prepago CFV");
    }



    public void seleccionarLaOpcionReenganche() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcReenganche.click();
        } else { hpcPage.opcReenganchePersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar Reenganche");

    }

    public void descargarExcel() throws FindFailed {


        hpcPage.btnDescargarExcel.click();

        pause(5);
     /*   Actions builder = new Actions(getDriver());
        Action series = builder
                .moveByOffset(160,364)
                .click()
                .build();

        series.perform();

       */

        Screen screen = new Screen();
        screen.click ("src\\test\\resources\\img\\nombreprepago.png");

    }

    public void ingresarMontoParcialAPagar(String montoParcial) throws IOException {
        hpcPage.importePagar.click();
        hpcPage.importePagar.clear();
        hpcPage.importePagar.sendKeys(montoParcial);
        util.tomarEvidencia("se ingresa monto parcial");
    }

    public void seleccionarUnaCuota() throws IOException {
        WebElement myElement = getDriver().findElement(By.id("dgLista_ctl02_chkSeleccion"));
        myElement.click();
        util.tomarEvidencia("selecciona un cuota");
    }

    public void ingresarMontoSolicitado() throws IOException {
        hpcPage.txtMontoSolicitado.click();
        hpcPage.txtMontoSolicitado.clear();
        hpcPage.txtMontoSolicitado.sendKeys("0");
        hpcPage.txtMontoBeneficiario.click();

        String montoBeneficiario= hpcPage.txtMontoBeneficiario.getValue();
        double monto = Double.parseDouble(montoBeneficiario.replace(",",""));

        String montoAdicional= Constant.Reenganche_MontoAdicional;
        double adicional= Double.parseDouble(montoAdicional);

        double suma = -1 * monto + adicional;
        String sumaMonto = String.valueOf(suma);

        hpcPage.txtMontoSolicitado.click();
        hpcPage.txtMontoSolicitado.clear();
        hpcPage.txtMontoSolicitado.sendKeys(sumaMonto);

        util.tomarEvidencia("Ingresar Monto Solicitado");
    }

    //JCC--------------------------------------------------------------------------------




    public void seleccionarLaOpcionDeInclusionCuotaFlexible() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcInclusionCuotaFlexible.click();
        } else { hpcPage.opcInclusionCuotaFlexiblePersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Se selecciona la opcion de Inclusion Cuota Flexible");
    }

    public void ingresarDatosDeInclusionDeCuotaFlexibleCF(String Anio,String Mes) throws IOException {
        hpcPage.inclusionCF_Anio.click();
        hpcPage.inclusionCF_Anio.clear();
        hpcPage.inclusionCF_Anio.sendKeys(Anio);

        hpcPage.inclusionCF_Mes.click();
        hpcPage.inclusionCF_Mes.clear();
        hpcPage.inclusionCF_Mes.sendKeys(Mes);

        util.tomarEvidencia("Se ingresa los Valores de Año y Mes");

        logger.info("Se ingresaron los valores de Año y Mes " + Anio + Mes);
    }

    public void seleccionarTipo() throws IOException {
        hpcPage.inclusionCF_Tipo.click();
        hpcPage.inclusionCF_Tipo.selectByVisibleText(Constant.InclusionCuotaFlexible_Tipo);

        util.tomarEvidencia("Seleccionar el Tipo");

    }



    public void ingresarFechaFechaValor(String fechaValor) throws IOException {
        hpcPage.inclusionCFV_Fecha.click();
        hpcPage.inclusionCFV_Fecha.clear();
        hpcPage.inclusionCFV_Fecha.sendKeys(fechaValor);
        util.tomarEvidencia("Se ingresa Fecha Valor" + fechaValor);

        logger.info("Fecha Valor de Inclusión Cuota Flexible CFV" + fechaValor);
    }

    public void seleccionarLaOpcionDeInclusionCuotaFlexibleCFV() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcInclusionCuotaFlexibleCFV.click();
        } else { hpcPage.opcInclusionCuotaFlexibleCFVPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Se selecciona la opción de Inclusión Cuota Flexible CFV");
    }

    public void seleccionarTipoParaInclusionCuotaFlexibleCFV(String tipo) throws IOException {
        pause(5);
        hpcPage.inclusionCF_Tipo.click();
        hpcPage.inclusionCF_Tipo.selectByVisibleText(tipo);
        util.tomarEvidencia("Selecciona la opción de Tipo" + tipo);
    }

    public void seleccionarLaOpcionInclusionCuotaReprogramada() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcInclusionCuotaReprogramada.click();
        } else { hpcPage.opcInclusionCuotaReprogramadaPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la opción Cuota Reprogramada");
    }

    public void ingresarCuotaCero(String nroCuotaCero) throws IOException {
        hpcPage.inclusionCReprog_NroCuotaCero.click();
        hpcPage.inclusionCReprog_NroCuotaCero.sendKeys(nroCuotaCero);
        util.tomarEvidencia("Se ingresa el Nro de Cuota Cero");
    }

    public void ingresarNroDeCuotas(String nroCuotas) throws IOException {
        hpcPage.inclusionCReprog_NroCuotasCero.click();
        hpcPage.inclusionCReprog_NroCuotasCero.sendKeys(nroCuotas);
        util.tomarEvidencia("Se ingresa el número de cuotas");
    }

    public void seleccionarLaOpcionAnulacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAnulacion.click();
        } else { hpcPage.opcAnulacionPersonal.click();}

       util.tomarEvidencia("Seleccionar la opción Anulación");
    }

    public void seleccionarLaOpcionAnulacionPagoDeCuotas() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAnulacionPagoDeCuotas.click();
        } else { hpcPage.opcAnulacionPagoDeCuotasPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la opción Anulación Pago de Cuotas");
    }

    public void ingresarDatosDelPago() throws IOException {
        hpcPage.saldoPendiente.click();
        String saldoPendiente = hpcPage.saldoPendiente.getValue();

        hpcPage.importePagar.click();
        hpcPage.importePagar.clear();
        hpcPage.importePagar.sendKeys(saldoPendiente);

        util.tomarEvidencia("Monto a pagar");
    }

    public void eliminarRegistrosDeLaGrilla(String cantCuotas) throws IOException {

        int nroCuotas = Integer.parseInt(cantCuotas);
        nroCuotas = nroCuotas + 1;

        for (int i = 0; i < nroCuotas; i++) {

                WebElement myElement =getDriver().findElement(By.xpath("//table[@id='dgrdCred']/tbody/tr[2]/td[2]"));
                myElement.click();

                hpcPage.btnEliminar.click();
                Alert alert = getDriver().switchTo().alert();
                alert.accept();
                alert.accept();
                logger.info("Se selecciono boton eliminar");
                pause(2);
                util.tomarEvidencia("Registro eliminado");
        }
    }

    public void ingresarFechaValorDeDesembolso(String fechaValor) throws IOException {

        //String FechaActual = hpcPage.lblFechaActual.getAttribute("value").toString();
        hpcPage.lstFechaDesembolso.click();

        String[] parts = fechaValor.split("/");
        String dia = parts[0];  // 20
        int diaE = Integer.parseInt(dia); // 20
        String mes = parts[1];  // 01
        int mesE = Integer.parseInt(mes); //1
        String anio = parts[2]; // 2019

        getDriver().switchTo().frame("gToday:calendario:../../Scripts/Calendario.js");

        Select valorAnio = new Select(getDriver().findElement(By.id("YearSelect")));
        valorAnio.selectByValue(anio);

        Select valorMes = new Select(getDriver().findElement(By.id("MonSelect")));
        valorMes.selectByValue(String.valueOf(mesE));

        List<WebElement> fechas = getDriver().findElements(By.xpath("//a[contains(.,'"+diaE+"')]"));
        for (WebElement fechaB : fechas) {
            if(fechaB.getText().trim().equals(diaE + "")) {
                fechaB.click();
                break;
            }
        }

        getDriver().switchTo().parentFrame();

        util.tomarEvidencia("Fecha valor");
        logger.info("Se ingresó fecha valor");
    }

    public void capturarNumeroDeCreditoGenerado() throws IOException {
        hpcPage.nroCreditoGenerado.click();
        String nroCredito = hpcPage.nroCreditoGenerado.getValue();
        util.tomarEvidencia("Se genero el credito " + nroCredito);
        logger.info("Se generó el crédito " + nroCredito);

        Constant.numCredito = nroCredito;
        util.renombrarCarpeta();
        pause(2);
    }

    public void seleccionarLaOpcionAnulacionPrepago() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAnulacionPrepago.click();
        } else { hpcPage.opcAnulacionPrepagoPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Anulación Prepago");

    }

    public void seleccionarLaOpcionAnulacionCancelacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAnulacionCancelacion.click();
        } else { hpcPage.opcAnulacionCancelacionPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la Anulación Cancelación");

    }

    public void seleccionarLaOpcionSimuladorPagoDeCuotas() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorPagoCuotas.click();
        } else { hpcPage.opcSimuladorPagoCuotasPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la opción Simulador Pago de Cuotas");
    }

    public void seleccionarLaOpcionSimuladorCuotaReprogramada() throws IOException {

        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorCuotaReprogramada.click();
        } else { hpcPage.opcSimuladorCuotaReprogramadaPersonal.click();}

        cambioFrame();
        util.tomarEvidencia("Seleccionar la opción Simulador Cuota Reprogramada");
    }


    public void ingresarValorDelInteresMoratorio() {
        hpcPage.txtInteresMoratorio.waitUntilVisible().click();
        //hpcPage.txtInteresMoratorio.click();
        hpcPage.txtInteresMoratorio.sendKeys(Constant.AjusteCuota_interesMoratorio);
    }


    public void ingresarElImporteAsegurado(String montoDesembolso) throws IOException {
        hpcPage.txtImporteAsegurado.click();
        hpcPage.txtImporteAsegurado.sendKeys(montoDesembolso);
        logger.info("Se ingreso el monto del Importe Asegurado");
        util.tomarEvidencia("Importe Asegurado");

    }

    public void seleccionarLaOpcionSimuladorCuotaFlexible() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorCuotaFlexible.click();
        } else { hpcPage.opcSimuladorCuotaFlexiblePersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Simulador Cuota Flexible");
        logger.info("click en Simulador Cuota Flexible");
        cambioFrame();
    }

    public void seleccionarLaOpcionSimuladorModificacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorModificacion.click();
        } else { hpcPage.opcSimuladorModificacionPersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Simulador Modificacion");
        logger.info("click en Simulador Modificacion");
        cambioFrame();

    }


    public void seleccionarLaOpcionSimulacionCambioDeVencimiento() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcSimuladorCambioVencimiento.click();
        } else { hpcPage.opcSimuladorCambioVencimientoPersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Simulador Cambio de Vencimiento");
        logger.info("click en Simulador Cambio de Vencimiento");
        cambioFrame();

    }

    public void ingresarDiaDeVencimientoDeLaSimulacion() throws IOException {
        hpcPage.diaVencimiento.click();
        hpcPage.diaVencimiento.sendKeys(Constant.CambioVencimiento_DiaVencimiento);
        util.tomarEvidencia("Datos cambio de vencimiento");
    }


    public void seleccionarLaOpcionAnulacionModificacion() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
            hpcPage.opcAnulacionModificacion.click();
        } else { hpcPage.opcAnulacionModificacionPersonal.click();}

        util.tomarEvidencia("Se selecciono la Opcion de Modificacion");
        logger.info("click en Modificacion");
        cambioFrame();
    }

    public void ingresarMotivo() throws IOException {
        hpcPage.txtMotivo.click();
        hpcPage.txtMotivo.sendKeys(Constant.Motivo);
        util.tomarEvidencia("Se ingresa Motivo");

    }

    public void seleccionarLaOpcionCargaMasiva() {
        hpcPage.opcCargaMasiva.click();
    }

    public void seleccionarLaOpcionCronogramaMasivo() {
        hpcPage.opcCronogramaMasivo.click();
    }

    public void seleccionarExaminar() throws FindFailed {
        String a = "HojaResumen_00000000000000001924";

        Screen screen = new Screen();
        screen.click("src\\test\\resources\\img\\CronogramaMasivo_btnExaminar.png");
        screen.type("src\\test\\resources\\img\\CronogramaMasivo_txtNombre.png",a);
        screen.click("src\\test\\resources\\img\\CronogramaMasivo_btnAbrir.png");

    }

    public void seleccionarLaOpcionAdeudado() throws IOException {
        hpcPage.opcAdeudado.click();
        logger.info("Se selecciono la opcion Adeudado");
        util.tomarEvidencia("Seleccionar la Opcion de Adeudado");

    }

    public void seleccionarLaOpcionLiquidacionAdeudado() throws IOException {
        hpcPage.opcLiqAdeudado.click();
        cambioFrame();
        logger.info("Se selecciono la Opcion Liquidacion Adeudado");
        util.tomarEvidencia("Selecciona la opcion Liquidacion Adeudado");
    }

    public void seleccionarLaOpcionLiquidacionCoberturado() throws IOException {
        hpcPage.opcLiqCoberturado.click();
        cambioFrame();
        logger.info("Se selecciono la Opcion Liquidacion Coberturado");
        util.tomarEvidencia("Selecciona la opcion Liquidacion Coberturado");
    }

    public void seleccionarLaOpcionAnulacionLiquidacionCoberturado() throws IOException {
        hpcPage.opcAnulacionLiqCoberturado.click();
        cambioFrame();
        logger.info("Se selecciono la Opcion Anulacion Liquidacion Coberturado");
        util.tomarEvidencia("Selecciona la opcion Anulacion Liquidacion Coberturado");
    }

    public void seleccionarLaOpcionAnulacionCoberturado() throws IOException {
        hpcPage.opcAnulacionCoberturado.click();
        cambioFrame();
        logger.info("Se selecciono la Opcion Anulacion Coberturado");
        util.tomarEvidencia("Selecciona la opcion Anulacion Coberturado");

    }

    public void seleccionarLaOpcionReportes() throws IOException {
        hpcPage.opcReportes.click();
        logger.info("Se selecciono la opcion Reportes");
        util.tomarEvidencia("Seleccionar la Opcion de Reportes");


    }

    public void seleccionarLaOpcionReporteDesembolsos() throws IOException {
        if(Constant.codGrupo.equals("1") || Constant.codGrupo.equals("2")) {
        hpcPage.opcReporteDesembolsos.click();
        } else { hpcPage.opcReporteDesembolsosPersonal.click();}

        cambioFrame();
        logger.info("Se selecciono la opcion Reporte Desembolsos");
        util.tomarEvidencia("Seleccionar la Opcion de Reporte Desembolsos");
    }

    public void ingresarFechaIngresoDel(String fechaIngresoDel) throws IOException {
        hpcPage.opcFechaIngresoDel.click();
        hpcPage.opcFechaIngresoDel.clear();
        hpcPage.opcFechaIngresoDel.sendKeys(fechaIngresoDel);
        logger.info("Se ingresa la Fecha de Ingreso ");
        util.tomarEvidencia("Ingresar Fecha ");


    }

    public void ingresarFechaIngresoAl(String fechaIngresoAl) throws IOException {
        hpcPage.opcFechaIngresoAl.click();
        hpcPage.opcFechaIngresoAl.clear();
        hpcPage.opcFechaIngresoAl.sendKeys(fechaIngresoAl);
        logger.info("Se ingresa la Fecha de Fin ");
        util.tomarEvidencia("Ingresar Fecha Fin ");


    }

    public void seleccionarBotonBuscar() {
        hpcPage.btnBuscar.click();
    }

    public void seleccionarLaOpcionCreditosRelacionados() throws IOException {
        hpcPage.opcCreditoRelacionados.click();
        cambioFrame();
        logger.info("Seleccionar la Opcion Creditos Relacionados ");
        util.tomarEvidencia("Seleccionar la Opcion Creditos Relacionados ");

    }

    public void ingresarLosDatosDeLaRelacion(String grupoPrincipal, String grupoAsociado, String nroCreditoPrincipal, String nroCreditoAsociado) throws IOException {
        hpcPage.txtGrupoPrincipal.click();
        hpcPage.txtGrupoPrincipal.selectByVisibleText(grupoPrincipal);
        hpcPage.txtNroCreditoPrincipal.click();
        hpcPage.txtNroCreditoPrincipal.sendKeys(nroCreditoPrincipal);
        hpcPage.txtNroCreditoPrincipal.sendKeys(Key.ENTER);
        pause(3);

        hpcPage.txtGrupoAsociado.click();
        hpcPage.txtGrupoAsociado.selectByVisibleText(grupoAsociado);
        hpcPage.txtNroCreditoAsociado.click();
        hpcPage.txtNroCreditoAsociado.sendKeys(nroCreditoAsociado);
        hpcPage.txtNroCreditoAsociado.sendKeys(Key.ENTER);

        pause(3);

        hpcPage.comentario.click();
        hpcPage.comentario.sendKeys(Constant.Comentario);

        logger.info("Ingresar los Datos de la Relacion ");
        util.tomarEvidencia("Ingresar los Datos de la Relacion");


    }


    public void seleccionarRegistroRelacionado(String nroCreditoPrincipal) {
        hpcPage.nroCreditoGenerado.click();
        hpcPage.nroCreditoGenerado.sendKeys(nroCreditoPrincipal);
        hpcPage.btnBuscar.click();
        hpcPage.grillaCreditoRelacionado.click();


    }



    public void ingresarMotivoDelCambio() {
        hpcPage.motivoCambio.click();
        hpcPage.motivoCambio.sendKeys(Constant.MotivoCambio);


    }

    public void seleccionarLaOpcionInclusionCuotaMiTaxi() throws IOException {
        hpcPage.opcInclusionCuotaMiTaxi.click();
        cambioFrame();
        logger.info("Seleccionar la Opcion Inclusion Cuota Mi Taxi ");
        util.tomarEvidencia("Seleccionar la Opcion Inclusion Cuota Mi Taxi");

    }

    public void seleccionarLaOpcionConfirmacionCofide() throws IOException {
        hpcPage.opcConfirmacionCofide.click();
        cambioFrame();
        logger.info("Seleccionar la Opcion Confirmacion Cofide");
        util.tomarEvidencia("Seleccionar la opcion Confirmacion Cofide");

    }

    public void ingresarFechaDelProcesoDel(String Fecha) {
        hpcPage.opcFechaIngresoDel.click();
        hpcPage.opcFechaIngresoDel.clear();
        hpcPage.opcFechaIngresoDel.sendKeys(Fecha);
        hpcPage.opcFechaIngresoDel.sendKeys(Key.ENTER);
        pause(3);
    }

    public void seleccionarRegistroGrillaConfirmacionCofide() {
        hpcPage.GrillaConfirmacionCofide.click();
        pause(3);

    }

    public void seleccionarBotonAbrir() {
        hpcPage.btnAbrir.click();
    }

    public void ingresarFechaLiquidacion(String FechaLiquidacion) {
        //String fLiquidacion = hpcPage.fecha.getValue();
        hpcPage.fechaLiquidacion.click();
        hpcPage.fechaLiquidacion.sendKeys(FechaLiquidacion);
        hpcPage.fechaLiquidacion.sendKeys(Key.ENTER);
         }


    public void ingresarObservacionConfirmacionCofide() throws IOException {
        hpcPage.observacion.click();
        hpcPage.observacion.sendKeys(Constant.Observacion);

        util.tomarEvidencia("Se ingresa Comentario");

    }

    public void seleccionarLaOpcionDeAsignacionDeBonos() throws IOException {
        hpcPage.opcAsignacionDeBonos.click();
        cambioFrame();
        logger.info("Seleccionar la Opcion Asignacion de Bonos ");
        util.tomarEvidencia("Seleccionar la Opcion Asignacion de Bonos");

    }

    public void seleccionarCreditoDelaGrilla() throws IOException {
        hpcPage.grilla.click();
        util.tomarEvidencia("Seleccionar registro de la grilla Asignacion de Bonos");
    }

    public void seleccionarCheckDelRegistro() {
        hpcPage.checkAsigBonos.click();
    }

    public void seleccionarLaOpcionDeTitulizacionDeCreditos() throws IOException {
        hpcPage.opcTitulizacionDeCreditos.click();
        cambioFrame();
        logger.info("Seleccionar la Opcion Titulizacion de Creditos ");
        util.tomarEvidencia("Seleccionar la Opcion Titulizacion de Creditos");
    }

    public void seleccionarEntidad(String entidad) throws IOException {
        hpcPage.cmbEntidad.click();
        hpcPage.cmbEntidad.selectByVisibleText(entidad);
        logger.info("Seleccionar Entidad");
        util.tomarEvidencia("Seleccionar Entidad");
        pause(3);
    }

    public void seleccionarPatrimonio() throws IOException {
        hpcPage.cmbPatrimonio.click();
        logger.info("Seleccionar Patrimonio");
        util.tomarEvidencia("Seleccionar Patrimonio");
            }


    public void ingresarNroPortafolio(String nroPortafolio) throws IOException {
        hpcPage.nroPortafolio.click();
        hpcPage.nroPortafolio.sendKeys(nroPortafolio);
        hpcPage.nroPortafolio.sendKeys(Key.ENTER);
        logger.info("Ingresar Nro Portafolio");
        util.tomarEvidencia("Ingresar Nro Portafolio");
    }

    public void seleccionarTipoDesasignacion(String tipoDesasignacion) throws IOException {
        hpcPage.tipoDesasignacion.click();
        hpcPage.tipoDesasignacion.selectByVisibleText(tipoDesasignacion);
        logger.info("Seleccionar Tipo Desasignacion");
        util.tomarEvidencia("Seleccionar Tipo Desasignacion");
    }


    public void ingresarBonoBMS(String bonoBMS) throws IOException {
        hpcPage.importeBMS.click();
        hpcPage.importeBMS.clear();
        hpcPage.importeBMS.sendKeys(bonoBMS);
        hpcPage.indBBP.click();
        logger.info("Ingresar Bono BMS");
        util.tomarEvidencia("Ingresar Bono BMS");



    }


    public void ingresarElNumeroFondoMV(String nroFMVCofide) throws IOException {
        hpcPage.nroFMV.click();
        hpcPage.nroFMV.clear();
        hpcPage.nroFMV.sendKeys(nroFMVCofide);
        logger.info("Ingresar Nro FMV");
        util.tomarEvidencia("Ingresar Nro FMV");

        }

    public void btnCerrarDesembolso() {
    }
}

