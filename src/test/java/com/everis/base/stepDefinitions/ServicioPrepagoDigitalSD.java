package com.everis.base.stepDefinitions;

import com.everis.base.steps.ServicioPrepagoDigitalSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class ServicioPrepagoDigitalSD {

    @Steps
    ServicioPrepagoDigitalSteps servicioPrepagoDigitalSteps;

    @Then("Ingresar a la url del servicio Simulacion Prepago Digital")
    public void ingresarALaUrlDelServicioSimulacionPrepagoDigital() {
        servicioPrepagoDigitalSteps.ingresarALaUrlDelServicioSimulacionPrepagoDigital();
    }

    @And("Ingresar numero de credito a simular {string}")
    public void ingresarNumeroDeCreditoASimular(String nroCredito) {
        servicioPrepagoDigitalSteps.ingresarNumeroDeCreditoASimular(nroCredito);
    }

    @And("Invocar servicio")
    public void invocarServicio() throws IOException {
        servicioPrepagoDigitalSteps.invocarServicio();
    }

    @And("Ingresar periodo de gracia {string}")
    public void ingresarPeriodoDeGracia(String periodoGracia) {
        servicioPrepagoDigitalSteps.ingresarPeriodoDeGracia(periodoGracia);

    }

    @And("Ingresar monto de prepago {string}")
    public void ingresarMontoDePrepago(String montoPrepago) {
        servicioPrepagoDigitalSteps.ingresarMontoDePrepago(montoPrepago);
    }
}