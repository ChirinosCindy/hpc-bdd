package com.everis.base.stepDefinitions;

import com.everis.base.steps.GeneralSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import io.cucumber.java.en.And;

import java.io.IOException;

public class GeneralSD {

    @Steps
    GeneralSteps generalSteps;

    @Then("Ingresar al SDA")
    public void IngresarAlSDA() {
        generalSteps.openSDA();
        generalSteps.RegistroLogin();
        generalSteps.ContraLogin();
        generalSteps.seleccionarAmbienteDePrueba();
        generalSteps.AceptarLogin();
    }

    @Then("Ingresar al SDA con usuario admin")
    public void ingresarAlSDAConUsuarioAdmin() {
        generalSteps.ingresarAlSDAconUsuarioAdmin();
    }

    @And("Seleccionar el Grupo {string}")
    public void seleccionarGrupo(String grupo) {
        generalSteps.clickGrupo(grupo);
    }

    @And("Seleccionar el Grupo Upgrade {string}")
    public void seleccionarGrupoUpgrade(String grupo) {
        generalSteps.clickGrupoUpgrade(grupo);
    }

    @And("Seleccionar el Grupo {string} admin")
    public void seleccionarElGrupoAdmin(String grupo) {
        generalSteps.seleccionarElGrupoAdmin(grupo);
    }

    @Given("Se quiere realizar una operacion de {string}")
    public void seQuiereRealizarUnaOperacionDe(String nombreCarpetaEvidencias) {
        generalSteps.seQuiereRealizarUnaOperacionDe(nombreCarpetaEvidencias);
    }

    @Given("La operacion se realiza por administrativo")
    public void LaOperacionSeRealizaPorAdministrativo(){
    }

}