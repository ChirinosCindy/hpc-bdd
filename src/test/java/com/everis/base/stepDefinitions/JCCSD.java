package com.everis.base.stepDefinitions;

import com.everis.base.steps.JCCSteps;
import io.cucumber.java.en.And;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class JCCSD {

    @Steps
    JCCSteps jccSteps;

    @And("Seleccionar la Opcion Transacciones")
    public void seleccionarLaOpcionTransacciones() {
        jccSteps.seleccionarLaOpcionTransacciones();
    }

    @And("Seleccionar la Opcion Pago Estandar")
    public void seleccionarLaOpcionPagoEstandar() {
        jccSteps.seleccionarLaOpcionPagoEstandar();
    }

    @And("Seleccionar Nuevo registro")
    public void seleccionarNuevoRegistro() throws IOException {
        jccSteps.seleccionarNuevoRegistro();
    }

    @And("Ingresar numero de Operacion {string}")
    public void ingresarNumeroDeOperacion(String nroOperacion) throws IOException {
        jccSteps.ingresarNumeroDeOperacion(nroOperacion);
    }

    @And("Ingresar el monto a pagar {string}")
    public void ingresarElMontoAPagar(String montoPagar) throws IOException {
        jccSteps.ingresarElMontoAPagar(montoPagar);
    }

    @And("Ingresar el monto a pagar")
    public void ingresarElMontoAPagar() throws IOException {
        jccSteps.ingresarElMontoAPagar();
    }

    @And("Seleccionar el motivo del pago estandar {string}")
    public void seleccionarElMotivoDelPagoEstandar(String motivoPago) throws IOException {
        jccSteps.seleccionarElMotivoDelPagoEstandar(motivoPago);
    }

    @And("Ingresar texto en observacion")
    public void ingresarTextoEnObservacion() throws IOException {
        jccSteps.ingresarTextoEnObservacion();
    }


    @And("Guardar registro")
    public void guardarRegistro() {
        jccSteps.guardarRegistro();
    }

    @And("Guardar nuevo registro")
    public void guardarNuevoRegistro() throws IOException {
        jccSteps.guardarNuevoRegistro();
    }

    @And("Seleccionar operacion en la grilla")
    public void seleccionarOperacionEnLaGrilla() throws IOException {
        jccSteps.seleccionarOperacionEnLaGrilla();
    }

    @And("Eliminar registro")
    public void eliminarRegistro() {
        jccSteps.eliminarRegistro();
    }


    @And("Seleccionar la Opcion Pago Especial")
    public void seleccionarLaOpcionPagoEspecial() {
        jccSteps.seleccionarLaOpcionPagoEspecial();


    }


    @And("Ingresar Observacion")
    public void ingresarObservacion()throws IOException   {
        jccSteps.ingresarObservacion();
    }

    @And("Seleccionar registro en la grilla")
    public void seleccionarRegistroEnLaGrilla() throws IOException {
        jccSteps.seleccionarRegistroEnLaGrilla();
    }


    @And("Confirmar operacion")
    public void confirmarOperacion() throws IOException {
        jccSteps.confirmarOperacion();

    }

    @And("Seleccionar la Opcion Ingresar Operacion")
    public void seleccionarLaOpcionIngresarOperacion() {
        jccSteps.seleccionarLaOpcionIngresarOperacion();
    }

    @And("Ingresar codigo del producto {string}")
    public void ingresarCodigoDelProducto(String codProducto) {
        jccSteps.ingresarCodigoDelProducto(codProducto);
    }

    @And("Ingresar codigo unico del cliente {string}")
    public void ingresarCodigoUnicoDelCliente(String codUnicoCliente) {
        jccSteps.ingresarCodigoUnicoDelCliente(codUnicoCliente);
    }

    @And("Seleccionar el tipo de moneda {string}")
    public void seleccionarElTipoDeMoneda(String tipoMoneda) {
        jccSteps.seleccionarElTipoDeMoneda(tipoMoneda);
    }

    @And("Ingresar capital origen {string}")
    public void ingresarCapitalOrigen(String capitalOrigen) {
        jccSteps.ingresarCapitalOrigen(capitalOrigen);
    }

    @And("Ingresar interes compensatorio origen {string}")
    public void ingresarInteresCompensatorioOrigen(String intCompOrigen) {
        jccSteps.ingresarInteresCompensatorioOrigen(intCompOrigen);
    }

    @And("Ingresar interes compensatorio vencido origen {string}")
    public void ingresarInteresCompensatorioVencidoOrigen(String intCompVencOrigen) {
        jccSteps.ingresarInteresCompensatorioVencidoOrigen(intCompVencOrigen);
    }

    @And("Ingresar interes moratorio origen {string}")
    public void ingresarInteresMoratorioOrigen(String intMoratorio) {
        jccSteps.ingresarInteresMoratorioOrigen(intMoratorio);
    }

    @And("Ingresar interes diferido origen {string}")
    public void ingresarInteresDiferidoOrigen(String intDiferidoOrigen) {
        jccSteps.ingresarInteresDiferidoOrigen(intDiferidoOrigen);
    }

    @And("Ingresar tasa interes compensatorio origen {string}")
    public void ingresarTasaInteresCompensatorioOrigen(String tasaIntCompOrigen) {
        jccSteps.ingresarTasaInteresCompensatorioOrigen(tasaIntCompOrigen);
    }

    @And("Ingresar tasa interes compensatorio vencido origen {string}")
    public void ingresarTasaInteresCompensatorioVencidoOrigen(String tasaIntCompVencOrigen) {
        jccSteps.ingresarTasaInteresCompesnsatorioVencidoOrigen(tasaIntCompVencOrigen);
    }

    @And("Ingresar tasa interes compensatorio vencido {string}")
    public void ingresarTasaInteresCompensatorioVencido(String tasaIntCompVenc) {
        jccSteps.ingresarTasaInteresCompensatorioVencido(tasaIntCompVenc);
    }

    @And("Ingresar tasa interes moratorio {string}")
    public void ingresarTasaInteresMoratorio(String tasaIntMoratorio) {
        jccSteps.ingresarTasaInteresMoratorio(tasaIntMoratorio);
    }

    @And("Ingresar gastos origen {string}")
    public void ingresarGastosOrigen(String gastosOrigen) {
        jccSteps.ingresarGastosOrigen(gastosOrigen);
    }

    @And("Ingresar seguros origen {string}")
    public void ingresarSegurosOrigen(String segurosOrigen) {
        jccSteps.ingresarSegurosOrigen(segurosOrigen);
    }

    @And("Ingresar comision origen {string}")
    public void ingresarComisionOrigen(String comisionOrigen) {
        jccSteps.ingresarComisionOrigen(comisionOrigen);
    }

    @And("Ingresar fecha inicio calculo {string}")
    public void ingresarFechaInicioCalculo(String fechaInicioCalculo) {
        jccSteps.ingresarFechaInicioCalculo(fechaInicioCalculo);
    }

    @And("Ingresar fecha desembolso {string}")
    public void ingresarFechaDesembolso(String fechaDesembolso) {
        jccSteps.ingresarFechaDesembolso(fechaDesembolso);
    }

    @And("Ingresar fecha vencimiento {string}")
    public void ingresarFechaVencimiento(String fechaVencimiento) {
        jccSteps.ingresarFechaVencimiento(fechaVencimiento);
    }

    @And("Ingresar codigo estudio de abogados {string}")
    public void ingresarCodigoEstudioDeAbogados(String codEstudioAbog) {
        jccSteps.ingresarCodigoEstudioDeAbogados(codEstudioAbog);
    }

    @And("Seleccionar aplicativo origen {string}")
    public void seleccionarAplicativoOrigen(String aplicativoOrigen) {
        jccSteps.seleccionarAplicativoOrigen(aplicativoOrigen);
    }

    @And("Ingresar numero de Operacion Anterior {string}")
    public void ingresarNumeroDeOperacionAnterior(String nroOperacionAnterior) {
        jccSteps.ingresarNumeroDeOperacionAnterior(nroOperacionAnterior);
    }

    @And("Ingresar tienda de colocacion {string}")
    public void ingresarTiendaDeColocacion(String tdaColocacion) throws IOException {
        jccSteps.ingresarTiendaDeColocacion(tdaColocacion);
    }

    @And("Ingresar Monto Capital")
    public void ingresarMontoCapital() {
        jccSteps.ingresarMontoCapital();

    }


    @And("Seleccionar el motivo del pago especial {string}")
    public void seleccionarElMotivoDelPagoEspecial(String motivoPago) throws IOException {
        jccSteps.seleccionarElMotivoDelPagoEspecial(motivoPago);
    }

    @And("Ingresar texto en observacion del Pago Especial")
    public void ingresarTextoEnObservacionDelPagoEspecial() throws IOException{
        jccSteps.ingresarTextoEnObservacionDelPagoEspecial();
    }

    @And("Ingresar Interes Compensatorio")
    public void ingresarInteresCompensatorio()throws IOException {
        jccSteps.ingresarInteresCompensatorio();
    }

    @And("Ingresar Interes Moratorio")
    public void ingresarInteresMoratorio()throws IOException {
        jccSteps.ingresarInteresMoratorio();

    }

    @And("Ingresar Gastos")
    public void ingresarGastos() throws IOException{
        jccSteps.ingresarGastos();
    }

    @And("Ingresar Gastos Protesto")
    public void ingresarGastosProtesto() throws IOException{
        jccSteps.ingresarGastosProtesto();

    }

    @And("Ingresar Seguros")
    public void ingresarSeguros() throws IOException{
        jccSteps.ingresarSeguros() ;

    }

    @And("Ingresar Honorarios")
    public void ingresarHonorarios() throws IOException {
        jccSteps.ingresarHonorarios();
    }

    @And("Ingresar Comisiones")
    public void ingresarComisiones() throws IOException {
        jccSteps.ingresarComisiones();

    }

    @And("Ingresar Capital {string}")
    public void ingresarCapital(String Capital) throws IOException {
        jccSteps.ingresarCapital(Capital);

    }

    @And("Ingresar Monto Interes Compensatorio {string}")
    public void ingresarMontoInteresCompensatorio(String InteresCompensatorio)throws IOException {
        jccSteps.ingresarMontoInteresCompensatorio(InteresCompensatorio);

    }

    @And("Ingresar Monto Interes Moratorio {string}")
    public void ingresarMontoInteresMoratorio(String InteresMoratorio)throws IOException {
        jccSteps.ingresarMontoInteresMoratorio(InteresMoratorio);

    }

    @And("Ingresar Monto Gastos {string}")
    public void ingresarMontoGastos(String Gastos)throws IOException {
        jccSteps.ingresarMontoGastos(Gastos);

    }

    @And("Ingresar Monto Gastos Protesto {string}")
    public void ingresarMontoGastosProtesto(String GastosProtesto)throws IOException {
        jccSteps.ingresarMontoGastosProtesto(GastosProtesto);

    }

    @And("Ingresar Monto Seguros {string}")
    public void ingresarMontoSeguros(String Seguros)throws IOException {
        jccSteps.ingresarMontoSeguros(Seguros);

    }

    @And("Ingresar Monto Honorarios {string}")
    public void ingresarMontoHonorarios(String Honorarios)throws IOException {
        jccSteps.ingresarMontoHonorarios(Honorarios);

    }

    @And("Ingresar Monto Comisiones {string}")
    public void ingresarMontoComisiones(String Comisiones)throws IOException {
        jccSteps.ingresarMontoComisiones(Comisiones);

    }

    @And("Seleccionar la Opcion Incrementos")
    public void seleccionarLaOpcionIncrementos() throws IOException {
        jccSteps.seleccionarLaOpcionIncrementos();
    }

    @And("Ingresar Monto Interes Diferido {string}")
    public void ingresarMontoInteresDiferido(String InteresDiferido) throws IOException {
        jccSteps.ingresarMontoInteresDiferido(InteresDiferido);
    }

    @And("Ingresar Monto interes compensatorio vencido origen {string}")
    public void ingresarMontoInteresCompensatorioVencidoOrigen(String intCompVencOrigen) throws IOException {
        jccSteps.ingresarMontoInteresCompensatorioVencidoOrigen(intCompVencOrigen);
    }

    @And("Ingresar Monto interes moratorio origen {string}")
    public void ingresarMontoInteresMoratorioOrigen(String intMoratorio) throws IOException {
        jccSteps.ingresarMontoInteresMoratorioOrigen(intMoratorio);
    }

    @And("Ingresar Monto gastos origen {string}")
    public void ingresarMontoGastosOrigen(String gastosOrigen) throws IOException {
        jccSteps.ingresarMontoGastosOrigen(gastosOrigen);

    }

    @And("Ingresar Monto seguros origen {string}")
    public void ingresarMontoSegurosOrigen(String segurosOrigen) throws IOException {
        jccSteps.ingresarMontoSegurosOrigen(segurosOrigen);

    }

    @And("Seleccionar la Opción Modificaciones")
    public void seleccionarLaOpciónModificaciones() throws IOException {
        jccSteps.seleccionarLaOpciónModificaciones();
    }

    @And("Ingresar valor de Honorarios {string}")
    public void ingresarValorDeHonorarios(String honorarios) throws IOException {
        jccSteps.ingresarValorDeHonorarios(honorarios);
    }

    @And("Seleccionar la Opcion Anulacion de Pago")
    public void seleccionarLaOpcionAnulacionDePago() throws IOException {
        jccSteps.seleccionarLaOpcionAnulacionDePago();
    }

    @And("Seleccionar Pagos a Anular")
    public void seleccionarPagosAAnular() throws IOException {
        jccSteps.seleccionarPagosAAnular();

    }
}