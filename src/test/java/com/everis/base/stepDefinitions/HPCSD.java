package com.everis.base.stepDefinitions;

import com.everis.base.steps.HPCSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import org.sikuli.script.FindFailed;
import java.io.IOException;

public class HPCSD {

    @Steps
    HPCSteps hpcSteps;

    //Desembolso

    @And("Seleccionar la Opcion Operaciones")
    public void seleccionarlaOpcionOperaciones()throws IOException {
        hpcSteps.clickPestanaOperaciones();
    }

    @And("Seleccionar la Opcion Operaciones Upgrade")
    public void seleccionarlaOpcionOperacionesUpgrade()throws IOException {
        hpcSteps.clickPestanaOperacionesUpgrade();
    }

    @And("Seleccionar la Opcion Creditos")
    public void seleccionarlaOpcionCreditos()throws IOException {
        hpcSteps.clickOpcionCreditos();
    }

    @And("Seleccionar la Opcion Creditos Upgrade")
    public void seleccionarlaOpcionCreditosUpgrade()throws IOException {
        hpcSteps.clickOpcionCreditosUpgrade();
    }

    @And("Seleccionar la Opcion Ingreso")
    public void SeleccionarOpcionIngreso() throws IOException {
        hpcSteps.clickSubOpcionIngreso();
    }

    @And("Seleccionar la Opcion Ingreso Upgrade")
    public void SeleccionarOpcionIngresoUpgrade() throws IOException {
        hpcSteps.clickSubOpcionIngresoUpgrade();
    }

    @And ("Seleccionar la Opcion de Modificacion")
    public void seleccionarOpcionModificacion()throws IOException{
        hpcSteps.clickSubOpcionModificacion();
    }

    @And("Seleccionar Boton Nuevo")
    public void SeleccionarBotonNuevo() throws IOException {
        hpcSteps.SeleccionarBotonNuevo();
    }

    @And("Seleccionar Boton Nuevo Upgrade")
    public void SeleccionarBotonNuevoUpgrade() throws IOException {
        hpcSteps.SeleccionarBotonNuevo();
    }

    @Given("capturar pantallas")
    public void capturarPantallas(){}

    @When("antes de operacion")
    public void antesDeOperacion(){}

    @And("click en Consultas")
    public void clickEnConsultas() {
        hpcSteps.clickOpcConsultas();
    }

    @And("click en Consulta del Credito")
    public void clickEnConsultaDelCredito(){
        hpcSteps.clickConsultaDelCredito();
    }

    @And("Ingresar credito {string}")
    public void ingresarCredito(String nroCredito) throws IOException {
        hpcSteps.ingresarNroCredito(nroCredito);
    }

    @And("Cambiar el valor de la tasa {string}")
    public void CambiarValorDeLaTasa(String TasaInteres) throws IOException {
        hpcSteps.cambiarValorTasa(TasaInteres);
    }

    @And("Ingresar el codigo del producto {string}")
    public void ingresarElCodigoDelProducto(String nroProducto) throws IOException {
        hpcSteps.ingresarElCodigoDelProducto(nroProducto);
    }

    @And("Seleccionar la moneda {string}")
    public void seleccionarLaMoneda (String cmbMoneda) throws IOException {
        hpcSteps.seleccionarLaMoneda(cmbMoneda);
    }

    @And("Ingresar CU del cliente {string}")
    public void ingresarCUDelCliente(String nrCU) {
        hpcSteps.ingresarCUDelCliente(nrCU);
    }

    @And("Ingresar codigo del promotor")
    public void ingresarCodigoDelPromotor() {
        hpcSteps.ingresarCodigoDelPromotor();
    }

    @And("Ingresar canal de venta")
    public void ingresarCanalDeVenta() {
        hpcSteps.ingresarCanalDeVenta();
    }

    @And("Ingresar codigo de tienda")
    public void ingresarCodigoDeTienda() {
        hpcSteps.ingresarCodigoDeTienda();
    }

    @And("Ingresar codigo de establecimiento")
    public void ingresarCodigoDeEstablecimiento() {
        hpcSteps.ingresarCodigoDeEstablecimiento();
    }

    @And("Ingresar datos de producto a refinanciar")
    public void ingresarDatosDeProductoARefinanciar() throws IOException {
        hpcSteps.ingresarDatosDeProductoARefinanciar();
    }

    @And("Seleccionar el tipo de evaluacion")
    public void seleccionarElTipoDeEvaluacion() {
        hpcSteps.seleccionarElTipoDeEvaluacion();
    }

    @And("Seleccionar la moneda del precio de venta {string}")
    public void seleccionarLaMonedaDelPrecioDeVenta(String Moneda) {
        hpcSteps.seleccionarLaMonedaDelPrecioDeVenta(Moneda);
    }

    @And("Ingresar el monto del precio de venta {string}")
    public void ingresarElMontoDelPrecioDeVenta(String precioVenta) {
        hpcSteps.ingresarElMontoDelPrecioDeVenta(precioVenta);
    }

    @And("Seleccionar la moneda del valor comercial {string}")
    public void seleccionarLaMonedaDelValorComercial(String cmbMoneda) {
        hpcSteps.seleccionarLaMonedaDelValorComercial(cmbMoneda);
    }

    @And("Ingresar el monto del valor comercial {string}")
    public void ingresarElMontoDelValorComercial(String valorComercial) throws IOException {
        hpcSteps.ingresarElMontoDelValorComercial(valorComercial);
    }

    @And("Ingresar el destino del credito")
    public void ingresarElDestinoDelCredito() {
        hpcSteps.ingresarElDestinoDelCredito();
    }

    @And("Ingresar el plazo {string}")
    public void ingresarElPlazo(String plazo) {
        hpcSteps.ingresarElPlazo(plazo);
    }

    @And("Ingresar la tasa {string}")
    public void ingresarLaTasa(String tasa) throws IOException {
        hpcSteps.ingresarLaTasa(tasa);
    }

    @And("Seleccionar envio de informe de pago")
    public void seleccionarEnvioDeInformeDePago() throws IOException {
        hpcSteps.seleccionarEnvioDeInformeDePago();
    }

    @And("Ingresar el tipo de abono")
    public void ingresarElTipoDeAbono() {
        hpcSteps.ingresarElTipoDeAbono();
    }

    @And("Ingresar el monto solicitado {string}")
    public void ingresarElMontoSolicitado(String MontoSolicitado) throws IOException {
        hpcSteps.ingresarElMontoSolicitado(MontoSolicitado);
    }

    @And("Ingresar la tasa del seguro del bien vehicular")
    public void ingresarLaTasaDelSeguroDelBienVehicular() throws IOException {
        hpcSteps.ingresarLaTasaDelSeguroDelBienVehicular();
    }

    @And("Ingresar monto asegurado {string}")
    public void ingresarMontoAsegurado(String montoAsegurado) throws IOException {
        hpcSteps.ingresarMontoAsegurado(montoAsegurado);
    }

    @And("ingresar Seguro del Bien {string}")
    public void ingresarSeguroDelBien(String PrecioVenta) throws IOException {
        hpcSteps.ingresarSeguroDelBien(PrecioVenta);
    }

    @And("Seleccionar la moneda del gravamen {string}")
    public void seleccionarLaMonedaDelGravamen(String monedaGravamen) {
        hpcSteps.seleccionarLaMonedaDelGravamen(monedaGravamen);
    }

    @And("Ingresar el monto de Gravamen {string}")
    public void ingresarElMontoDeGravamen(String montoGravamen) throws IOException {
        hpcSteps.ingresarElMontoDeGravamen(montoGravamen);
    }

    @And("Guardar Operacion")
    public void GuardarOperacion() throws IOException {
        hpcSteps.clickBtnGrabar();
    }

    @And ("Eliminar Operacion")
    public void EliminarOperacion() throws IOException {
        hpcSteps.clickBtnEliminar();
    }

    @Given("Se quiere realizar una operacion de Ajuste")
    public void seQuiereRealizarUnaOperacionDeAjuste() {
    }

    @And("Seleccionar la Opcion Ajuste")
    public void seleccionarLaOpcionAjuste() throws IOException{
        hpcSteps.clickSubOpcionAjuste();
    }

    @And("Seleccionar Registro de la Grilla")
    public void seleccionarRegistrodelaGrilla() throws IOException {
        hpcSteps.seleccionarRegistrodelaGrilla();
    }

    @And("ingresar Ajuste Desde la Cuota {string}")
    public void ingresarAjusteDesdeLaCuota(String AjusteCuota)throws IOException {
        hpcSteps.ingresarAjusteDesdelaCuota(AjusteCuota);
    }

    @Given("Se quiere realizar una operacion de Descargo")
    public void seQuiereRealizarUnaOperacionDeDescargo() {
    }

    @And("Seleccionar la Opcion Descargo")
    public void seleccionarLaOpcionDescargo() throws IOException{
        hpcSteps.clicSubopcionDescargo();

    }

    @And("Seleccionar Datos del Descargo")
    public void seleccionarDatosDelDescargo() throws IOException {
        hpcSteps.seleccionarDatosDelDescargo();
    }

    @When("Se quiere realizar una operacion de Reingreso")
    public void seQuiereRealizarUnaOperacionDeReingreso() {
    }

    @And("Seleccionar la Opcion Reingreso")
    public void seleccionarLaOpcionReingreso() throws IOException {
        hpcSteps.clickSubopcionReingreso();
    }

    @And("Salir del Formulario")
    public void SalirDelFormulario() throws IOException {
        hpcSteps.clickBtnCerrar();
    }

    @And("Salir del Formulario Desembolso")
    public void SalirDelFormularioDesembolso() throws IOException {
        hpcSteps.clickBtnCerrarDesembolso();
    }

    @When("Se quiere realizar una operacion de Simulador Especifico")
    public void seQuiereRealizarUnaOperacionDeSimuladorEspecifico() {
    }

    @And("Seleccionar la Opcion Simuladores")
    public void seleccionarLaOpcionSimuladores()throws IOException {
        hpcSteps.seleccionarLaOpcionSimuladores();
    }

    @And("Seleccionar la Opcion Simulador Especifico")
    public void seleccionarLaOpcionSimuladorEspecifico() {
    }

    @Given("Se quiere realizar una operacion de Cambio de Vencimiento")
    public void seQuiereRealizarUnaOperacionDeCambioDeVencimiento() {
    }

    @And("Seleccionar la Opcion Cambio de Vencimiento")
    public void seleccionarLaOpcionCambioDeVencimiento() throws IOException {
        hpcSteps.clickSubopcionCambioVencimiento();

    }

    @And("Ingresar Datos del Cambio de Vencimiento")
    public void ingresarDatosDelCambioDeVencimiento() throws IOException {
        hpcSteps.ingresarDatosDelCambioDeVencimiento();
    }

    @Given("Se quiere realizar una operacion de Ajuste de Cuota")
    public void seQuiereRealizarUnaOperacionDeAjusteDeCuota() {
    }

    @And("Seleccionar la Opcion Ajuste de Cuota")
    public void seleccionarLaOpcionAjusteDeCuota()throws IOException {
        hpcSteps.seleccionarLaOpcionAjusteDeCuota();
    }

    @And("Seleccionar del Cronograma de Cuotas la opcion Modificar {string}")
    public void seleccionarDelCronogramaDeCuotasLaOpcionModificar(String cantCuotas)throws IOException {
        hpcSteps.seleccionarDelCronogramaDeCuotasLaOpcionModificar(cantCuotas);

    }

    @And("ingresar valor del Interes Compensatorio")
    public void ingresarValorDelInteresCompensatorio() throws IOException {
        hpcSteps.ingresarValorDelInteresCompensatorio();
    }

    //@And("Guardar Valor ingresado en el Cronograma")
    //public void guardarValorIngresadoEnElCronograma() {
     //   hpcSteps.clickBtnGrabarCronograma();

    //}

    @Given("Se quiere realizar una operacion de Pago de Cuotas")
    public void seQuiereRealizarUnaOperacionDePagoDeCuotas() {
    }

    @And("Seleccionar la Opcion Pagos")
    public void seleccionarLaOpcionPagos() throws IOException{
        hpcSteps.SeleccionarLaOpcionPagos();

    }

    @And("Seleccionar la Opcion Pagos CFV")
    public void seleccionarLaOpcionPagosCFV()throws IOException {
        hpcSteps.SeleccionarLaOpcionPagosCFV();
    }



    @And("Seleccionar la Opcion de Pago de Cuotas")
    public void seleccionarLaOpcionDePagoDeCuotas()throws IOException {
        hpcSteps.seleccionarLaOpcionDePagoDeCuotas();
    }

    @And("pagar {string} cuotas")
    public void pagarCuotas(String cantCuotas) throws IOException {
        hpcSteps.pagarCuotas(cantCuotas);
    }

    @And("Ingresar Datos del Pago de Cuotas")
    public void ingresarDatosDelPago() throws IOException {
        hpcSteps.ingresarDatosDelPago();
    }


    @And("Seleccionar la Opcion Simulador Cancelacion CFV")
    public void seleccionarLaOpcionSimuladorCancelacionCFV() throws IOException {
        hpcSteps.seleccionarOpcionSimuladorCancelacionCFV();
    }

    @And("ingresar fecha valor {string} para simulacion")
    public void ingresarFechaValorParaSimulacion(String fechaValor)throws IOException {
        hpcSteps.ingresarFechaValorParaSimulacion(fechaValor);
    }

    @And("Simular Operacion")
    public void simularOperacion() throws IOException {
        hpcSteps.simularOperacion();
    }


    @Given("Se quiere realizar una operacion de Simulacion Cancelacion CFV")
    public void SeQuiereRealizarUnaOperacionDeSimulacionCancelacionCFV() {

    }


    @Given("Se quiere realizar una operacion de Prepago")
    public void seQuiereRealizarUnaOperacionDePrepago(){ }

    @And("Seleccionar la Opcion Prepago")
    public void seleccionarLaOpcionPrepago() throws IOException{
        hpcSteps.seleccionarLaOpcionPrepago();
    }

    @And("ingresar Datos del Prepago {string} {string}")
    public void ingresarDatosDelPrepago(String montoNeto, String modalidad)throws IOException {
        hpcSteps.ingresarDatosDelPrepago(montoNeto,modalidad);


    }

    @And("Seleccionar la Opcion Cancelacion CFV")
    public void seleccionarLaOpcionCancelacionCFV() throws IOException {
        hpcSteps.seleccionarLaOpcionCancelacionCFV();
    }

    @And("Seleccionar medio de cargo")
    public void seleccionarMedioDeCargo()throws IOException {
        hpcSteps.selccionarMedioDeCargo();
    }

    @And("ingresar fecha valor {string}")
    public void ingresarFechaValor(String fechaValor) throws  IOException{
        hpcSteps.ingresarFechaValor(fechaValor);
    }

    @Given("Se quiere realizar una operacion de Cancelacion CFV")
    public void seQuiereRealizarUnaOperacionDeCancelacionCFV() {
    }


    @Given("Se quiere realizar una operacion de Pago de Cuotas CFV")
    public void seQuiereRealizarUnaOperacionDePagoDeCuotasCFV() {
    }

    @Then("Seleccion la Opcion Pago de Cuotas CFV")
    public void seleccionLaOpcionPagoDeCuotasCFV() throws IOException {
        hpcSteps.seleccionLaOpcionPagoDeCuotasCFV();
    }

    @And("pagar {string} cuotas CFV")
    public void pagarCuotasCFV(String cantCuotas) {
        hpcSteps.pagarCuotasCFV(cantCuotas);

    }

    @Given("Se quiere realizar una operacion de Cancelacion")
    public void seQuiereRealizarUnaOperacionDeCancelacion() {

    }

    @And("Seleccionar la Opcion Cancelacion")
    public void seleccionarLaOpcionCancelacion() throws IOException {
        hpcSteps.seleccionarLaOpcionCancelacion();
    }

    @Given("Se quiere realizar una operacion de Simulacion Cancelacion")
    public void seQuiereRealizarUnaOperacionDeSimulacionCancelacion() {

    }

    @And("Seleccionar la Opcion Simulador Cancelacion")
    public void seleccionarLaOpcionSimuladorCancelacion() {
        hpcSteps.seleccionarLaOpcionSimuladorCancelacion();
    }

    @Given("Se quiere realizar una operacion de Simulacion Prepago CFV")
    public void seQuiereRealizarUnaOperacionDeSimulacionPrepagoCFV() {

    }

    @And("Seleccionar la Opcion Simulador Prepago CFV")
    public void seleccionarLaOpcionSimuladorPrepagoCFV() throws IOException {
        hpcSteps.seleccionarLaOpcionSimuladorPrepagoCFV();
    }

    @And("ingresar monto a simular en el prepago {string}")
    public void ingresarMontoASimularEnElPrepago(String montoSimulacionPrepago) {
        hpcSteps.ingresarMontoASimularEnElPrepago(montoSimulacionPrepago);
    }

    @And("ingresar fecha valor {string} para simulacion Prepago")
    public void ingresarFechaValorParaSimulacionPrepago(String fechaValor) {
        hpcSteps.ingresarFechaValorParaSimulacionPrepago(fechaValor);
    }

    @And("ingresar modalidad de prepago {string}")
    public void ingresarModalidadDePrepago(String modalidadPrepago) {
        hpcSteps.ingresarModalidadDePrepago(modalidadPrepago);
    }

    @Given("Se quiere realizar una operacion de Simulacion Prepago")
    public void seQuiereRealizarUnaOperacionDeSimulacionPrepago() {

    }

    @And("Seleccionar la Opcion Simulador Prepago")
    public void seleccionarLaOpcionSimuladorPrepago() throws IOException {
        hpcSteps.seleccionarLaOpcionSimuladorPrepago();
    }

    @Given("Se quiere ingresar nuevos valores del Indice VAC")
    public void seQuiereIngresarNuevosValoresDelIndiceVAC() {

    }

    @And("Seleccionar la Opcion Mantenimiento")
    public void seleccionarLaOpcionMantenimiento() {
        hpcSteps.seleccionarLaOpcionMantenimiento();
    }

    @And("Seleccionar la Opcion Indice VAC")
    public void seleccionarLaOpcionIndiceVAC() {
        hpcSteps.seleccionarLaOpcionIndiceVAC();
    }

    @And("Ingresar fecha del Indice {string}")
    public void ingresarFechaDelIndice(String fecha) {
        hpcSteps.ingresarFechaDelIndice(fecha);
    }

    @And("Ingresar valor del Indice {string}")
    public void ingresarValorDelIndice(String valorIndice) {
        hpcSteps.ingresarValorDelIndice(valorIndice);
    }

    @And("Buscar el ultimo registro de indice VAC ingresado {string}")
    public void buscarElUltimoRegistroDeIndiceVACIngresado(String fecha) {
        hpcSteps.buscarElUltimoRegistroDeIndiceVACIngresado(fecha);
    }

    @And("Seleccionar el registro del indice")
    public void seleccionarElRegistroDelIndice() {
        hpcSteps.seleccionarElRegistroDelIndice();
    }

    @Given("Se quiere ingresar nuevos factores para el calculo de intereses legales")
    public void seQuiereIngresarNuevosFactoresParaElCalculoDeInteresesLegales() {

    }

    @And("Seleccionar la Opcion Factor Bono")
    public void seleccionarLaOpcionFactorBono() {
        hpcSteps.seleccionarLaOpcionFactorBono();
    }

    @And("Ingresar fecha del factor {string}")
    public void ingresarFechaDelFactor(String fechaFactor) {
        hpcSteps.ingresarFechaDelFactor(fechaFactor);
    }

    @And("Ingresar valor del factor en soles {string}")
    public void ingresarValorDelFactorEnSoles(String factorSoles) {
        hpcSteps.ingresarValorDelFactorEnSoles(factorSoles);
    }

    @And("Ingresar valor del factor en dolares {string}")
    public void ingresarValorDelFactorEnDolares(String factorDolares) {
        hpcSteps.ingresarValorDelFactorEnDolares(factorDolares);
    }

    @And("Seleccionar el registro de los Factores")
    public void seleccionarElRegistroDeLosFactores() {
        hpcSteps.seleccionarElRegistroDeLosFactores();
    }

    @And("Buscar el ultimo registro de factores ingresado {string}")
    public void buscarElUltimoRegistroDeFactoresIngresado(String fecha) {
        hpcSteps.buscarElUltimoRegistroDeFactoresIngresado(fecha);
    }

    @And("ingresar a pestana cronograma")
    public void ingresarAPestanaCronograma() {
        hpcSteps.ingresarAPestanaCronograma();
    }

    @And("validar que el valor del saldo en el cronograma sea correcto")
    public void validarQueElValorDelSaldoEnElCronogramaSeaCorrecto() {
        hpcSteps.validarQueElValorDelSaldoEnElCronogramaSeaCorrecto();
    }

    @And("scroll hacia la derecha")
    public void scrollHaciaLaDerecha() {
        hpcSteps.scrollHaciaLaDerecha();
    }

    @And("avanzar a la siguiente pagina")
    public void avanzarALaSiguientePagina() {
        hpcSteps.avanzarALaSiguientePagina();
    }

    @And("avanzar hasta la ultima pagina")
    public void avanzarHastaLaUltimaPagina() {
        hpcSteps.avanzarHastaLaUltimaPagina();
    }

    @And("tomar evidencias Cronogramas")
    public void tomarEvidenciasCronogramas() throws IOException {
        hpcSteps.tomarEvidenciasCronogramas();
    }

    @And("retroceder a la pagina anterior")
    public void retrocederALaPaginaAnterior() {
        hpcSteps.retrocederALaPaginaAnterior();
    }

    @And("retroceder a la primera pagina")
    public void retrocederALaPrimeraPagina() {
        hpcSteps.retrocederALaPrimeraPagina();
    }

    @Given("Se quiere validar si ambos saldos son iguales")
    public void seQuiereValidarSiAmbosSaldosSonIguales() {

    }



    @Given("capturar pantallas del Detalle del Credito")
    public void capturarPantallasDelDetalleDelCredito() {
    }

    @And("tomar evidencia Detalle del Credito")
    public void tomarEvidenciaDetalleDelCredito() throws IOException {
        hpcSteps.tomarEvidenciaDetalleDelCredito();
    }

    @Given("Se quiere realizar una operacion de Bloqueo")
    public void seQuiereRealizarUnaOperacionDeBloqueo() {

    }

    @And("Seleccionar la Opcion de Bloqueo")
    public void seleccionarLaOpcionDeBloqueo() throws IOException{
        hpcSteps.seleccionarLaOpcionDeBloqueo();
    }

    @And("ingresar comentario")
    public void ingresarComentario() throws IOException{
        hpcSteps.ingresarComentario();

    }

    @Given("Se quiere realizar una operacion de DesBloqueo")
    public void seQuiereRealizarUnaOperacionDeDesBloqueo() {
    }


    @And("Seleccionar la Opcion de DesBloqueo")
    public void seleccionarLaOpcionDeDesBloqueo()throws IOException {
        hpcSteps.seleccionarLaOpcionDeDesBloqueo();

    }


    @And("Seleccionar la Opcion de Cambio Producto Hipotecario")
    public void seleccionarLaOpcionDeCambioProductoHipotecario() throws IOException{
        hpcSteps.seleccionarLaOpcionDeCambioProductoHipotecario();
    }


    @And("Seleccionar la Operacion Prepago CFV")
    public void seleccionarLaOperacionPrepagoCFV() throws IOException{
        hpcSteps.seleccionarLaOperacionPrepagoCFV();

    }


    @And("Seleccionar la Opcion Reenganche")
    public void seleccionarLaOpcionReenganche()throws IOException {
        hpcSteps.seleccionarLaOpcionReenganche();

    }

    @And("ingresar Monto Solicitado")
    public void ingresarMontoSolicitado()throws IOException {
        hpcSteps.ingresarMontoSolicitado();
    }

    @And("Descargar Excel")
    public void descargarExcel() throws FindFailed {
        hpcSteps.descargarExcel();
    }

    @And("ingresar monto parcial a pagar {string}")
    public void ingresarMontoParcialAPagar(String montoParcial) throws IOException {
        hpcSteps.ingresarMontoParcialAPagar(montoParcial);
    }

    @And("seleccionar una cuota")
    public void seleccionarUnaCuota() throws IOException {
        hpcSteps.seleccionarUnaCuota();
    }

    @And("Seleccionar la Opcion de Inclusion Cuota Flexible")
    public void seleccionarLaOpcionDeInclusionCuotaFlexible() throws IOException{
        hpcSteps.seleccionarLaOpcionDeInclusionCuotaFlexible();
    }

    @And("Ingresar Datos de Inclusion de Cuota flexible CF {string} {string}")
    public void ingresarDatosDeInclusionDeCuotaFlexibleCF(String Anio, String Mes) throws IOException{
        hpcSteps.ingresarDatosDeInclusionDeCuotaFlexibleCF(Anio,Mes);

    }

    @And("Seleccionar Tipo")
    public void seleccionarTipo()throws IOException {
        hpcSteps.seleccionarTipo();
    }


    @And("Seleccionar la Opcion de Inclusion Cuota Flexible CFV")
    public void seleccionarLaOpcionDeInclusionCuotaFlexibleCFV()throws IOException {
        hpcSteps.seleccionarLaOpcionDeInclusionCuotaFlexibleCFV();
    }

    @And("Ingresar Fecha Fecha Valor {string}")
    public void ingresarFechaFechaValor(String fechaValor)throws IOException {
        hpcSteps.ingresarFechaFechaValor(fechaValor);
    }

    @And("Seleccionar Tipo para Inclusion CuotaFlexible CFV {string}")
    public void seleccionarTipoParaInclusionCuotaFlexibleCFV(String tipo)throws IOException {
        hpcSteps.seleccionarTipoParaInclusionCuotaFlexibleCFV(tipo);
    }

    @And("Seleccionar la Opcion Inclusion Cuota Reprogramada")
    public void seleccionarLaOpcionInclusionCuotaReprogramada() throws IOException{
        hpcSteps.seleccionarLaOpcionInclusionCuotaReprogramada();

    }

    @And("Ingresar Cuota Cero {string}")
    public void ingresarCuotaCero(String nroCuotaCero)throws IOException {
        hpcSteps.ingresarCuotaCero(nroCuotaCero);

    }

    @And("Ingresar Nro de Cuotas {string}")
    public void ingresarNroDeCuotas(String nroCuotas)throws IOException {
        hpcSteps.ingresarNroDeCuotas(nroCuotas);
    }

    @And("Seleccionar la Opcion Anulacion")
    public void seleccionarLaOpcionAnulacion() throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacion();
    }

    @And("Seleccionar la Opcion Anulacion Pago de Cuotas")
    public void seleccionarLaOpcionAnulacionPagoDeCuotas() throws IOException{
        hpcSteps.seleccionarLaOpcionAnulacionPagoDeCuotas();
    }

    @And("Eliminar Registros de la Grilla {string}")
    public void eliminarRegistrosDeLaGrilla(String cantCuotas)throws IOException {
        hpcSteps.eliminarRegistrosDeLaGrilla(cantCuotas);
    }

    @And("ingresar fecha valor {string} de desembolso")
    public void ingresarFechaValorDeDesembolso(String fechaValor) throws IOException {
        hpcSteps.ingresarFechaValorDeDesembolso(fechaValor);
    }

    @And("Capturar numero de credito generado")
    public void capturarNumeroDeCreditoGenerado() throws IOException {
        hpcSteps.capturarNumeroDeCreditoGenerado();
    }

    @And("Seleccionar la Opcion Anulacion Prepago")
    public void seleccionarLaOpcionAnulacionPrepago() throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacionPrepago();

    }

    @And("Seleccionar la Opcion Anulacion Cancelacion")
    public void seleccionarLaOpcionAnulacionCancelacion()throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacionCancelacion();
    }

    @And("Seleccionar la Opcion Simulador Pago de Cuotas")
    public void seleccionarLaOpcionSimuladorPagoDeCuotas() throws IOException{
        hpcSteps.seleccionarLaOpcionSimuladorPagoDeCuotas();
    }

    @And("Seleccionar la Opcion Simulador Cuota Reprogramada")
    public void seleccionarLaOpcionSimuladorCuotaReprogramada() throws IOException{
        hpcSteps.seleccionarLaOpcionSimuladorCuotaReprogramada();
    }

    @And("Verificar que la operacion se haya realizado con exito")
    public void verificarQueLaOperacionSeHayaRealizadoConExito() throws IOException {
        hpcSteps.verificarQueLaOperacionSeHayaRealizadoConExito();
    }

    @And("Verificar que la operacion se haya eliminado")
    public void verificarQueLaOperacionSeHayaEliminado() throws IOException {
        hpcSteps.verificarQueLaOperacionSeHayaEliminado();
    }


    @And("ingresar valor del Interes Moratorio")
    public void ingresarValorDelInteresMoratorio() {
        hpcSteps.ingresarValorDelInteresMoratorio();
    }


    @And("Ingresar el importe asegurado {string}")
    public void ingresarElImporteAsegurado(String montoDesembolso) throws IOException {
        hpcSteps.ingresarElImporteAsegurado(montoDesembolso);

    }

    @And("Seleccionar la Opcion Simulador Cuota Flexible")
    public void seleccionarLaOpcionSimuladorCuotaFlexible() throws IOException {
        hpcSteps.seleccionarLaOpcionSimuladorCuotaFlexible();
    }

    @And("Seleccionar la Opcion Simulador Modificacion")
    public void seleccionarLaOpcionSimuladorModificacion() throws IOException {
        hpcSteps.seleccionarLaOpcionSimuladorModificacion();
    }



    @And("Seleccionar la Opcion Simulacion Cambio de Vencimiento")
    public void seleccionarLaOpcionSimulacionCambioDeVencimiento() throws IOException {
        hpcSteps.seleccionarLaOpcionSimulacionCambioDeVencimiento();
    }

    @And("Ingresar Dia de Vencimiento de la Simulacion")
    public void ingresarDiaDeVencimientoDeLaSimulacion() throws IOException {
        hpcSteps.ingresarDiaDeVencimientoDeLaSimulacion();
    }


    @And("Seleccionar la Opcion Anulacion Modificacion")
    public void seleccionarLaOpcionAnulacionModificacion() throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacionModificacion();

    }

    @And("Ingresar Motivo")
    public void ingresarMotivo() throws IOException {
        hpcSteps.ingresarMotivo();
    }

    @And("Seleccionar la Opcion Carga Masiva")
    public void seleccionarLaOpcionCargaMasiva() {
        hpcSteps.seleccionarLaOpcionCargaMasiva();
    }

    @And("Seleccionar la Opcion Cronograma Masivo")
    public void seleccionarLaOpcionCronogramaMasivo() {
        hpcSteps.seleccionarLaOpcionCronogramaMasivo();
    }

    @And("Seleccionar Examinar")
    public void seleccionarExaminar() throws FindFailed {
        hpcSteps.seleccionarExaminar();
    }

    @And("Seleccionar la Opcion Adeudado")
    public void seleccionarLaOpcionAdeudado() throws IOException {
        hpcSteps.seleccionarLaOpcionAdeudado();
    }

    @And("Seleccionar la Opcion Liquidacion Adeudado")
    public void seleccionarLaOpcionLiquidacionAdeudado() throws IOException {
        hpcSteps.seleccionarLaOpcionLiquidacionAdeudado();
    }

    @And("Seleccionar la Opcion Liquidacion Coberturado")
    public void seleccionarLaOpcionLiquidacionCoberturado() throws IOException {
        hpcSteps.seleccionarLaOpcionLiquidacionCoberturado();

    }

    @And("Seleccionar la Opcion Anulacion Liquidacion Coberturado")
    public void seleccionarLaOpcionAnulacionLiquidacionCoberturado() throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacionLiquidacionCoberturado();
    }

    @And("Seleccionar la Opcion Anulacion Coberturado")
    public void seleccionarLaOpcionAnulacionCoberturado() throws IOException {
        hpcSteps.seleccionarLaOpcionAnulacionCoberturado();
    }

    @And("Seleccionar la Opcion Reportes")
    public void seleccionarLaOpcionReportes() throws IOException {
        hpcSteps.seleccionarLaOpcionReportes();
    }

    @And("Seleccionar la Opcion Reporte Desembolsos")
    public void seleccionarLaOpcionReporteDesembolsos() throws IOException {
        hpcSteps.seleccionarLaOpcionReporteDesembolsos();
    }

    @And("Ingresar Fecha Ingreso del {string}")
    public void ingresarFechaIngresoDel(String fechaIngresoDel) throws IOException {
        hpcSteps.ingresarFechaIngresoDel(fechaIngresoDel);
    }

    @And("Ingresar Ffecha Ingreso al  {string}")
    public void ingresarFfechaIngresoAl(String fechaIngresoAl) throws IOException {
        hpcSteps.ingresarFechaIngresoAl(fechaIngresoAl);
    }

    @And("Seleccionar Boton Buscar")
    public void seleccionarBotonBuscar() {
        hpcSteps.seleccionarBotonBuscar();
    }

    @And("Seleccionar la Opcion Creditos Relacionados")
    public void seleccionarLaOpcionCreditosRelacionados() throws IOException {
        hpcSteps.seleccionarLaOpcionCreditosRelacionados();
    }


    @And("Ingresar los Datos de la Relacion {string} {string} {string} {string}")
    public void ingresarLosDatosDeLaRelacion(String grupoPrincipal, String grupoAsociado, String nroCreditoPrincipal, String nroCreditoAsociado) throws IOException {
        hpcSteps.ingresarLosDatosDeLaRelacion(grupoPrincipal,grupoAsociado,nroCreditoPrincipal,nroCreditoAsociado);
    }

    @And("Seleccionar Registro Relacionado {string}")
    public void seleccionarRegistroRelacionado(String nroCreditoPrincipal) {
        hpcSteps.seleccionarRegistroRelacionado(nroCreditoPrincipal);
    }


    @And("ingresar Motivo del Cambio")
    public void ingresarMotivoDelCambio() {
        hpcSteps.ingresarMotivoDelCambio();

    }

    @And("Seleccionar la Opcion Inclusion Cuota Mi Taxi")
    public void seleccionarLaOpcionInclusionCuotaMiTaxi() throws IOException {
        hpcSteps.seleccionarLaOpcionInclusionCuotaMiTaxi();
    }

    @And("Seleccionar la Opcion Confirmacion Cofide")
    public void seleccionarLaOpcionConfirmacionCofide() throws IOException {
        hpcSteps.seleccionarLaOpcionConfirmacionCofide();

    }

    @And("Ingresar Fecha del Proceso Del {string}")
    public void ingresarFechaDelProcesoDel(String Fecha) {
        hpcSteps.ingresarFechaDelProcesoDel(Fecha);
    }

    @And("Seleccionar registro Grilla Confirmacion Cofide")
    public void seleccionarRegistroGrillaConfirmacionCofide() {
        hpcSteps.seleccionarRegistroGrillaConfirmacionCofide();
    }

    @And("Seleccionar Boton Abrir")
    public void seleccionarBotonAbrir() {
        hpcSteps.seleccionarBotonAbrir();
    }

    @And("Ingresar Fecha Liquidacion {string}")
    public void ingresarFechaLiquidacion(String FechaLiquidacion) {
        hpcSteps.ingresarFechaLiquidacion(FechaLiquidacion);
    }

    @And("Ingresar Observacion Confirmacion Cofide")
    public void ingresarObservacionConfirmacionCofide() throws IOException {
        hpcSteps.ingresarObservacionConfirmacionCofide();
    }

    @And("Seleccionar la Opcion de Asignacion de Bonos")
    public void seleccionarLaOpcionDeAsignacionDeBonos() throws IOException {
        hpcSteps.seleccionarLaOpcionDeAsignacionDeBonos();

    }

    @And("Seleccionar credito de la grilla")
    public void seleccionarCreditoDelaGrilla() throws IOException {
        hpcSteps.seleccionarCreditoDelaGrilla();
    }

    @And("Seleccionar Check del registro")
    public void seleccionarCheckDelRegistro() {
        hpcSteps.seleccionarCheckDelRegistro();

    }

    @And("Seleccionar la Opcion de Titulizacion de Creditos")
    public void seleccionarLaOpcionDeTitulizacionDeCreditos() throws IOException {
        hpcSteps.seleccionarLaOpcionDeTitulizacionDeCreditos();
    }

    @And("Seleccionar Entidad {string}")
    public void seleccionarEntidad(String entidad) throws IOException {
        hpcSteps.seleccionarEntidad(entidad);
    }

    @And("Seleccionar Patrimonio")
    public void seleccionarPatrimonio() throws IOException {
        hpcSteps.seleccionarPatrimonio();

    }

    @And("Ingresar NroPortafolio {string}")
    public void ingresarNroPortafolio(String nroPortafolio) throws IOException {
        hpcSteps.ingresarNroPortafolio(nroPortafolio);
    }

    @And("Seleccionar Tipo Desasignacion {string}")
    public void seleccionarTipoDesasignacion(String tipoDesasignacion) throws IOException {
        hpcSteps.seleccionarTipoDesasignacion(tipoDesasignacion);
    }


    @And("ingresar BonoBMS {string}")
    public void ingresarBonoBMS(String BonoBMS) throws IOException{
        hpcSteps.ingresarBonoBMS(BonoBMS);

    }

    @And("Ingresar el numero Fondo MV {string}")
    public void ingresarElNumeroFondoMV(String nroFMVCofide) throws IOException {
        hpcSteps.ingresarElNumeroFondoMV(nroFMVCofide);
    }

    @And("Verificar registro Tarifa Dia Ayer")
    public void verificarRegistroTarifaDiaAyer() throws IOException {
        hpcSteps.verificarRegistroTarifaDiaAyer();

    }
}
