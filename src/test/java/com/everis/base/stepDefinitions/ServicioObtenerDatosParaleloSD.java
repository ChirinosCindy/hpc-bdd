package com.everis.base.stepDefinitions;

import com.everis.base.steps.ServicioObtenerDatosParaleloSteps;
import com.everis.base.steps.ServicioPrepagoDigitalSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class ServicioObtenerDatosParaleloSD {

    @Steps
    ServicioObtenerDatosParaleloSteps servicioObtenerDatosParaleloSteps;

    @Then("Ingresar a la url del servicio Obtener Datos Paralelo")
    public void ingresarALaUrlDelServicioObtenerDatosParalelo() {
        servicioObtenerDatosParaleloSteps.ingresarALaUrlDelServicioObtenerDatosParalelo();
    }

    @And("Ingresar numero de credito principal {string}")
    public void ingresarNumeroDeCreditoPrincipal(String nroCredito) {
        servicioObtenerDatosParaleloSteps.ingresarNumeroDeCreditoPrincipal(nroCredito);
    }

    @And("Invocar servicio Obtener Datos Paralelo")
    public void invocarServicioObtenerDatosParalelo() throws IOException {
        servicioObtenerDatosParaleloSteps.invocarServicioObtenerDatosParalelo();
    }

}