package com.everis.base.stepDefinitions;

import com.everis.base.steps.ServicioObtenerDatosPrepagoSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class ServicioObtenerDatosPrepagoSD {

    @Steps
    ServicioObtenerDatosPrepagoSteps servicioObtenerDatosPrepagoSteps;

    @Then("Ingresar a la url del servicio Obtener Datos Prepago")
    public void ingresarALaUrlDelServicioObtenerDatosPrepago() {
        servicioObtenerDatosPrepagoSteps.ingresarALaUrlDelServicioObtenerDatosPrepago();
    }

    @And("Ingresar numero de credito {string}")
    public void ingresarNumeroDeCredito(String nroCredito) {
        servicioObtenerDatosPrepagoSteps.ingresarNumeroDeCredito(nroCredito);
    }

    @And("Invocar servicio Obtener Datos Prepago")
    public void invocarServicioObtenerDatosPrepago() throws IOException {
        servicioObtenerDatosPrepagoSteps.invocarServicioObtenerDatosPrepago();
    }



}