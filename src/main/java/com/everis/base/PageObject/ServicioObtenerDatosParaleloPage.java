package com.everis.base.PageObject;

import com.everis.base.commons.Constant;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl(Constant.URL_ObtenerDatosParalelo)
public class ServicioObtenerDatosParaleloPage extends PageObject {

    @FindBy(name = "intNroCredito")
    public WebElementFacade nroCredito;

    @FindBy(xpath = "//div[@id='content']/span/form/table/tbody/tr[3]/td[2]/input")
    public WebElementFacade btnInvoke;

    @FindBy(xpath = "//div[@id='content']/p")
    public WebElementFacade txtNombreServicio;

    @FindBy(xpath = "//div[2]/div[2]/div[6]/div[2]/div[1]/div/span[5]")
    public WebElementFacade txtCodError;

    @FindBy(xpath = "//div[2]/div[2]/div[6]/div[2]/div[2]/div/span[5]")
    public WebElementFacade txtMensajeError;


}