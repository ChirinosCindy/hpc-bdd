package com.everis.base.PageObject;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@DefaultUrl ("http://intranetib/sda/")
public class HPCPage extends PageObject {

    //OPCIONES GENERICAS

    @FindBy(id = "fpAnimswapImgFP1")
    public WebElementFacade btnNuevo;

    @FindBy(id = "tblBarra_lnkAbrir")
    public WebElementFacade btnAbrir;

    @FindBy(id = "tblBarra_lnkEliminar")
    public WebElementFacade btnEliminar;

    @FindBy(id = "fpAnimswapImgFP6")
    public WebElementFacade btnCerrar;

    @FindBy(id="fpAnimswapImgFP6']")
    public WebElementFacade btnCerrarDesembolso;

    @FindBy(id = "fpAnimswapImgFP8")
    public WebElementFacade btnSimular;

    @FindBy(id = "txtRestanteNuevo")
    public WebElementFacade txtNuevasCuotasRestantes;

    @FindBy(id = "cvMontoPrePagoValidar")
    public WebElementFacade txtMensajeErrorSimulacionPrepago;

    @FindBy(id = "fpAnimswapImgFP10")
    public WebElementFacade btnDescargarExcel;

    @FindBy(id = "fpAnimswapImgFP4")
    public WebElementFacade btnBuscar;

    @FindBy(xpath = "//*[@id=\"frmCabecera\"]")
    public WebElementFacade ifrCabecera;

    @FindBy(xpath = "//*[@id=\"frmcontent\"]")
    public WebElementFacade ifrContent;

    @FindBy(xpath = "//*[@id=\"frmMain\"]")
    public WebElementFacade ifrmMain;


    //PESTANAS----------------------------------------------------------------------------------------------------------
    @FindBy(xpath="//span[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[1]/a")
    public WebElementFacade opcMantenimiento;


    @FindBy(xpath="//span[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[1]/a")
    public WebElementFacade opcOperaciones;

    @FindBy(xpath="//span[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[1]/a")
    public WebElementFacade opcOperacionesUpgrade;

    @FindBy(xpath="//*[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[2]/a")
    public WebElementFacade opcSimuladores;

    @FindBy(xpath="//span[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[3]/a")
    public WebElementFacade opcConsultas;

    @FindBy(id = "txtFechaIngreso")
    public WebElementFacade txtFechaIngreso;

    @FindBy(xpath = "//div[@id='wtpSeguroDesgravamen']/table[1]/tbody/tr/td[1]\n")
    public WebElementFacade seccionSeguroDesgravamen;

    @FindBy(id = "txtDesSeguroDesg")
    public WebElementFacade txtTipoSeguroDesgravamen;

    @FindBy(xpath = "//input[@id='ucBusquedaCredito_txtCodigo']")
    public WebElementFacade txtNroCredito;

    @FindBy(id = "txtNroCredito")
    public WebElementFacade txtNroCreditoCronograma;

    @FindBy(xpath = "txtTotalCancelar")
    public WebElementFacade montoTotalCancelar;

    @FindBy(id = "txtMontoPrePago")
    public WebElementFacade txtMontoAPrepagar;

    @FindBy(xpath = "//table[@id='tdContent']/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/div")
    public WebElementFacade pestanaCronograma;

    @FindBy(xpath="//span[@id='WucMenuHpc_lblMenu']/table/tbody/tr/td[4]/a")
    public WebElementFacade opcReportes;

    //OPCIONES - PESTANA MANTENIMIENTO--------------------------------------------------------------------------------

    @FindBy(xpath="//div[@id='011']/a[11]")
    public WebElementFacade opcIndiceVAC;

    @FindBy(xpath="//div[@id='011']/a[12]")
    public WebElementFacade opcFactorBono;

    @FindBy(id="ucFechaIndiceInicio_txtFecha")
    public WebElementFacade txtFechaInicio;

    @FindBy(id="ucFechaIndiceFin_txtFecha")
    public WebElementFacade txtFechaFin;

    @FindBy(id = "ucFechaIndice_txtFecha")
    public WebElementFacade txtFechaIndiceVAC;

    @FindBy(id="txtValorIndice")
    public WebElementFacade txtValorIndiceVAC;

    @FindBy(id="ucFechaFactorInicio_txtFecha")
    public WebElementFacade txtFechaFactorInicio;

    @FindBy(id="ucFechaFactorFin_txtFecha")
    public WebElementFacade txtFechaFactorFin;


    @FindBy(id = "ucFechaFactor_txtFecha")
    public WebElementFacade txtFechaFactor;

    @FindBy(id="txtFactorSoles")
    public WebElementFacade txtFactorSoles;

    @FindBy(id="txtFactorDolares")
    public WebElementFacade txtFactorDolares;

    @FindBy(id = "dgrdMntIndice_ctl03_hypCodigo")
    public  WebElementFacade registroIndiceVAC;

    @FindBy(xpath = "//table[@id='dgrdMntFactor']/tbody/tr[2]/td[2]")
    public  WebElementFacade registroFactor;


    //OPCIONES - PESTANA OPERACIONES--------------------------------------------------------------------------------

    @FindBy(id="txtNumCredito")
    public WebElementFacade nroCreditoGenerado;
                    //div[@id='021']/a[2]
    @FindBy(xpath="//div[@id='021']/a[2]")
    public WebElementFacade opcCreditos;

    @FindBy(xpath="//div[@id='021']/a[2]")
    public WebElementFacade opcCreditosUpgrade;

    @FindBy(xpath = "//div[@id='022']/a[2]")
    public WebElementFacade opcCreditosPersonal;

    @FindBy(xpath = "//div[@id='022']/a[2]")
    public WebElementFacade opcCreditosPersonalUpgrade;

    @FindBy(xpath="//div[@id='021']/a[3]")
    public WebElementFacade opcPagos;

    @FindBy(xpath="//div[@id='022']/a[3]")
    public WebElementFacade opcPagosPersonal;

    @FindBy(xpath="//*[@id=\"021\"]/a[4]")
    public WebElementFacade opcPagosCFV;

    @FindBy(xpath="//*[@id=\"022\"]/a[4]")
    public WebElementFacade opcPagosCFVPersonal;

    @FindBy(xpath="//div[@id='021_0070']/a[6]")
    public WebElementFacade opcSimuladorPagoDeCuotasCFV;

    @FindBy(xpath="//div[@id='021_0070']/a[4]")
    public WebElementFacade opcSimuladorPrepagoCFV;

    @FindBy(xpath="//div[@id='022_0070']/a[4]")
    public WebElementFacade opcSimuladorPrepagoCFVPersonal;

    @FindBy(xpath="//div[@id='031']/a[4]")
    public WebElementFacade opcSimuladorCancelacion;

    @FindBy(xpath="//div[@id='032']/a[4]")
    public WebElementFacade opcSimuladorCancelacionPersonal;

    @FindBy(xpath="//div[@id='021_0070']/a[5]")
    public WebElementFacade opcSimuladorCancelacionCFV;

    @FindBy(xpath="//div[@id='022_0070']/a[5]")
    public WebElementFacade opcSimuladorCancelacionCFVPersonal;

    @FindBy(xpath="//*[@id='021']/a[5]")
    public WebElementFacade opcAnulaciones;

    @FindBy(id="ucFechaSimulacion_txtFecha")
    public WebElementFacade txtFechaSimulacion;

    @FindBy(id="ucFecha_txtFecha")
    public WebElementFacade txtFechaValor;

    @FindBy(id="ucFechaPrePago_txtFecha")
    public WebElementFacade txtFechaValorPrepago;

    @FindBy(id="cvFechaSimulacionValidar")
    public WebElementFacade mensajeValidacionFechaSimulacion;

    //SUB OPCIONES - PESTANA CONSULTAS

    @FindBy(xpath="//*[@id='061']/a[1]")
    public WebElementFacade opcConsultaCredito;



    @FindBy(xpath="//*[@id='062']/a[1]")
    public WebElementFacade opcConsultaCreditoPersonal;

    @FindBy(id="txtSaldo")
    public WebElementFacade txtSaldo_SeccionDetalle;

    //SUB OPCIONES - OPCION CREDITOS
    //div[@id='021_0066']/a[1]
    @FindBy(xpath="//div[@id='021_0066']/a[1]")
    public WebElementFacade opcIngreso;

    @FindBy(xpath="//div[@id='021_0066']/a[1]")
    public WebElementFacade opcIngresoUpgrade;

    @FindBy(xpath = "//div[@id='022_0066']/a[1]")
    public WebElementFacade opcIngresoPersonal;

    @FindBy(xpath = "//div[@id='022_0066']/a[1]")
    public WebElementFacade opcIngresoPersonalUpgrade;

    // --- Modificacion
    @FindBy(xpath = "//div[@id='021_0066']/a[2]")
    public WebElementFacade opcModificacion;

    @FindBy(xpath = "//div[@id='022_0066']/a[2]")
    public WebElementFacade opcModificacionPersonal;

    //Ajuste         //div[@id='021_0066']/a[6]
    @FindBy(xpath = "//div[@id='021_0066']/a[6]")
    public WebElementFacade opcAjuste;

    @FindBy(xpath = "//div[@id='022_0066']/a[6]")
    public WebElementFacade opcAjustePersonal;

    //SUB OPCIONES - OPCION PAGOS---------------------------------------------------------------------------------------

    @FindBy(xpath="//div[@id='021_0069']/a[3]")
    public WebElementFacade opcPagoCuotas;

    @FindBy(xpath="//div[@id='022_0069']/a[3]")
    public WebElementFacade opcPagoCuotasPersonal;

    @FindBy(xpath="//*[@id=\"021_0069\"]/a[1]")
    public WebElementFacade opcPrepago;

    @FindBy(xpath="//*[@id=\"022_0069\"]/a[1]")
    public WebElementFacade opcPrepagoPersonal;

    @FindBy(xpath="//*[@id=\"021_0069\"]/a[2]")
    public WebElementFacade opcCancelacion;

    @FindBy(xpath="//*[@id=\"022_0069\"]/a[2]")
    public WebElementFacade opcCancelacionPersonal;

    @FindBy(xpath="//div[@id='021_0070']/a[2]")
    public WebElementFacade opcCancelacionCFV;

    @FindBy(xpath="//div[@id='022_0070']/a[2]")
    public WebElementFacade opcCancelacionCFVPersonal;


    //OPCION - PESTAnA OPERACIONES> CREDITO> INGRESO (Datos del Credito)
    // INGRESO (Datos del Principales)

    @FindBy(xpath = "//input[@id='ucBusquedaProducto_txtCodigo']")
    public WebElementFacade txtNroProducto;

    @FindBy(xpath = "//select[@id='ucListaMoneda_ddlLista']")
    public WebElementFacade cmbMoneda;

    //INGRESO (Datos Administrativos)

    @FindBy(id="ucBusquedaCodUnico_txtCodigo")
    public WebElementFacade txtCodUnico;

    @FindBy(id="ucBusquedaPromotor_txtCodigo")
    public WebElementFacade txtCodPromotor;

    @FindBy(id="UCBusquedaCanalVenta_txtCodigo")
    public WebElementFacade txtCodCanalVenta;

    @FindBy(id="ucBusquedaTienda_txtCodigo")
    public WebElementFacade txtCodBusquedaTienda;

    @FindBy(xpath ="//select[@id='ucListaTipoEvaluacion_ddlLista']")
    public WebElementFacade cmbTipoEvaluacion;

    @FindBy(xpath ="//select[@id='ucListaMonedaPrecioVenta_ddlLista']")
    public WebElementFacade cmbPrecioVenta;

    @FindBy(id="txtPrecioVentaInmueble")
    public WebElementFacade txtPrecioVenta;

    @FindBy(xpath = "//select[@id='ucListaMonedaValorComercial_ddlLista']")
    public WebElementFacade cmbValorComercial;

    @FindBy(id="txtValorComercialInmueble")
    public WebElementFacade txtValorComercial;

    @FindBy(id="UCBusquedaEstablecimiento_txtCodigo")
    public WebElementFacade txtCodigoEstablecimiento;

    @FindBy(id="txtSeguroDelBien")
    public WebElementFacade txtTasaSeguroBienVehicular;

    @FindBy(id="txtImporteAseg")
    public WebElementFacade txtImporteAseguradoVehicular;

    @FindBy(id="imgProductoRefinanciado")
    public WebElementFacade btnIngresoProductosRefinanciar;

    @FindBy(id="dgrPRef_ctl02_ucListaFPR_ddlLista")
    public WebElementFacade cmbProductoRefinanciado;

    @FindBy(id="dgrPRef_ctl02_txtFNumeroProductoRefinanciado")
    public WebElementFacade txtNProductoRefinanciado;

    @FindBy(id="dgrPRef_ctl02_txtFMontoSolicitado")
    public WebElementFacade txtMontoSolicitadoProductoRefinanciado;

    @FindBy(id="dgrPRef_ctl02_txtFDiferidoInteres")
    public WebElementFacade txtInteresDiferidoRefinanciado;

    @FindBy(id="dgrPRef_ctl02_txtFDiferidoComision")
    public WebElementFacade txtComisionDiferidoRefinanciado;

    @FindBy(id="dgrPRef_ctl02_txtFCodigoUnico")
    public WebElementFacade txtCodigoUnicoRefinanciado;


    @FindBy(xpath="//*[@id='dgrPRef_ctl02_lnkAddRow']/img")
    public WebElementFacade btnGuardarProductoRefinanciado;

    @FindBy(id="ibtnAceptar")
    public WebElementFacade btnAceptarProductoRefinanciado;


    @FindBy(id="ucBusquedaDestinoCred_txtCodigo")
    public WebElementFacade txtTipoDestino;

    @FindBy(id="txtplazoMeses")
    public WebElementFacade txtPlazo;

    @FindBy(id="txtTasaInteres")
    public WebElementFacade txtTasaInteres;

    @FindBy(id="lstIndTipoEnvioInforme")
    public WebElementFacade cmbEnvioInformePago;

    @FindBy(id="ucBusquedaTipoAbono_txtCodigo")
    public WebElementFacade txtTipoAbono;

    @FindBy(id="txtMontoSolicitado")
    public WebElementFacade txtMontoSolicitado;

    @FindBy(id="txtImporteAseg")
    public WebElementFacade txtImporteAsegurado;

    @FindBy(id="ucListaMonedaGravamen_ddlLista")
    public WebElementFacade txtMonedaGravamen;

    @FindBy(id="txtMontoGravamen")
    public WebElementFacade txtMontoGravamen;

    @FindBy(id = "fpAnimswapImgFP3")
    public WebElementFacade btnGrabar;


    @FindBy(xpath="//table[@id='dgrdCred']/tbody/tr[2]/td[2]")
    public WebElementFacade clickGrilla;

    @FindBy(xpath="//table[@id='dgrdMntIndice']/tbody/tr[2]/td[2]")
    public WebElementFacade clickGrillaMantenimiento;

    @FindBy(id="txtCuotaAjuste")
    public WebElementFacade cuotaAjuste;

    @FindBy(xpath = "//div[@id='021_0066']/a[3]")
    public WebElementFacade opcDescargo;

    @FindBy(xpath = "//div[@id='022_0066']/a[3]")
    public WebElementFacade opcDescargoPersonal;

    @FindBy(id="ucListaGenericaTipoDescargo_ddlLista")
    public WebElementFacade tipoDescargo;

    @FindBy(id="ucListaGenericaSubTipoDescargo_ddlLista")
    public WebElementFacade subTipoDescargo;

    @FindBy(id="txtComentario")
    public WebElementFacade comentario;

    @FindBy(xpath = "//div[@id='021_0066']/a[4]")
    public WebElementFacade opcReingreso;

    @FindBy(xpath = "//div[@id='022_0066']/a[4]")
    public WebElementFacade opcReingresoPersonal;

    @FindBy(xpath = "//div[@id='021_0066']/a[5]")
    public WebElementFacade opcCambioVencimiento;

    @FindBy(xpath = "//div[@id='022_0066']/a[5]")
    public WebElementFacade opcCambioVencimientoPersonal;

    @FindBy(id="txtDiaVencimiento")
    public WebElementFacade diaVencimiento;


    @FindBy(xpath = "//div[@id='021_0066']/a[7]")
    public WebElementFacade opcAjusteDeCuota;

    @FindBy(xpath = "//div[@id='022_0066']/a[7]")
    public WebElementFacade opcAjusteDeCuotaPersonal;

    @FindBy(xpath = "//a[@id='dgCronograma_ctl02_hypUpd']/img")
    public WebElementFacade btnModificarCuota;

    @FindBy(id="txtIntCompensatorioMod")
    public WebElementFacade interesCompensatorioMod;

    @FindBy(id="txtTotal")
    public WebElementFacade saldoPendiente;

    @FindBy(id="txtPago")
    public WebElementFacade importePagar;

    @FindBy (id="ucListaGenericaCuentaContable_ddlLista")
    public WebElementFacade lstMedioCargo;

    @FindBy(id="txtMonto")
    public WebElementFacade montoNeto;

    @FindBy(id="ucListaGenericaModalidad_ddlLista")
    public WebElementFacade modalidadPrepago;


    @FindBy(xpath = "//div[@id='021_0070']/a[3]")
    public WebElementFacade opcPagoCuotasCFV;

    @FindBy(xpath = "//div[@id='022_0070']/a[3]")
    public WebElementFacade opcPagoCuotasCFVPersonal;


    @FindBy(id="ucListaGenericaModalidadPrePago_ddlLista")
    public WebElementFacade modalidadPrepagoSimulacion;

    @FindBy(xpath="//div[@id='031']/a[3]")
    public WebElementFacade opcSimuladorPrepago;

    @FindBy(xpath="//div[@id='032']/a[3]")
    public WebElementFacade opcSimuladorPrepagoPersonal;

    @FindBy(xpath="//table[@id='tdContent']/tbody/tr[2]/td[2]/div")
    public WebElementFacade pantallaCronograma;

    @FindBy(xpath = "//table[@id='dgrCronograma']/tbody/tr[2]/td[11]")
    public WebElementFacade ultimoElemento;

    @FindBy(xpath = "//table[@id='dgrCronograma']/tbody/tr[2]/td[10]")
    public WebElementFacade penultimoElemento;

    @FindBy(id = "ucPaginacion_imgAnterior")
    public WebElementFacade retrocederPaginaAnterior;

    @FindBy(id = "ucPaginacion_imgSiguiente")
    public WebElementFacade avanzarSiguientePagina;

    @FindBy(id = "ucPaginacion_imgPrimero")
    public WebElementFacade retrocederHastaPrimeraPagina;

    @FindBy(id = "ucPaginacion_imgUltimo")
    public WebElementFacade avanzarHastaUltimaPagina;

    @FindBy(id = "ucPaginacion_txtIrPagina")
    public WebElementFacade nroPagina;

    @FindBy(id = "ucPaginacion_lblNumReg")
    public WebElementFacade cantidadRegistros;

    @FindBy(id = "ucListaTipoCronograma_ddlLista")
    public WebElementFacade cmbTipoCronograma;

    @FindBy(xpath = "//div[@id='021_0066']/a[14]")
    public WebElementFacade opcBloqueo;

    @FindBy(xpath = "//div[@id='022_0066']/a[14]")
    public WebElementFacade opcBloqueoPersonal;

    @FindBy(xpath = "//div[@id='021_0066']/a[16]")
    public WebElementFacade opcDesbloqueo;

    @FindBy(xpath = "//div[@id='022_0066']/a[16]")
    public WebElementFacade opcDesbloqueoPersonal;

    @FindBy(xpath = "//div[@id='021_0066']/a[15]")
    public WebElementFacade opcCambioProductoHipotecario;

    @FindBy(xpath = "//div[@id='021_0070']/a[1]")
    public WebElementFacade opcPrepagoCFV;

    @FindBy(xpath = "//div[@id='022_0070']/a[1]")
    public WebElementFacade opcPrepagoCFVPersonal;

    @FindBy(xpath = "//div[@id='021_0066']/a[17]")
    public WebElementFacade opcReenganche;

    @FindBy(xpath = "//div[@id='022_0066']/a[17]")
    public WebElementFacade opcReenganchePersonal;

    @FindBy(id="txtMontoBeneficiario")
    public WebElementFacade txtMontoBeneficiario;


    @FindBy(xpath = "//div[@id='021_0066']/a[8]")
    public WebElementFacade opcInclusionCuotaFlexible;

    @FindBy(xpath = "//div[@id='022_0066']/a[8]")
    public WebElementFacade opcInclusionCuotaFlexiblePersonal;

    @FindBy(id="txtAnio")
    public WebElementFacade inclusionCF_Anio;

    @FindBy(id="txtMes")
    public WebElementFacade inclusionCF_Mes;

    @FindBy(id="ucListaGenericaTipoCuotaCero_ddlLista")
    public WebElementFacade inclusionCF_Tipo;

    @FindBy(xpath = "//div[@id='021_0066']/a[9]")
    public WebElementFacade opcInclusionCuotaFlexibleCFV;

    @FindBy(xpath = "//div[@id='022_0066']/a[9]")
    public WebElementFacade opcInclusionCuotaFlexibleCFVPersonal;

    @FindBy(id="ucFechaCalculada_txtFecha")
    public WebElementFacade inclusionCFV_Fecha;

    @FindBy(xpath = "//div[@id='021_0066']/a[10]")
    public WebElementFacade opcInclusionCuotaReprogramada;

    @FindBy(xpath = "//div[@id='022_0066']/a[10]")
    public WebElementFacade opcInclusionCuotaReprogramadaPersonal;

    @FindBy(id="txtNroCuotaCero")
    public WebElementFacade inclusionCReprog_NroCuotaCero;

    @FindBy(id="txtNroCuotasCero")
    public WebElementFacade inclusionCReprog_NroCuotasCero;

    @FindBy(xpath = "//div[@id='021']/a[5]")
    public WebElementFacade opcAnulacion;

    @FindBy(xpath = "//div[@id='022']/a[5]")
    public WebElementFacade opcAnulacionPersonal;

    @FindBy(xpath = "//div[@id='021_0147']/a[4]")
    public WebElementFacade opcAnulacionPagoDeCuotas;

    @FindBy(xpath = "//div[@id='022_0147']/a[4]")
    public WebElementFacade opcAnulacionPagoDeCuotasPersonal;

     @FindBy(xpath = "//div[@id='021_0147']/a[2]")
    public WebElementFacade opcAnulacionPrepago;

    @FindBy(xpath = "//div[@id='022_0147']/a[2]")
    public WebElementFacade opcAnulacionPrepagoPersonal;


     @FindBy(xpath = "//div[@id='021_0147']/a[1]")
    public WebElementFacade opcAnulacionCancelacion;

    @FindBy(xpath = "//div[@id='022_0147']/a[1]")
    public WebElementFacade opcAnulacionCancelacionPersonal;

     @FindBy(xpath = "//div[@id='031']/a[5]")
    public WebElementFacade opcSimuladorPagoCuotas;

    @FindBy(xpath = "//div[@id='032']/a[5]")
    public WebElementFacade opcSimuladorPagoCuotasPersonal;

     @FindBy(xpath = "//div[@id='031']/a[7]")
    public WebElementFacade opcSimuladorCuotaReprogramada;

    @FindBy(xpath = "//div[@id='032']/a[7]")
    public WebElementFacade opcSimuladorCuotaReprogramadaPersonal;



    @FindBy(id ="ucFechaIngreso_txtFecha")
    public WebElementFacade lblFechaActual;

    @FindBy(id ="ucFechaDesembolso_hypFecha")
    public WebElementFacade lstFechaDesembolso;

    @FindBy(id ="txtIntMoratorioMod")
    public WebElementFacade txtInteresMoratorio;

    @FindBy(xpath = "//div[@id='031']/a[6]")
    public WebElementFacade opcSimuladorCuotaFlexible;

    @FindBy(xpath = "//div[@id='032']/a[6]")
    public WebElementFacade opcSimuladorCuotaFlexiblePersonal;

    @FindBy(xpath = "//div[@id='031']/a[8]")
    public WebElementFacade opcSimuladorModificacion;

    @FindBy(xpath = "//div[@id='032']/a[8]")
    public WebElementFacade opcSimuladorModificacionPersonal;

    @FindBy(xpath = "//div[@id='031']/a[10]")
    public WebElementFacade opcSimuladorCambioVencimiento;

    @FindBy(xpath = "//div[@id='032']/a[10]")
    public WebElementFacade opcSimuladorCambioVencimientoPersonal;

    @FindBy(xpath = "//div[@id='021_0147']/a[3]")
    public WebElement opcAnulacionModificacion;


    @FindBy(xpath = "//div[@id='022_0147']/a[3]")
    public WebElement opcAnulacionModificacionPersonal;


    @FindBy(id="txtMotivo")
    public WebElementFacade txtMotivo;

    @FindBy(xpath = "//div[@id='021']/a[18]")
    public WebElementFacade opcCargaMasiva;

    @FindBy(xpath = "//div[@id='021_0196']/a[2]")
    public WebElementFacade opcCronogramaMasivo;

    @FindBy(xpath = "//div[@id='021']/a[12]")
    public WebElementFacade opcAdeudado;

    @FindBy(xpath = "//div[@id='021_0136']/a[2]")
    public WebElementFacade opcLiqAdeudado;

    @FindBy(xpath = "//div[@id='021_0136']/a[4]")
    public WebElementFacade opcLiqCoberturado;

    @FindBy(xpath = "//div[@id='021_0136']/a[5]")
    public WebElementFacade opcAnulacionCoberturado;

    @FindBy(xpath = "//div[@id='021_0136']/a[6]")
    public WebElementFacade opcAnulacionLiqCoberturado;

    @FindBy(xpath = "//div[@id='071']/a[1]")
    public WebElementFacade opcReporteDesembolsos;

    @FindBy(xpath = "//div[@id='072']/a[1]")
    public WebElementFacade opcReporteDesembolsosPersonal;

    @FindBy(id="ucFechaInicio_txtFecha")
    public WebElementFacade opcFechaIngresoDel;

    @FindBy(id="ucFechaFin_txtFecha")
    public WebElementFacade opcFechaIngresoAl;

    @FindBy(xpath = "//div[@id='021']/a[8]")
    public WebElementFacade opcCreditoRelacionados;

    @FindBy(id="ucListaGrupoPrincipal_ddlLista")
    public WebElementFacade txtGrupoPrincipal;

    @FindBy(id="ucListaGrupoRelacionado_ddlLista")
    public WebElementFacade txtGrupoAsociado;

    @FindBy(id="ucBusquedaCreditoPrincipal_txtCodigo")
    public WebElementFacade txtNroCreditoPrincipal;

    @FindBy(id="ucBusquedaCreditoRelacionado_txtCodigo")
    public WebElementFacade txtNroCreditoAsociado;


    @FindBy(xpath = "//table[@id='dgrdMnt']/tbody/tr[2]/td[1]")
    public WebElementFacade grillaCreditoRelacionado;


    @FindBy (id="rbCargoCtaSI")
    public WebElementFacade indicadorSI;

    @FindBy (id="rbCargoCtaNO")
    public WebElementFacade indicadorNO;


    @FindBy (id="txtMotivoCambio")
    public WebElementFacade motivoCambio;


    @FindBy(xpath = "//div[@id='022_0066']/a[11]")
    public WebElementFacade opcInclusionCuotaMiTaxi;


    @FindBy(xpath = "//div[@id='021_0136']/a[1]")
    public WebElementFacade opcConfirmacionCofide;

    @FindBy(xpath = "//table[@id='dgrCartaCofide']/tbody/tr[2]/td[3]")
    public WebElementFacade GrillaConfirmacionCofide;

    @FindBy(id="fpAnimswapImgFP2")
    public WebElementFacade btnAbrirRegistro;

    @FindBy(id="ucFechaLiquidacion_txtFecha")
    public WebElementFacade fechaLiquidacion;

    @FindBy(id="txtFecha")
    public WebElementFacade fecha;

    @FindBy(id="txtObservacion")
    public WebElementFacade observacion;

    @FindBy(xpath = "//div[@id='021']/a[6]")
    public WebElementFacade opcAsignacionDeBonos;

    @FindBy(xpath = "//table[@id='dgrdMnt']/tbody/tr[2]/td[2]")
    public WebElementFacade grilla;

    @FindBy(id="dgListaElegidos_ctl02_chkSeleccion")
    public WebElementFacade checkAsigBonos;

    @FindBy(xpath = "//div[@id='021']/a[7]")
    public WebElementFacade opcTitulizacionDeCreditos;

    @FindBy(id="ucListaEntidad_ddlLista")
    public WebElementFacade cmbEntidad;

    @FindBy(id="ucListaPatrimonio_ddlLista")
    public WebElementFacade cmbPatrimonio;

    @FindBy(id="txtNumeroPortafolio")
    public WebElementFacade nroPortafolio;

    @FindBy(id="ucListaTipoDesasignacion_ddlLista")
    public WebElementFacade tipoDesasignacion;

    @FindBy(id="txtImporteBBP")
    public WebElementFacade importeBBP;

    @FindBy(id="chkIndBBP")
    public WebElementFacade indBBP;

    @FindBy(id="txtImporteBMS")
    public WebElementFacade importeBMS;

    @FindBy(id="chkIndBMS")
    public WebElementFacade indBMS;

    @FindBy(id="txtFMV")
    public WebElementFacade nroFMV;


    }







