package com.everis.base.PageObject;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("http://intranetib/sda/")
public class GeneralPage  extends PageObject {

    //LOGIN

    @FindBy(id = "inpuser")
    public WebElementFacade txtUserLogin;

    @FindBy(id = "inp_clave")
    public WebElementFacade txtPassLogin;

    @FindBy(name = "selambiente")
    public WebElementFacade cbAmbienteLogin;

    @FindBy(id = "Aceptar")
    public WebElementFacade btnAceptarLogin;


    //GRUPOS HPC - Servidor: s183va05\bdm

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[13]/td/p[2]/a")
    public WebElementFacade txtHipotecario;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[1]/a")
    public WebElementFacade txtHipotecarioUpgrade;

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[13]/td/p[1]/a")
    public WebElementFacade txtHipotecarioADM;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[6]/td/p[2]/a")
    public WebElementFacade txtPPGH;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[2]/a")
    public WebElementFacade txtPPGHUpgrade;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[6]/td/p[3]/a")
    public WebElementFacade txtPersonal;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[3]/a")
    public WebElementFacade txtPersonalUpgrade;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[6]/td/p[4]/a")
    public WebElementFacade txtVehicular;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[4]/a")
    public WebElementFacade txtVehicularUpgrade;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[6]/td/p[5]/a")
    public WebElementFacade txtRefinanciado;

    @FindBy(xpath="/html/body/table[1]/tbody/tr[4]/td[1]/table/tbody/tr[6]/td/p[5]/a")
    public WebElementFacade txtRefinanciadoUpgrade;

    /* HPC BETA - UPGRADE HPC

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[1]/a")
    public WebElementFacade txtHipotecario;

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[2]/a")
    public WebElementFacade txtPPGH;

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[3]/a")
    public WebElementFacade txtPersonal;

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[4]/a")
    public WebElementFacade txtVehicular;

    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[5]/a")
    public WebElementFacade txtRefinanciado;
*/

    //GRUPOS JCC

    //@FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[1]/a")
    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[8]/td/p[1]/a")
    public WebElementFacade JCC_BP_OPE;

    //@FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[3]/a")
    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[8]/td/p[3]/a")
    public WebElementFacade JCC_BC_OPE;

    //@FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[5]/a")
    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[8]/td/p[5]/a")
    public WebElementFacade JCC_MP_OPE;

    //@FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[2]/a")
    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[8]/td/p[2]/a")
    public WebElementFacade JCC_BC_ADM;

    //@FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[7]/td/p[3]/a")
    @FindBy(xpath="//table[1]/tbody/tr[4]/td[1]/table/tbody/tr[8]/td/p[4]/a")
    public WebElementFacade JCC_MP_ADM;

}