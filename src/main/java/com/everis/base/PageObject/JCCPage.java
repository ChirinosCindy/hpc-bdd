package com.everis.base.PageObject;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("http://intranetib/sda/")
public class JCCPage  extends PageObject {

    @FindBy(id = "cuwMantenimientoJCC_ibtnNuevo")
    public WebElementFacade btnNuevoRegistro;

    @FindBy(id = "cuwMantenimientoJCC_ibtnConsulta")
    public WebElementFacade btnBuscarRegistro;

    @FindBy(id = "cuwMantenimientoJCC_ibtnEliminar")
    public WebElementFacade btnEliminarRegistro;

    @FindBy(id = "cuwMantenimientoJCC_ibtnGrabar")
    public WebElementFacade btnGuardarRegistro;

    @FindBy(id = "cuwMantenimientoJCC_ibtnReporte")
    public WebElementFacade btnDescargarExcelJCC;

    @FindBy(id="cuwMantenimientoJCC_ibtnSalir")
    public WebElementFacade btnCerrarRegistro;

    @FindBy(id="mainFrame")
    public WebElementFacade iFrame;

    @FindBy(xpath="//table[@id='gvwListadoCredito']/tbody/tr[2]/td[3]")
    public WebElementFacade registroGrillaPagoEspecial;

    @FindBy(xpath="//table[@id='gvwListadoCredito']/tbody/tr[2]/td[2]")
    public WebElementFacade registroGrillaJCC;

    @FindBy(xpath="//li[@id='1']/a")
    public WebElementFacade opcTransacciones;

    @FindBy(xpath="//li[@id='5']/a")
    public WebElementFacade opcPagoEstandar;

    @FindBy(xpath="//li[@id='2']/a")
    public WebElementFacade opcIngresarOperacion;

    @FindBy(id="cuwOperacionJudicial_txtNumOper")
    public WebElementFacade txtNroOperacion;

    @FindBy(id="txtImpCanc")
    public WebElementFacade txtImporteCancelacion;

    @FindBy(id="txtMonPag")
    public WebElementFacade txtMontoPagar;

    @FindBy(id="ddlMotivo")
    public WebElementFacade cmbMotivoPago;

    @FindBy(id="txtObservaciones")
    public WebElementFacade txtObservaciones;


    @FindBy(xpath = "//li[@id='6']/a")
    public WebElementFacade opcPagoEspecial;

    @FindBy(xpath="//div[1]/div[3]/div/button[1]/span")
    public WebElementFacade alertSi;

    @FindBy(xpath="//div[2]/div[3]/div/button[1]/span")
    public WebElementFacade alertSi2;

    @FindBy(xpath="//div[1]/div[3]/div/button/span")
    public WebElementFacade alertAceptar;

    @FindBy(xpath="//div[2]/div[3]/div/button/span")
    public WebElementFacade alertAceptar2;

    @FindBy(xpath = "//div[@id='divMessageConfirm']/table/tbody/tr/td[2]/strong")
    public WebElementFacade textoMensaje;

    @FindBy(id="divMessageConfirm")
    public WebElementFacade contenedorMensajeConfirm;

    @FindBy(id="txtProd")
    public WebElementFacade txtCodigoProducto;

    @FindBy(id="txtCUCliente")
    public WebElementFacade txtCUCliente;

    @FindBy(id="ddlMoneda")
    public WebElementFacade cmbTipoMoneda;

    @FindBy(id="txtMonCap")
    public WebElementFacade txtCapitalOrigen;

    @FindBy(id="txtIntComOri")
    public WebElementFacade txtInteresCompensatorioOrigen;

    @FindBy(id="txtMonMoraOri")
    public WebElementFacade InteresMoratorioOrigen;

    @FindBy(id="txtIntMor")
    public WebElementFacade txtInteresMoratorioOrigen;

    @FindBy(id="txtIntComVenOri")
    public WebElementFacade InteresCompensatorioVencidoOrigen;

    @FindBy(id="txtIntComVen")
    public WebElementFacade txtInteresCompensatorioVencidoOrigen;

    @FindBy(id="txtIntDife")
    public WebElementFacade txtInteresDiferidoOrigen;

    @FindBy(id="txtTasIntComOri")
    public WebElementFacade txtTasaInteresCompensatorioOrigen;

    @FindBy(id="txtTasIntComJCC")
    public WebElementFacade txtTasaInteresCompensatorioVencido;

    @FindBy(id="txtTasComVen")
    public WebElementFacade txtTasaInteresCompensatorioVencidoOrigen;

    @FindBy(id="txtTasMor")
    public WebElementFacade txtTasaInteresMoratorio;

    @FindBy(id="txtMonGasProOri")
    public WebElementFacade txtGastosOrigen;

    @FindBy(id="txtMonSeg")
    public WebElementFacade txtSegurosOrigen;

    @FindBy(id="txtComision")
    public WebElementFacade txtComisionOrigen;

    @FindBy(id="txtFechaIniCal")
    public WebElementFacade txtFechaInicioCalculo;

    @FindBy(id="txtFechaDesem")
    public WebElementFacade txtFechaDesembolso;

    @FindBy(id="txtFechaVencto")
    public WebElementFacade txtFechaVencimiento;

    @FindBy(id="ddlAppOri")
    public WebElementFacade cmbAplicativoOrigen;

    @FindBy(id="txtNumOperAnt")
    public WebElementFacade txtNroOperacionAnterior;

    @FindBy(id="txtTdaColoc")
    public WebElementFacade txtTiendaColocacion;

    @FindBy(id="txtEstAbo")
    public WebElementFacade txtCodEstudioAbogados;

    @FindBy(id="txtMonCapital")
    public WebElementFacade montoCapital;

    @FindBy(id="txtIntCom")
    public WebElementFacade montoInteresCompensatorio;

    @FindBy(id="txtIntMora")
    public WebElementFacade montoInteresMoratorio;

    @FindBy(id="txtMonGasto")
    public WebElementFacade montoGastos;

    @FindBy(id="txtMonSeguOri")
    public WebElementFacade seguroOrigen;

    @FindBy(id="txtMonGasProt")
    public WebElementFacade montoGastosProtesto;

    @FindBy(id="txtMonSegu")
    public WebElementFacade montoSeguros;

    @FindBy(id="txtMonHono")
    public WebElementFacade montoHonorarios;

    @FindBy(id="txtMonComi")
    public WebElementFacade montoComisiones;

    @FindBy(id="tdMonCapital")
    public WebElementFacade saldoActual_capital;

    @FindBy(id="txtObservacion")
    public WebElementFacade txtObservacion;

    @FindBy(id="tdIntCom")
    public WebElementFacade saldoActual_interesCompensatorio;

    @FindBy(id="tdIntMora")
    public WebElementFacade saldoActual_interesMoratorio;

    @FindBy(id="tdMonGasto")
    public WebElementFacade saldoActual_gastos;

    @FindBy(id="tdMonGasProt")
    public WebElementFacade saldoActual_gastosProtesto;

    @FindBy(id="tdMonSegu")
    public WebElementFacade saldoActual_Seguros;

    @FindBy(id="tdMonComi")
    public WebElementFacade saldoActual_Comisiones;

    @FindBy(id="tdMonHono")
    public WebElementFacade saldoActual_Honorarios;

    @FindBy(xpath="//li[@id='7']/a")
    public WebElementFacade opcAnulacionPagosJCC;

    @FindBy(id="tdPagCapital")
    public WebElementFacade pagado_capital;

    @FindBy(id="pagado_intCompOrig")
    public WebElementFacade pagado_interesCompensatorioOrigen;

    @FindBy(xpath="//li[@id='9']")
    public WebElementFacade opcIncrementos;

    @FindBy(xpath = "//li[@id='10']/a")
    public WebElementFacade opcModificacion;

    @FindBy(id="txtTasHon")
    public WebElementFacade txtHonorarios;


    @FindBy(xpath="//div[@id='dvCargandoModal']/table/tbody/tr/td[2]")
    public WebElementFacade cargaModal;

    @FindBy(id="ddlTipMov")
    public WebElementFacade tipoMovimiento;

    @FindBy(id="ddlCategoria")
    public WebElementFacade categoria;

    @FindBy(id="ddlConcepto")
    public WebElementFacade concepto;

    @FindBy(id="txtNroDocumento")
    public WebElementFacade nroPlanilla;


    @FindBy(id="btnGrabar")
    public WebElementFacade btnGrabar;

    @FindBy(id="lkbVerTransaccionPago")
    public WebElementFacade lnkPagosAnular;


    @FindBy(xpath ="//table[@id='gvwPago']/tbody/tr[2]/td[1]")
    public WebElementFacade registroAnularPago;














}