package com.everis.base.PageObject;

import com.everis.base.commons.Constant;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl(Constant.URL_SimulacionPrepagoDigital)
public class ServicioPrepagoDigitalPage extends PageObject {

    @FindBy(name = "intNroCredito")
    public WebElementFacade nroCredito;

    @FindBy(name = "intPeriodoGracia")
    public WebElementFacade periodoGracia;

    @FindBy(name = "decMontoPrepago")
    public WebElementFacade montoPrepago;

    @FindBy(xpath = "//div[@id='content']/span/form/table/tbody/tr[5]/td[2]/input")
    public WebElementFacade btnInvoke;

    @FindBy(xpath = "//div[@id='content']/p")
    public WebElementFacade txtNombreServicio;

    @FindBy(xpath = "//div[2]/div[2]/div[12]/div[2]/div[1]/div/span[5]")
    public WebElementFacade txtCodError;

    @FindBy(xpath = "//div[2]/div[2]/div[12]/div[2]/div[2]/div/span[5]")
    public WebElementFacade txtMensajeError;


}