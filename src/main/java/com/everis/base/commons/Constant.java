package com.everis.base.commons;

public class Constant {

    public static final String User = "S9233";
    public static final String Password = "Alesa5959*";
    public static final String Ambiente = "Pruebas";

    public static final String User_ADMIN = "S9233";
    public static final String Password_ADMIN = "Alesa5858*";

    public static final String RUTA_EVIDENCIAS = "C:\\Users\\Public\\Documents";
    public static String numCredito = "";
    public static String nombreGrupo = "";

    public static final String Mensaje_REGISTRO_EXITOSO = "Se agrego con exito el registro";
    public static final String Mensaje_REGISTRO_ELIMINADO = "Se elimino con exito el registro";
    public static final String Mensaje_REGISTRO_DATOS_FUERA_PLANTILLA = "Existen datos fuera del rango d"; //Existen datos fuera del rango de la plantilla :
    public static final String Mensaje_REGISTRO_TARIFA_DIA_AYER = "Los Valores de Tarifas son del Dia de ayer"; //Los Valores de Tarifas son del Dia de ayer
    public static final String Mensaje_PROCESANDO = "Procesando...";

    public static final String Desembolso_Promotor = "22626";
    public static final String Desembolso_Canal = "100";
    public static final String Desembolso_Tienda = "100";
    public static final String Desembolso_TipoEvaluacion = "NORMAL";
    public static final String Desembolso_TipoAbono = "5"; //Transitoria
    public static final String Desembolso_DestinoCredito = "1";
    public static final String Desembolso_TipoEnvioInformePago = "NO ENVIO";
    public static final String Desembolso_Establecimiento = "1425";
    public static final String Desembolso_TasaSegBienVehicular = "0.001";
    public static final String Desembolso_ProductoRefinanciado = "CREDITO HIPOTECARIO";
    public static final String Desembolso_NumeroProdRefinanciar = "42118";
    public static final String Desembolso_MontoRefinanciar = "12000";
    public static final String Desembolso_InteresRefinanciar = "100";
    public static final String Desembolso_ComisionRefinanciar = "120";
    public static final String Desembolso_ClienteRefinanciar = "50000001";

    public static final String Descargo_TipoDescargo = "NORMAL";
    public static final String Descargo_SubTipoDescargo = "NO SE REINGRESARA POSTERIORMENTE";
    public static final String Descargo_Comentario = "REALIZAR OPERACIoN";

    public static final String CambioVencimiento_DiaVencimiento ="2";
    public static final String CambioVencimiento_Comentario = "REALIZAR OPERACIoN DE CAMBIO DE VENCIMIENTO";

    public static final String AjusteCuota_interesCompensatorio="20";
    public static final String AjusteCuota_interesMoratorio="100";

    public static final String Pago_MedioCargo="IB";

    public static final String Bloqueo_Comentario="REALIZAR LA OPERACIoN";

    public static final String Reenganche_MontoAdicional="10000";

    public static final String InclusionCuotaFlexible_Tipo = "CONSERVAR EL PLAZO";
    public static final String InclusionCuotaFleixble_Comentario = "REALIZAR OPERACION";

    public static String codGrupo = "";
    public static final  String Motivo = "Realizar Operacion";
    public static final String Comentario="Realizar Operacion";

    public static final String URL_ObtenerDatosParalelo = "http://hpcuat/WSHpcIntranet/Consulta.asmx?op=ObtenerDatosParalelo";
    public static final String URL_ObtenerDatosPrepago = "http://hpcuat/WSHpcIntranet/Consulta.asmx?op=ObtenerDatosPrepago";
    public static final String URL_SimulacionPrepagoDigital = "http://hpcuat/WSHpcIntranet/simulacion.asmx?op=ObtenerDatosSimulacionPrepagoDigital";

    public static final int[] TamanoEvidencia_ObtenerDatosParalelo = {4, 12, 740, 280};
    public static final int[] TamanoEvidencia_ObtenerDatosPrepago = {4, 12, 740, 310};
    public static final int[] TamanoEvidencia_SimulacionPrepagoDigital = {4, 12, 740, 350};

    public static final String MotivoCambio = "Realizar Operacion";
    public static final String Observacion ="Realizar Operacion";

}
