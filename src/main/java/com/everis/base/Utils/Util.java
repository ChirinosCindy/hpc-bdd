package com.everis.base.Utils;

import com.everis.base.commons.Constant;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.ZonedDateTime;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Util extends Constant{

    private static final Logger logger = LoggerFactory.getLogger(Util.class);
    public int numerador = 0;
    protected static String rutaEvidencias = Constant.RUTA_EVIDENCIAS;
    protected static String nombreCarpeta = "";
    protected static String fechaYHora = "";

    public void crearCarpetaEvidencias(String carpetaEvidencias) {
        fechaYHora = ZonedDateTime.now().toString().substring(0,19)
                .replace("T"," ").replace(":","").replace("-","");

        File directorio = new File(Constant.RUTA_EVIDENCIAS + "\\" +
                fechaYHora + " - " +
                carpetaEvidencias);
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                logger.info("Directorio de evidencias creado");
            } else {
                logger.info("Error al crear directorio evidencias");
            }
        }
        Constant.numCredito = "";
        nombreCarpeta = carpetaEvidencias;
        rutaEvidencias = directorio.toString();
    }

    public void tomarEvidencia(String nombreEvidencia) throws IOException {
        numerador ++;
        File scrFile = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(rutaEvidencias + "\\" +
                numerador + "_" +  Constant.numCredito + " " + nombreEvidencia + ".png"));
    }

    public void tomarEvidenciaHPC(String nombreEvidencia) throws IOException {
        numerador ++;
        File screenshot = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);

        BufferedImage fullImg = ImageIO.read(screenshot);
        BufferedImage elemImg = fullImg.getSubimage(288,81,790,360);
        ImageIO.write(elemImg,"png",screenshot);

        FileUtils.copyFile(screenshot, new File(rutaEvidencias + "\\" +
                numerador + "_" +  Constant.numCredito + " " + nombreEvidencia + ".png"));
    }

    public void tomarEvidenciaSimulacionPrepagoDigital(String nombreEvidencia) throws IOException {
        numerador ++;
        File screenshot = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);

        BufferedImage fullImg = ImageIO.read(screenshot);
        BufferedImage elemImg = fullImg.getSubimage(
                Constant.TamanoEvidencia_SimulacionPrepagoDigital[0],
                Constant.TamanoEvidencia_SimulacionPrepagoDigital[1],
                Constant.TamanoEvidencia_SimulacionPrepagoDigital[2],
                Constant.TamanoEvidencia_SimulacionPrepagoDigital[3]);
        ImageIO.write(elemImg,"png",screenshot);

        FileUtils.copyFile(screenshot, new File(rutaEvidencias + "\\" +
                numerador + "_" +  Constant.numCredito + " " + nombreEvidencia + ".png"));
    }

    public void tomarEvidenciaObtenerDatosParalelo(String nombreEvidencia) throws IOException {
        numerador ++;
        File screenshot = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);

        BufferedImage fullImg = ImageIO.read(screenshot);
        BufferedImage elemImg = fullImg.getSubimage(
                Constant.TamanoEvidencia_ObtenerDatosParalelo[0],
                Constant.TamanoEvidencia_ObtenerDatosParalelo[1],
                Constant.TamanoEvidencia_ObtenerDatosParalelo[2],
                Constant.TamanoEvidencia_ObtenerDatosParalelo[3]);
        ImageIO.write(elemImg,"png",screenshot);

        FileUtils.copyFile(screenshot, new File(rutaEvidencias + "\\" +
                numerador + "_" +  Constant.numCredito + " " + nombreEvidencia + ".png"));
    }

    public void tomarEvidenciaObtenerDatosPrepago(String nombreEvidencia) throws IOException {
        numerador ++;
        File screenshot = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);

        BufferedImage fullImg = ImageIO.read(screenshot);
        BufferedImage elemImg = fullImg.getSubimage(
                Constant.TamanoEvidencia_ObtenerDatosPrepago[0],
                Constant.TamanoEvidencia_ObtenerDatosPrepago[1],
                Constant.TamanoEvidencia_ObtenerDatosPrepago[2],
                Constant.TamanoEvidencia_ObtenerDatosPrepago[3]);
        ImageIO.write(elemImg,"png",screenshot);

        FileUtils.copyFile(screenshot, new File(rutaEvidencias + "\\" +
                numerador + "_" +  Constant.numCredito + " " + nombreEvidencia + ".png"));
    }

    public void renombrarCarpeta(){
        if (!Constant.numCredito.equals("")) {
            File oldfile = new File(rutaEvidencias);
            File newfile = new File(rutaEvidencias + " " + Constant.nombreGrupo + " " + Constant.numCredito);
            if (oldfile.renameTo(newfile)) {
                rutaEvidencias = rutaEvidencias + " " +
                        Constant.nombreGrupo + " " +
                        Constant.numCredito;
            } else {
                logger.error("Error al renombrar carpeta");
            }
        }
        Constant.nombreGrupo = "";
    }

    public void crearLog(String mensajeError) throws IOException {
        if (Constant.numCredito.equals("")) { Constant.numCredito = "00000"; }
        FileWriter archivo;
        archivo = new FileWriter(Constant.RUTA_EVIDENCIAS + "\\" + "log.txt", true);

        //String fechaYHora = ZonedDateTime.now().toString().substring(0,19).replace("T"," ");

        archivo.write("\r" + fechaYHora + "|"
                + nombreCarpeta.toUpperCase() + "|"
                + Constant.numCredito + "|"
                +mensajeError);
        archivo.close();
    }

}
